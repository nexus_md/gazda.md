<?php

Route::prefix( '{locale}/user/{user}' )->group( function () {
	Route::get( '', 'HomeController@index' )->name( 'user.home' );
	Route::resource( 'object', 'ObjectController', [ 'as' => 'user' ] )->except( 'show' );

	Route::delete( 'object/gallery/{gallery}/delete', 'ObjectController@destroyImageGallery' )
	     ->name( 'user.object.delete-image-gallery' );

	Route::prefix( 'object/translation/{object}' )->group( function () {
		Route::get( '', 'ObjectController@translation' )->name( 'user.object.translation' );
		Route::post( '', 'ObjectController@processTranslation' )->name( 'user.object.process-translation' );

		Route::get( 'get', 'ObjectController@getTranslation' )->name( 'user.object.get-translation' );
	} );

	Route::resource( 'action', 'ActionController', [ 'as' => 'user' ] )->except( [ 'edit', 'update', 'destroy' ] );

	Route::resource( 'number', 'NumberController', [ 'as' => 'user' ] )->except( 'show' );
	Route::prefix( 'number' )->group( function () {
		Route::get( 'get-numbers', 'NumberController@getNumbers' )->name( 'user.number.get-numbers' );
		Route::get( 'get-number/{object}', 'NumberController@getObjectNumber' )->name( 'user.object.get-number' );
	} );

	Route::resource( 'plan', 'PlanController', [ 'as' => 'user' ] )->except( 'show' );
	Route::prefix( 'plan' )->group( function () {
		Route::get( 'get-plans', 'PlanController@getPlans' )->name( 'user.plan.get-plans' );
		Route::get( 'get-plans/{object}', 'PlanController@getObjectPlans' )->name( 'user.plan.get-object-plans' );
	} );

	Route::get( 'get-regions/{id}', 'CityController@getRegions' )->name( 'user.get-regions' );
} );