<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get( '/', 'HomeController@index' )->name( 'home' );

Route::get( 'ajax-get-regions', 'HomeController@ajaxGetRegions' )->name( 'ajax-get-regions' );

Route::prefix( '{locale}' )->group( function ()
{
	Route::get( '', 'HomeController@index' )->name( 'home' );

    Route::get('apartments/wish-list/add', 'ObjectController@addWishList')->name('apartments.add-wish-list');
    Route::get('apartments/wish-list', 'ObjectController@wishList')->name('apartments.wish-list');

    Route::resource('comments', 'CommentController');
    Route::resource('apartments', 'ObjectController');
    Route::resource('contacts', 'ContactController');
    Route::resource('info', 'PageController');

	Route::get( 'apartments/ajax/get-markers', 'ObjectController@getMarkers' )->name( 'apartments.get-markers' );
} );

Route::get( '/image/{imageName}', 'ImageController@index' )->name( 'image' );

Route::get( 'get-regions-tree/{id}', 'CityController@getTreeSubRegions' )->name( 'city.get-regions-tree' );

Route::get( 'get-regions/{id}', 'CityController@getRegions' )->name( 'city.get-regions' );

Route::get( 'get-parent-regions', 'CityController@getParentRegions' )->name( 'city.get-parent-regions' );

Route::get( 'get-raw-regions/{id}', 'CityController@getRawRegions' )->name( 'city.get-raw-regions' );