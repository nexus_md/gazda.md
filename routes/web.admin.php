<?php

Route::prefix( '{locale}/admin' )->group( function ()
{
	Route::get( '', 'HomeController@index' )->name( 'admin.home' );

	Route::resource( 'realtor', 'RealtorController' )->except( 'show' );
	Route::get( 'realtor/get-users', 'RealtorController@getUsers' )->name( 'realtor.get-users' );
	Route::get( 'realtor/get-user/{number}', 'RealtorController@getNumberUser' )->name( 'realtor.get-number-user' );

	Route::resource( 'object', 'ObjectController' )->except( 'show' );
	Route::get( 'object/get-objects', 'ObjectController@getObjects' )->name( 'plan.get-objects' );
	Route::get( 'object/get-objects/{plan}', 'ObjectController@getPlanObjects' )->name( 'plan.get-plan-objects' );

	Route::post( 'object/{id}/set-published', 'ObjectController@setPublishedState' )->name('object.set.published');
	Route::post( 'object/{id}/set-urgent', 'ObjectController@setUrgentState' )->name('object.set.urgent');

	Route::delete( 'object/gallery/{gallery}/delete', 'ObjectController@destroyImageGallery' )
	     ->name( 'object.delete-image-gallery' );

	Route::prefix( 'object/translation/{object}' )->group( function ()
	{
		Route::get( '', 'ObjectController@translation' )->name( 'object.translation' );
		Route::post( '', 'ObjectController@processTranslation' )->name( 'object.process-translation' );

		Route::get( 'get', 'ObjectController@getTranslation' )->name( 'object.get-translation' );
	} );

	Route::resource( 'number', 'NumberController' )->except( 'show' );
	Route::get( 'number/get-numbers', 'NumberController@getNumbers' )->name( 'number.get-numbers' );
	Route::get( 'number/get-numbers/{realtor}', 'NumberController@getUserNumbers' )->name( 'realtor.get-numbers' );
	Route::get( 'number/get-number/{object}', 'NumberController@getObjectNumber' )->name( 'object.get-number' );

	Route::resource( 'plan', 'PlanController' )->except( 'show' );
	Route::get( 'plan/get-plans', 'PlanController@getPlans' )->name( 'plan.get-plans' );
	Route::get( 'plan/get-plans/{object}', 'PlanController@getObjectPlans' )->name( 'plan.get-object-plans' );

	Route::get( 'action', 'ActionController@index' )->name( 'action.index' );

	Route::resource( 'pages', 'PageController' );

	Route::get( 'get-regions/{id}', 'CityController@getRegions' )->name( 'get-regions' );

	Route::resource( 'facility', 'FacilityController' );
	Route::post('comment/approve', 'CommentController@approve')->name('comment.approve');
	Route::post('comment/disable', 'CommentController@disable')->name('comment.disable');
	Route::post('comment/delete', 'CommentController@delete')->name('comment.delete');
    Route::resource( 'comment', 'CommentController' )->names(['index'=> 'comment.index', 'store' => 'comment.store']);

} );