<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterObjectTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'objects', function ( Blueprint $table ) {
			$table->integer( 'price' )->after( 'construction_type' );
			$table->integer( 'price_type' )->after( 'construction_type' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'objects', function ( Blueprint $table ) {
			//
		} );
	}
}
