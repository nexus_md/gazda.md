<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyQuotientsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'currency_quotients', function ( Blueprint $table )
		{
			$table->increments( 'id' );
			$table->integer( 'numerator' )->unsigned();
			$table->integer( 'denominator' )->unsigned();
			$table->double( 'quotient' )->unsigned();
			$table->timestamps();

			$table->index( [ 'numerator', 'denominator' ] );

			$table->foreign( 'numerator' )
			      ->references( 'id' )->on( 'currencies' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( 'currency_quotients' );
	}
}
