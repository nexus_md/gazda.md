<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectTranslationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'object_translations', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'object_id' )->unsigned();
			$table->string( 'iso_code' )->index();
			$table->string( 'slug' )->unique();
			$table->string( 'title' )->index();
			$table->text( 'description' );
			$table->text( 'text' );

			$table->foreign( 'object_id' )
			      ->references( 'id' )->on( 'objects' )
			      ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'object_translations' );
	}
}
