<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('object_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('type');
            $table->integer('advance_price')->nullable();
            $table->integer('passive_price')->nullable();
            $table->integer('total_price')->nullable();
            $table->text('text')->nullable();
            $table->timestamps();

            $table->foreign('object_id')
                ->references('id')->on('objects')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
