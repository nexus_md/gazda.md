<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'videos', function ( Blueprint $table )
		{
			$table->increments( 'id' );
			$table->integer( 'object_id' )->unsigned();
			$table->text( 'url' );
			$table->timestamps();

			$table->foreign( 'object_id' )
			      ->references( 'id' )->on( 'objects' )
			      ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( 'videos' );
	}
}
