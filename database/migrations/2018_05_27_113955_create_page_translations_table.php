<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTranslationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'page_translations', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'page_id' )->unsigned();
			$table->string( 'title' );
			$table->string( 'slug' )->index();
			$table->text( 'content' );
			$table->timestamps();

			$table->foreign( 'page_id' )
			      ->references( 'id' )->on( 'pages' )
			      ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'page_translations' );
	}
}
