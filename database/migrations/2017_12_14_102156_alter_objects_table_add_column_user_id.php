<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterObjectsTableAddColumnUserId extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'objects', function ( Blueprint $table ) {
			$table->integer( 'user_id' )->after( 'id' )->unsigned()->index()->nullable();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'objects', function ( Blueprint $table ) {
			//
		} );
	}
}
