<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterObjectsTableAddRestColumns extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table( 'objects', function ( Blueprint $table )
		{
			$table->integer( 'entrance' )->nullable()->after( 'slug' );
			$table->integer( 'apartment_nr' )->nullable()->after( 'slug' );
			$table->integer( 'floor_count' )->nullable()->after( 'slug' );
			$table->integer( 'living_area' )->nullable()->after( 'slug' );
			$table->integer( 'kitchen_area' )->nullable()->after( 'slug' );
			$table->integer( 'balcony_count' )->nullable()->after( 'slug' );
			$table->integer( 'parking_lot' )->nullable()->after( 'slug' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table( 'objects', function ( Blueprint $table )
		{
			//
		} );
	}
}
