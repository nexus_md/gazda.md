<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesObjectsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'facilities_objects', function ( Blueprint $table )
		{
			$table->integer( 'facility_id' )->unsigned();
			$table->integer( 'object_id' )->unsigned();

			$table->foreign( 'facility_id' )
			      ->references( 'id' )->on( 'facilities' )
			      ->onDelete( 'cascade' );

			$table->foreign( 'object_id' )
			      ->references( 'id' )->on( 'objects' )
			      ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( 'facilities_objects' );
	}
}
