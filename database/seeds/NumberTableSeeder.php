<?php

use Illuminate\Database\Seeder;

class NumberTableSeeder extends Seeder {

	protected $nr;
	protected $user;

	public function __construct() {
		$this->nr   = 5;
		$this->user = new \App\User( [ 'id' => 1 ] );
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		for ( $i = 0; $i < $this->nr; $i ++ ) {
			$this->user->numbers()->save( factory( \App\Number::class )->make() );
		}
	}
}
