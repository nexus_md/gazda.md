<?php

use Illuminate\Database\Seeder;

class ObjectTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory( \App\Object::class, 1000 )->create()->each( function ( $object )
		{
			$object->translation()->save( factory( \App\ObjectTranslation::class )->make() );
		} );
	}
}
