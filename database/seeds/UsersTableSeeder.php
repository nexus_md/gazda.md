<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		\App\User::create( [
			'name'     => 'Admin',
			'email'    => 'admin@mail.com',
			'password' => bcrypt( 'qweqwe' ),
			'role'     => 0,
		] );

		factory( \App\User::class, 1000 )->create()->each( function ( $user ) {
			$user->numbers()->save( factory( \App\Number::class )->make() );
			$user->numbers()->save( factory( \App\Number::class )->make() );
			$user->numbers()->save( factory( \App\Number::class )->make() );
			$user->numbers()->save( factory( \App\Number::class )->make() );
			$user->numbers()->save( factory( \App\Number::class )->make() );
		} );
	}
}
