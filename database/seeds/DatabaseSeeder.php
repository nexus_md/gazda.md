<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call( [
			Facilities::class,
			UsersTableSeeder::class,
			NumberTableSeeder::class,
//			PlanTableSeeder::class,
			CommentsTableSeeder::class,
//			CityTableSeeder::class,
			CurrencyTableSeeder::class,
			ObjectTableSeeder::class,
		] );
	}
}
