<?php

use Illuminate\Database\Seeder;

class Facilities extends Seeder
{
	protected $facilities;

	public function __construct() {
		$this->facilities = [
			'Можно с детьми',
			'Можно с животными',
			'Балкон',
			'Холодильник',
			'Стиральная машина',
			'Мебель в комнатах',
			'Мебель на кухне',
			'Телевизор',
			'Телефон',
			'Oтопление',
			'Лифт',
			'Парковая зона',
			'Детский сад',
			'Продуктовый рынок',
			'Больница',
			'Детская площадка',
			'Школа',
			'Супермаркет'
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
//	    factory( \App\Facility::class, 1000 )->create()->each( function ( $object )
//	    {
//		    $object->translation()->save( factory( \App\FacilityTranslation::class )->make() );
//	    } );
		foreach ($this->facilities as $value) {
			$facility = \App\Facility::create([
			]);

			$facility->translation()->create([
				'iso_code' => 'ru',
				'name' => $value
			]);

			$facility->translation()->create([
				'iso_code' => 'en',
				'name' => $value
			]);

			$facility->translation()->create([
				'iso_code' => 'ro',
				'name' => $value
			]);

		}

	}
}