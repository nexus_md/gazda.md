<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$currencies = [
			[
				'acronym' => 'MDL',
				'name'    => 'Moldovan leu',
				'symbol'  => 'MDL',
			],

			[
				'acronym' => 'USD',
				'name'    => 'United States dollar',
				'symbol'  => '$',
			],

			[
				'acronym' => 'EUR',
				'name'    => 'Euro',
				'symbol'  => 'EUR',
			],
		];

		$addedCurrencies = [];

		foreach ( $currencies as $currency ) {
			$addedCurrencies[] = \App\Currency::create( $currency );
		}

		foreach ( $addedCurrencies as $numerator ) {
			foreach ( $addedCurrencies as $denominator ) {
				$numerator->quotients()->create( [
					'denominator' => $denominator->id,
					'quotient'    => mt_rand() / mt_getrandmax(),
				] );
			}
		}
	}
}
