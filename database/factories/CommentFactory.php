<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define( App\Comment::class, function ( Faker $faker ) {
	return [
		'name' => $faker->name(),
		'text' => $faker->text(),
		'mark' => $faker->numberBetween( 1, 5 ),
	];
} );
