<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define( App\ObjectTranslation::class, function ( Faker $faker ) {
	return [
		'iso_code'    => 'en',
		'description' => $faker->realText(),
		'text'        => $faker->realText( 500 ),
	];
} );
