<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define( \App\Object::class, function ( Faker $faker )
{
	return [
		'city_id'    => \App\City::inRandomOrder()->first()->id,
		'address'    => $faker->address,
		'lat'        => 47.0145636 + $faker->randomFloat( null, - 0.2, 0.2 ),
		'lng'        => 28.831588799999963 + $faker->randomFloat( null, - 0.2, 0.2 ),
		'area'       => $faker->numberBetween( 15, 50 ),
		'room_count' => $faker->numberBetween( 15, 50 ),
		'floor'      => $faker->numberBetween( 15, 50 ),
		'type'       => $faker->numberBetween( 0, count( config( 'estate.object_state' ) ) - 1 ),
		'outer_type' => $faker->numberBetween( 0, count( config( 'estate.object_type' ) ) - 1 ),

		'construction_type' => $faker->numberBetween( 0, count( config( 'estate.construction_type' ) ) - 1 ),
		'currency_id' => 1,

		'price_type'  => $faker->numberBetween( 0, count( config( 'estate.object_price_type' ) ) - 1 ),
		'price'       => $faker->numberBetween( 100, 1000 ),
		'featured'    => true,
		'publish'     => true,
		'validate'    => true,
		'currency_id' => 1,
	];
} );
