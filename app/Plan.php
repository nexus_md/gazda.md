<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Plan extends Model {
	protected $fillable = [
		'name',
		'image',
	];

	public function scopeSearch( $query, Request $request ) {
		if ( $request->search ) {
			return $query->where( 'name', 'like', "%{$request->search}%" );
		}
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function objects() {
		return $this->belongsToMany( Object::class );
	}
}
