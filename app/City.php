<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $fillable = [ 'name', 'parent_id' ];

	protected $table = 'cities';

	public $timestamps = false;

	//Scopes

	public function scopeSelectCities( $query, $value )
	{
		return $query->whereIn( 'id', $value );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeParent( $query )
	{
		return $query->where( 'parent_id', 0 );
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeChild( $query, $value )
	{
		return $query->where( 'parent_id', $value );
	}

	//Relations

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function objects()
	{
		return $this->hasMany( Object::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function regions()
	{
		return $this->hasMany( City::class, 'parent_id' );
	}

	public function city()
	{
		return $this->belongsTo( City::class, 'parent_id' );
	}
}
