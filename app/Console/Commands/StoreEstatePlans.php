<?php

namespace App\Console\Commands;

use App\Plan;
use App\Repository\ImageHelper;
use Illuminate\Console\Command;

class StoreEstatePlans extends Command
{
	/**
	 * @var string
	 */
	protected $url;

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'estate:plans:store';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->url = 'http://liderimobil.md/planificare';

		$this->description = "Parse and store estate plans from {$this->url}";
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->parsePlans();
	}

	protected function parsePlans()
	{
		$dom = new \DOMDocument();

		libxml_use_internal_errors( true );

		$dom->loadHTML( mb_convert_encoding( file_get_contents( $this->url ), 'HTML-ENTITIES', 'UTF-8' ) );

		libxml_use_internal_errors( false );

		$sections = $dom->getElementsByTagName( 'div' );

		foreach ( $sections as $section ) {
			if ( $section->getAttribute( 'class' ) == 'row plan-list' ) {
				$this->getPlan( $section );
			}
		}
	}

	/**
	 * @param $section
	 */
	protected function getPlan( $section )
	{
		$title = $section->previousSibling->previousSibling;

		foreach ( $section->getElementsByTagName( 'a' ) as $image ) {
			$subTitle = $image->previousSibling->previousSibling;

			$data = [ 'name' => "{$title->textContent}, {$subTitle->textContent}" ];

			$path = ImageHelper::saveImage( $image->getAttribute( 'href' ) );

			$data['image'] = $path['name'];

			$plan = Plan::create( $data );

			$this->info( "{$plan->name}, created." );
		}
	}
}
