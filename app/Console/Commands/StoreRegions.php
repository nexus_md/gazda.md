<?php

namespace App\Console\Commands;

use App\City;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Console\Command;

class StoreRegions extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'regions:store {path}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Store regions based on html file';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$path = base_path() . "/{$this->argument( 'path' )}";

		$dom = new \DOMDocument();

		$dom->loadHTML( mb_convert_encoding( file_get_contents( $path ), 'HTML-ENTITIES', 'UTF-8' ) );

		$options = $dom->getElementsByTagName( 'option' );

		$this->extractRegions( $options );
	}

	/**
	 * @param $options
	 */
	private function extractRegions( $options ) {
		$client = new Client();

		$cookieJar = CookieJar::fromArray( [
			'simpalsid.lang' => 'ro',
		], '999.md' );

		foreach ( $options as $option ) {
			$regions = $this->extractRegion( $client, $cookieJar, $option->getAttribute( 'value' ), $option->textContent, 7, 0 );

			foreach ( $regions['content'] as $region ) {
				$subRegions = $this->extractRegion( $client, $cookieJar, $region[0], $region[1], 8, $regions['parent']->id );

				foreach ( $subRegions['content'] as $subRegion ) {
					if ( ! $subRegion[0] ) {
						continue;
					}

					City::create( [ 'name' => $subRegion[1], 'parent_id' => $subRegions['parent']->id ] );
					$this->info( "{$subRegion[1]} added." );
				}
			}
		}
	}

	/**
	 * @param $client
	 * @param $cookieJar
	 * @param $id
	 * @param $name
	 * @param $type
	 *
	 * @return array|mixed
	 */
	private function extractRegion( $client, $cookieJar, $id, $name, $type, $parent_id = 0 ) {
		if ( ! $id ) {
			$this->error( "Id for {$name} was not found." );

			return [ 'content' => [] ];
		}

		$result = $client->get( "https://999.md/ajax/options?offer_type=776&subcategory=real-estate%2Fapartments-and-rooms&{$type}={$id}", [ 'cookies' => $cookieJar ] );

		if ( $result->getStatusCode() != 200 ) {
			$this->error( "Request for {$id}=>{$name} resulted in status code: {$result->getStatusCode()}" );

			return [ 'content' => [] ];
		}

		$city = City::create( [ 'name' => $name, 'parent_id' => $parent_id ] );

		$this->info( "{$name} added." );

		return [ 'content' => json_decode( $result->getBody() ), 'parent' => $city ];
	}
}
