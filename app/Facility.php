<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
	protected $guarded = [ 'id', 'created_at', 'updated_at' ];

	protected $table = 'facilities';

	// Relations
	public function translation()
	{
		return $this->hasOne( FacilityTranslation::class );
	}

	public function objects()
	{
		return $this->belongsToMany( Object::class, 'facilities_objects' );
	}
}
