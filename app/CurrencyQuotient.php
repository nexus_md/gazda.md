<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyQuotient extends Model
{
	// Scopes

	/**
	 * @param $query
	 * @param Model $model
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeGetQuotient( $query, Model $model, $currency )
	{
		return $query->where( 'numerator', $model->currency->id )->where( 'denominator', $currency->id );
	}

	// Relations

	public function currency()
	{
		return $this->belongsTo( Currency::class, 'denominator' );
	}
}
