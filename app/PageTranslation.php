<?php

namespace App;

use App\Scopes\LocaleScope;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model {
	use Sluggable;

	protected $fillable = [
		'iso_code',
		'title',
		'content',
	];

	/**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	protected static function boot() {
		parent::boot();

		static::addGlobalScope( new LocaleScope() );
	}

	public function sluggable(): array {
		return [
			'slug' => [
				'source' => [
					'title',
				],
			],
		];
	}

	//Scopes

	/**
	 * @param $query
	 * @param $locale
	 *
	 * @return mixed
	 */
	public function scopeLocale( $query, $value ) {
		return $query->where( 'iso_code', $value );
	}

	public function scopeSlug( $query, $value ) {
		return $query->where( 'slug', $value );
	}
}
