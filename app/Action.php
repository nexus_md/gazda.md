<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Action extends Model {
	protected $guarded = [];

	protected $dates = [
		'from',
		'to',
	];

	// Setters
	public function setFromAttribute( $value ) {
		$this->attributes['from'] = $value ? Carbon::createFromFormat( 'd.m.Y', $value ) : null;
	}

	public function setToAttribute( $value ) {
		$this->attributes['to'] = $value ? Carbon::createFromFormat( 'd.m.Y', $value ) : null;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo( User::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function object() {
		return $this->belongsTo( Object::class );
	}
}
