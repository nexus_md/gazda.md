<?php

namespace App;

use App\Scopes\LocaleScope;
use Illuminate\Database\Eloquent\Model;

class ObjectTranslation extends Model
{
    protected $fillable = [
        'description',
        'text',
        'iso_code',
    ];

    public $timestamps = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LocaleScope());
    }

    /**
     * @param $query
     * @param $locale
     * @return mixed
     */
    public function scopeLocale($query, $value)
    {
        return $query->where('iso_code', $value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function object()
    {
        return $this->belongsTo(Object::class);
    }
}
