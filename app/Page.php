<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {
	protected $fillable = [
		'header',
		'footer',
	];

	//Scope

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeHeader( $query ) {
		return $query->where( 'header', 1 );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeFooter( $query ) {
		return $query->where( 'footer', 1 );
	}

	// Relations

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function translation() {
		return $this->hasOne( PageTranslation::class );
	}
}
