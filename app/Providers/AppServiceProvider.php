<?php

namespace App\Providers;

use App\Page;
use App\User;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->dbListen();

		view()->composer( 'layouts.*', function ( $view )
		{
			$view->withCurrentRoute( Route::currentRouteName() );
		} );

		view()->composer( 'layouts.app', function ( $view )
		{
			$headerLinks = Page::with( 'translation' )->header()->get();
			$footerLinks = Page::with( 'translation' )->footer()->get();

			$view->withHeaderLinks( $headerLinks )->withFooterLinks( $footerLinks );
		} );
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		if ( App::runningInConsole() ) {
			return;
		}

		$this->app->bind( User::class, function ( Application $app )
		{
			return User::findorfail( $app->get( 'request' )->route( 'user' ) );
		} );
	}

	public function dbListen()
	{
		\DB::listen( function ( $query )
		{
			\Log::info(
				$query->sql,
				$query->bindings,
				$query->time
			);
		} );
	}
}
