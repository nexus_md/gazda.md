<?php

namespace App\Providers;

use App\Repository\Currency\Currency;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Blade::directive( 'currency', function ( $model )
		{
			return '<?php echo e( App\Repository\Currency\Currency::convert( ' . e( $model ) . ' ) ) ?>';
		} );

		Currency::init( $this->app->request );
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
