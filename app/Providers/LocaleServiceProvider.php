<?php

namespace App\Providers;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class LocaleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.app', 'layouts.admin'], function ($view)
        {
            $locales = config('locales.locales');

            $path = substr(Request::path(), 3);

            foreach ($locales as $key => &$locale) {
                $locale = "{$key}/$path";
            }

            $view->withLocales($locales);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
