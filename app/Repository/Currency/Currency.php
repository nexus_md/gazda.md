<?php

namespace App\Repository\Currency;


use App\CurrencyQuotient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Currency
{
	/**
	 * @var string
	 */
	protected static $currency;

	/**
	 * @var array
	 */
	protected static $returnedQuotients = [];

	/**
	 * @var array
	 */
	protected static $exceptionQuotients = [];

	/**
	 * @var \App\Currency
	 */
	protected static $modelCurrency;

	/**
	 * Initialize Currency model
	 *
	 * @param Request $request
	 */
	public static function init( Request $request )
	{
		if ( $request->method() === 'GET' && $request->currency ) {
			self::$currency      = substr( $request->currency, 0, 3 );
			self::$modelCurrency = \App\Currency::acronym( self::$currency )->first();
		}
	}

	/**
	 * Convert Model currency to selected one.
	 *
	 * @param Model $model
	 *
	 * @return string
	 */
	public static function convert( Model $model ): string
	{
		if ( self::validate( $model ) ) {
			return self::prettify( $model );
		}

		$quotient = self::getReturnedQuotient( "{$model->currency->acronym}/" . self::$currency ) ?? CurrencyQuotient::getQuotient( $model, self::$modelCurrency )
		                                                                                                             ->first();

		if ( ! self::isValid( $model, $quotient ) ) {
			return self::prettify( $model );
		}

		self::storeReturnedQuotient( "{$model->currency->acronym}/" . self::$currency, $quotient );

		return self::prettify( $model, $quotient );
	}

	/**
	 * Check if it makes sense to convert.
	 *
	 * @param Model $model
	 *
	 * @return bool
	 */
	protected static function validate( Model $model ): bool
	{
		return ! self::$modelCurrency
		       || $model->currency->acronym === self::$currency
		       || self::findExceptionQuotient( "{$model->currency->acronym}/" . self::$currency );
	}

	/**
	 * Check if current quotient is useful.
	 *
	 * @param string $value
	 *
	 * @return bool
	 */
	protected static function findExceptionQuotient( string $value ): bool
	{
		return array_search( $value, self::$exceptionQuotients ) !== false;
	}

	/**
	 * Return a 'pretty' format of the modified price.
	 *
	 * @param Model $model
	 * @param CurrencyQuotient $quotient
	 *
	 * @return string
	 */
	protected static function prettify( Model $model, ?CurrencyQuotient $quotient = null ): string
	{
		$finalPrice = $model->price;
		$symbol     = $model->currency->symbol;

		if ( $quotient ) {
			$finalPrice = round( $finalPrice * $quotient->quotient, 2 );
			$symbol     = self::$modelCurrency->symbol;
		}

		return "{$finalPrice} {$symbol}";
	}

	/**
	 * Get cached quotient.
	 *
	 * @param Model $model
	 *
	 * @return CurrencyQuotient
	 */
	protected static function getReturnedQuotient( string $index ): ?CurrencyQuotient
	{
		return isset( self::$returnedQuotients[ $index ] ) ? self::$returnedQuotients[ $index ] : null;
	}

	/**
	 * Cache quotient.
	 *
	 * @param string $index
	 * @param CurrencyQuotient $quotient
	 */
	protected static function storeReturnedQuotient( string $index, ?CurrencyQuotient $quotient )
	{
		if ( ! isset( self::$returnedQuotients[ $index ] ) ) {
			self::$returnedQuotients[ $index ] = $quotient;
		}
	}

	/**
	 * Check if fetched quotient from DB is valid.
	 *
	 * @param Model $model
	 * @param CurrencyQuotient|null $quotient
	 *
	 * @return bool
	 */
	protected static function isValid( Model $model, ?CurrencyQuotient $quotient )
	{
		try {
			if ( ! $quotient ) {
				$index = "{$model->currency->acronym}/" . self::$currency;

				self::$exceptionQuotients[] = $index;

				throw new \Exception( "Quotient not found for '{$index}'" );
			}
		} catch ( \Exception $e ) {
			report( $e );

			return false;
		}

		return true;
	}
}