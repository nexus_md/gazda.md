<?php
/**
 * Created by PhpStorm.
 * User: Calin
 * Date: 12/6/2017
 * Time: 8:30 PM
 */

namespace App\Repository;


use App\Object;
use App\Gallery;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ObjectControllerHelper
{
	public static function store( Request $request )
	{
		$object = Object::create( $request->all() );

		if ( $request->plan_id ) {
			$object->plans()->attach( $request->plan_id );
		}

		if ( $request->number_id ) {
			$object->numbers()->attach( $request->number_id );
		}

		if ( $request->facilities ) {
			$object->facilities()->attach( $request->facilities );
		}

		$pathBase    = ImageHelper::saveImage( $request->image, Image::BASE );
		$pathGrid    = ImageHelper::saveImage( $request->image, Image::GRID );
		$pathSidebar = ImageHelper::saveImage( $request->image, Image::SIDEBAR );

		$object->images()->create( [ 'image' => $pathBase['name'], 'type' => Image::BASE ] );
		$object->images()->create( [ 'image' => $pathGrid['name'], 'type' => Image::GRID ] );
		$object->images()->create( [ 'image' => $pathSidebar['name'], 'type' => Image::SIDEBAR ] );

		if ( $request->video ) {
			$object->video()->create( [ 'url' => $request->video ] );
		}

		foreach ( (array) $request->images as $key => $image ) {
			$pathBase  = ImageHelper::saveImage( $image, Gallery::BASE );
			$pathThumb = ImageHelper::saveImage( $image, Gallery::THUMB );

			$pos = $request->image_positions ? $request->image_positions[ $key ] : $key;

			$gallery = $object->gallery()->create( [
				'image'    => $pathBase['name'],
				'type'     => Gallery::BASE,
				'position' => $pos,
			] );
			$object->gallery()->create( [
				'image'     => $pathThumb['name'],
				'type'      => Gallery::THUMB,
				'parent_id' => $gallery->id,
				'position'  => $pos,
			] );
		}

		return route( 'object.translation', [ app()->getLocale(), $object->id ] );
	}

	public static function update( Request $request, $id, User $user = null )
	{
		if ( $user ) {
			$object = Object::byUser( $user )->findorfail( $id );
		} else {
			$object = Object::findorfail( $id );
		}

		if ( $request->plan_id ) {
			$object->plans()->sync( $request->plan_id );
		} else {
			$object->plans()->detach();
		}

		if ( $request->number_id ) {
			$object->numbers()->sync( $request->number_id );
		} else {
			$object->numbers()->detach();
		}

		if ( $request->facilities ) {
			$object->facilities()->sync( $request->facilities );
		} else {
			$object->facilities()->detach();
		}

		foreach ( (array) $request->uploaded_images_positions as $key => $value ) {
			$thumb = $object->gallery()->findorfail( $value );
			$base  = $object->gallery()->findorfail( $thumb->parent_id );

			$thumb->position = $key;
			$base->position  = $key;

			$thumb->save();
			$base->save();
		}

		if ( $request->video ) {
			$object->video()->updateOrCreate( [ 'url' => $request->video ] );
		}

		$object->fill( $request->all() + [ 'publish' => $request->publish, 'featured' => $request->featured ] );
		$object->save();

		if ( $request->image ) {
			$images = $object->images()->get();

			foreach ( $images as $image ) {
				$path = ImageHelper::getDirs( $image->image );
				Storage::disk( 'public' )->delete( "images/{$path}/{$image->image}" );
			}

			$pathBase    = ImageHelper::saveImage( $request->image, Image::BASE );
			$pathGrid    = ImageHelper::saveImage( $request->image, Image::GRID );
			$pathSidebar = ImageHelper::saveImage( $request->image, Image::SIDEBAR );

			$object->images()->size( Image::BASE )->update( [ 'image' => $pathBase['name'] ] );
			$object->images()->size( Image::GRID )->update( [ 'image' => $pathGrid['name'] ] );
			$object->images()->size( Image::SIDEBAR )->update( [ 'image' => $pathSidebar['name'] ] );
		}

		$galleryCount = $object->gallery()->count() / 2;

		$addedGallery = [];

		foreach ( (array) $request->images as $key => $image ) {
			$pathBase  = ImageHelper::saveImage( $image, Gallery::BASE );
			$pathThumb = ImageHelper::saveImage( $image, Gallery::THUMB );

			$pos = $request->image_positions ? $request->image_positions[ $key ] : $key;
			$pos += $galleryCount;

			$gallery = $object->gallery()->create( [
				'image'    => $pathBase['name'],
				'type'     => Gallery::BASE,
				'position' => $pos,
			] );

			$thumb = $object->gallery()->create( [
				'image'     => $pathThumb['name'],
				'type'      => Gallery::THUMB,
				'parent_id' => $gallery->id,
				'position'  => $pos,
			] );

			$addedGallery[] = (object) [ 'id' => $thumb->id, 'image' => $thumb->image, 'pos' => $pos ];
		}

		uasort( $addedGallery, function ( $a, $b )
		{
			if ( $a->pos == $b->pos ) {
				return 0;
			}

			return $a->pos < $b->pos ? - 1 : 1;
		} );

		return view( 'admin.object.partials.uploaded_images' )->withGallery( $addedGallery )->render();
	}

	public static function destroy( $id )
	{
		$object = Object::findorfail( $id );

		$images  = $object->images()->get();
		$gallery = $object->gallery()->get();

		foreach ( $images as $image ) {
			$path = ImageHelper::getDirs( $image->image );
			Storage::disk( 'public' )->delete( "images/{$path}/{$image->image}" );
		}

		foreach ( $gallery as $image ) {
			$path = ImageHelper::getDirs( $image->image );
			Storage::disk( 'public' )->delete( "images/{$path}/{$image->image}" );
		}

		$object->delete();
	}

	public static function destroyImageGallery( $id )
	{
		$thumb = Gallery::findorfail( $id );
		$base  = Gallery::findorfail( $thumb->parent_id );

		$thumbPath = ImageHelper::getDirs( $thumb->image );
		$basePath  = ImageHelper::getDirs( $base->image );

		Storage::disk( 'public' )->delete( "images/{$thumbPath}/{$thumb->image}" );
		Storage::disk( 'public' )->delete( "images/{$basePath}/{$base->image}" );

		$thumb->delete();
		$base->delete();
	}
}