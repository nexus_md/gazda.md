<?php
/**
 * Created by PhpStorm.
 * User: Calin
 * Date: 5/18/2018
 * Time: 3:38 PM
 */

namespace App\Repository;


class Reflection {
	static public function getConstants( $class ) {
		return ( new \ReflectionClass( $class ) )->getConstants();
	}
}