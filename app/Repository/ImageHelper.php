<?php

namespace App\Repository;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImageHelper
{
	/**
	 * @param $file
	 * @param $size
	 *
	 * @return array|bool
	 */
	static public function saveImage( $file, $width = null, $height = null )
	{
		$img = \Image::make( $file );

		if ( $width != null ) {
			$img->fit( (int) $width, (int) ( ! $height ? $width / 1.5 : $height ), function ( $constraint )
			{
				$constraint->upsize();
			} );
		}

		$dir  = new Dir( str_random( 32 ) );
		$path = $dir->getPath();

		$img->save( "{$path['path']}/{$path['name']}" );

		return $path;
	}

	/**
	 * @param $imageName
	 * @param $dir
	 */
	static public function removeImage( $imageName )
	{
		$path = self::getDirs( $imageName );

		Storage::disk( 'public' )->delete( "images/{$path}/{$imageName}" );
	}

	/**
	 * @param $imageName
	 *
	 * @return string
	 */
	static public function getDirs( $imageName )
	{
		$hash  = ( new Str( $imageName ) )->hashCode();
		$first = $hash & 0xff;
		$scnd  = ( $hash >> 8 ) & 0xff;

		return "{$first}/{$scnd}";
	}

	/**
	 * @param $html
	 */
	static public function deleteCkAddedImages( $html )
	{
		$document = new \DOMDocument();
		$document->loadHTML( $html );

		$tags = $document->getElementsByTagName( 'img' );

		foreach ( $tags as $tag ) {
			$src = $tag->getAttribute( 'src' );

			try {
				$name = app( 'router' )->getRoutes()->match( app( 'request' )->create( $src ) )->getName();

				if ( $name != 'image' ) {
					continue;
				}
			} catch ( NotFoundHttpException $e ) {
				continue;
			}

			$array = explode( '/', $src );

			$imageName = last( $array );
			$path      = self::getDirs( $imageName );

			Storage::disk( 'public' )->delete( "images/{$path}/{$imageName}" );
		}
	}
}