<?php

namespace App\Repository;


class Dir
{
	private $name;
	private $path;

	/**
	 * Dir constructor.
	 *
	 * @param $name
	 * @param int $path
	 */
	public function __construct( $name, $path = 0 )
	{
		$this->name = $name;

		if ( $path ) {
			$this->path = $path;
		} else {
			$this->path = 'images';
		}
	}

	/**
	 * @return array|bool
	 */
	public function getPath()
	{
		$path = $this->validateAndReturnPath( $this->name );

		return ( $path ? $path : $this->validateAndReturnPath( microtime() . $this->name ) );
	}

	/**
	 * @param $name
	 *
	 * @return array|bool
	 */
	private function validateAndReturnPath( $name )
	{
		$hash  = ( new Str( $name ) )->hashCode();
		$first = $hash & 0xff;
		$scnd  = ( $hash >> 8 ) & 0xff;
		$path  = storage_path( "app/public/{$this->path}/{$first}/{$scnd}" );

		if ( ! is_dir( $path ) ) {
			mkdir( $path, 0777, true );
		} else if ( file_exists( "{$path}/{$name}" ) ) {
			return false;
		}

		return [ "path" => $path, "name" => $name ];
	}
}