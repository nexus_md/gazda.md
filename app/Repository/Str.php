<?php

namespace App\Repository;

class Str
{
    private $value;
    private $length;
    private $cachedHashValue;

    /**
     * Str constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->length = 32;
        $this->value = substr($value, 0, $this->length);
    }

    /**
     * @return float|int
     */
    public function hashCode()
    {
        if ($this->cachedHashValue) {
            return $this->cachedHashValue;
        }

        $hash = 0;
        $max = pow(2, 31);

        for ($i = 0; $i < $this->length; $i++) {
            $hash += ord($this->value[$i]) * pow(31, $this->length - $i - 1);
            if ($hash > $max) {
                $hash %= $max;
            }
        }

        return $this->cachedHashValue = $hash;
    }
}