<?php
/**
 * Created by PhpStorm.
 * User: Calin
 * Date: 11/4/2017
 * Time: 8:19 PM
 */

namespace App\Repository\Traits;

use App\User;
use Illuminate\Support\Facades\Auth;

trait Redirect {
	/**
	 * @return string
	 */
	protected function redirectTo() {
		$user = Auth::user();

		if ( $user->role == User::ADMIN ) {
			return route( 'admin.home', app()->getLocale() );
		}

		if ( $user->role == User::SIMPLE ) {
			return route( 'apartments.create', app()->getLocale() );
		}

		return route( 'user.home', [ app()->getLocale(), $user->id ] );
	}
}