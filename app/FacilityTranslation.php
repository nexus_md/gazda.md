<?php

namespace App;

use App\Scopes\LocaleScope;
use Illuminate\Database\Eloquent\Model;

class FacilityTranslation extends Model
{
	protected $fillable = [ 'name', 'iso_code' ];

	/**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope( new LocaleScope() );
	}

	// Scopes
	public function scopeLocale( $query, $value )
	{
		return $query->where( 'iso_code', $value );
	}
}
