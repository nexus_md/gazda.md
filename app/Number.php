<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Number extends Model {
	protected $fillable = [
		'number',
	];

	public function scopeNotFree( $query ) {
		return $query->where( 'user_id', '!=', Auth::user()->id );
	}

	/**
	 * @param $query
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function scopeSearch( $query, Request $request ) {
		if ( $request->search ) {
			return $query->where( 'number', 'like', "%{$request->search}%" )
			             ->orWhereRaw( "user_id IN (SELECT id FROM users WHERE name LIKE ?)", [ "%{$request->search}%" ] );
		}
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeRealtorOrNull( $query ) {
		return $query->where( 'user_id', null )
		             ->orWhereRaw( 'user_id = (SELECT id FROM users WHERE id = numbers.user_id AND role = ?)', [ User::REALTOR ] );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeObjectCount( $query ) {
		return $query->selectRaw( 'id index_id, id, user_id, number, ( SELECT count(*) FROM objects WHERE id in (SELECT object_id FROM number_object WHERE number_id = index_id) ) object_count' );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo( User::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function objects() {
		return $this->belongsToMany( Object::class );
	}
}
