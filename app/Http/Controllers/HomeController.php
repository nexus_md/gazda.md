<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\PlanRequest;
use App\Image;
use App\Object;
use App\Plan;
use App\Repository\ImageHelper;
use Illuminate\Http\Request;
use App\Comment;

class HomeController extends Controller
{
	/**
	 * Main page
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view( 'home' )
			->withFeaturedObjects( Object::featured()->limit( 6 )->orderByDesc( 'id' )->get() )
			->withLastObjects( Object::grid()->limit( 4 )->orderByDesc( 'id' )->get() )
			->withPerHourObjects( Object::perHour()->limit( 4 )->orderByDesc( 'id' )->get() )
			->withMarkers( Object::select( 'lat', 'lng' )->orderByDesc( 'id' )->limit( 100 )->get()->toArray() )
			->withCities( City::parent()->orderBy( 'id' )->get() )
            ->withComment( Comment::orderByDesc( 'id' )->where('publish', 1)->first() );
	}

	/**
	 * @param Request $request
	 *
	 * @return array|void
	 */
	public function ajaxGetRegions( Request $request )
	{
		if ( ! $request->ajax() ) {
			abort( 404 );
		}

		$regions = City::with( 'regions.regions:id,parent_id' )->findorfail( $request->city_id )->regions;

		$ids = [];

		foreach ( $regions as $subRegions ) {
			if ( $subRegions->regions ) {
				foreach ( $subRegions->regions as $region ) {
					$ids[] = $region->id;
				}
			} else {
				$ids[] = $request->id;
			}
		}

		return $ids;
	}
}
