<?php

namespace App\Http\Controllers;

use App\City;
use App\Currency;
use App\Facility;
use App\Http\Controllers\Helpers\ObjectControllerHelper;
use App\Http\Requests\ObjectRequest;
use App\Object;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repository\ObjectControllerHelper as OCHHelper;
use Illuminate\Support\Facades\Auth;

class ObjectController extends Controller
{
    use ObjectControllerHelper;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale, Request $request)
    {
        $objects = Object::grid()
                         ->filter($request)
                         ->orderByRaw("FIELD(urgent , 1) DESC")
                         ->orderByDesc('id')
                         ->paginate(12);

        $cities = config('city.default');

        return view('apartments')
            ->withObjects($objects)
            ->withCities(City::selectCities($cities)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

        if ( ! $user) {
            abort(404);
        }

        return view('create_object')
            ->withCities(City::parent()->get())
            ->withCurrency(Currency::get())
            ->withFacilities(Facility::with('translation')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ObjectRequest $request)
    {
        $user = Auth::user();

        if ( ! $user) {
            abort(404);
        }

        $request->request->add(['validate' => 0, 'user_id' => $user->id, 'owner_']);

        OCHHelper::store($request);

//		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $locale, $slug)
    {
        if ($request->ajax()) {
            return view('partials.apartments.map_card')->withValue(Object::grid()
                                                                         ->findOrFail($slug));
        }

        $object = Object::product($slug)->firstOrFail();

        $exceptions = ['apartment_nr', 'entrance', 'object_insights', 'owner_name', 'owner_number', 'owner_email', 'currency_id', 'outer_type'];

        $properties = $this->getProperties($object);
        $properties = collect($properties)->except($exceptions);

        $this->getOrSetCookie('apartment_links', $slug);

        return view('apartment')
            ->withObject($object)
            ->withProperties($properties)
            ->withSeenApartments($this->getObjectsByCookie('apartment_links'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Async load of filtered objects.
     *
     * @param Request $request
     * @param $locale
     *
     * @return array|bool
     */
    public function getMarkers(Request $request, $locale)
    {
        if ( ! $request->ajax()) {
            return;
        }

        return Object::map()
                     ->filter($request)
                     ->orderByDesc('id')
                     ->get();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|void
     */
    public function addWishList(Request $request)
    {
        if ( ! $request->ajax()) {
            return;
        }

        $validator = \Validator::make($request->all(), [
            'slug' => 'required|string',
        ]);

        if ($validator->fails()) {
            return new JsonResponse($validator->messages(), 422);
        }

        $this->setOrDeleteCookie('wish-list', $request->slug);
    }

    public function wishList($locale)
    {
        $wishList = $this->getObjectsByCookie('wish-list');

        return view('wish_list')->withWishList($wishList);
    }
}
