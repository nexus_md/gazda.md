<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
	/**
	 * Extract only first level of regions.
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getRegions( $id )
	{
		return view( 'partials.form.tree_sub_regions' )
			->withCities( City::child( $id )->selectRaw( '*, null as regions' )->get() )
			->withChecked( '' )
			->withGrid( 'col-11 offset-1' )
			->render();
	}

	/**
	 * Return only city collection.
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getRawRegions( $id )
	{
		return City::child( $id )->get();
	}

	/**
	 * Extract all levels of regions.
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getTreeSubRegions( $id )
	{
		return view( 'partials.form.tree_sub_regions' )
			->withCities( City::with( 'regions.regions' )
			                  ->findOrFail( $id )->regions )
			->withChecked( 'checked' )
			->withGrid( 'col-11 offset-1' )
			->render();
	}

	/**
	 * Get only parent regions.
	 *
	 * @return mixed
	 */
	public function getParentRegions()
	{
		return view( 'partials.form.tree_sub_regions' )
			->withCities( City::parent()->selectRaw( '*, null as regions' )->get() )
			->withChecked( '' )
			->withGrid( 'col-12' )
			->render();
	}
}
