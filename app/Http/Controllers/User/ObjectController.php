<?php

namespace App\Http\Controllers\User;

use App\City;
use App\Currency;
use App\Facility;
use App\Gallery;
use App\Http\Requests\ObjectRequest;
use App\Http\Requests\ObjectTranslationRequest;
use App\Image;
use App\Object;
use App\Repository\ObjectControllerHelper;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class ObjectController extends Controller
{
	protected $user;

	public function __construct( User $user )
	{
		$this->middleware( 'auth' );
		$this->middleware( 'owner' );

		$this->user = $user;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$objects = Object::byUser( $this->user )
		                 ->with( 'city.city.city' )
		                 ->orderBy( 'id', 'DESC' )
		                 ->paginate( 20 );

		return view( 'user.object.index' )->withData( $objects );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'user.object.create' )
			->withCities( City::parent()->get() )
			->withCurrency( Currency::get() )
			->withFacilities( Facility::with( 'translation' )->get() );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( ObjectRequest $request )
	{
		$request->request->add( [ 'validate' => 0, 'user_id' => $this->user->id ] );

		return ObjectControllerHelper::store( $request );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $user, $id )
	{
		$object = Object::byUser( $this->user )->with( [
			'image'   => function ( $query )
			{
				return $query->size( Image::BASE )->first();
			},
			'gallery' => function ( $query )
			{
				return $query->size( Gallery::THUMB )->orderBy( 'position' );
			},
			'city.city.city',
			'city.city.regions',
			'city.city.city.regions',
			'facilities',
		] )->findorfail( $id );

		return view( 'user.object.create' )
			->withFormData( $object )
			->withCities( City::parent()->get() )
			->withFacilities( Facility::with( 'translation' )->get() );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $locale, $user, $id )
	{
		$request->request->add( [ 'validate' => 0 ] );

		return ObjectControllerHelper::update( $request, $id, $this->user );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id )
	{
		//
	}

	/**
	 * Show the form for editing | creating the translation for the specified resource.
	 *
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function translation( $locale, $user, $id )
	{
		Object::byUser( $this->user )->findorfail( $id );

		return view( 'user.object.create_translation' )->withId( $id );
	}

	/**
	 * Update | create translation for the specified resource.
	 *
	 * @param $locale
	 * @param ObjectTranslationRequest $request
	 * @param $id
	 */
	public function processTranslation( ObjectTranslationRequest $request, $locale, $user, $id )
	{
		$object = Object::byUser( $this->user )->findorfail( $id );

		$object->validate = 0;
		$object->save();

		$object->translation()->updateOrCreate( [
			'iso_code' => $request->input( 'locale' ),
		], $request->all() );
	}

	/**
	 * Get current translation for the specified resource.
	 *
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getTranslation( Request $request, $locale, $user, $id )
	{
		$object = Object::byUser( $this->user )->findorfail( $id );

		return $object->translation()->withoutGlobalScopes()->locale( $request->input( 'locale' ) )->first();
	}

	/**
	 * Destroy current image from gallery.
	 *
	 * @param $locale
	 * @param $id
	 */
	public function destroyImageGallery( $locale, $user, $id )
	{
		$object = Gallery::findorfail( $id )->object()->byUser( $this->user )->first();

		$object->validate = 0;
		$object->save();

		ObjectControllerHelper::destroyImageGallery( $id );
	}
}
