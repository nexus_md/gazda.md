<?php

namespace App\Http\Controllers\User;

use App\Object;
use App\Plan;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class PlanController extends Controller
{
	protected $user;

	public function __construct( User $user )
	{
		$this->middleware( 'auth' );
		$this->middleware( 'owner' );

		$this->user = $user;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id )
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id )
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id )
	{
		//
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function getPlans( Request $request )
	{
		return Plan::search( $request )->orderBy( 'id', 'DESC' )->paginate( 20 );
	}

	/**
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getObjectPlans( $locale, $user, $id )
	{
		return Object::byUser( $this->user )
		             ->findorfail( $id )
		             ->plans()
		             ->select( 'id', 'name' )
		             ->orderBy( 'id', 'DESC' )
		             ->get();
	}
}
