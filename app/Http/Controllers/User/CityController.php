<?php

namespace App\Http\Controllers\User;

use App\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
	protected $user;

	public function __construct( User $user )
	{
		$this->middleware( 'auth' );
		$this->middleware( 'owner' );

		$this->user = $user;
	}

	public function getRegions( $locale, $user, $id )
	{
		return City::child( $id )->get();
	}
}
