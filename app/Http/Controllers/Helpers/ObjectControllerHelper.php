<?php
/**
 * Created by PhpStorm.
 * User: Calin
 * Date: 5/28/2018
 * Time: 3:39 PM
 */

namespace App\Http\Controllers\Helpers;


use App\Object;

trait ObjectControllerHelper
{
    public function toMapFormat($objects)
    {
        $markets = [];

        foreach ($objects as $value) {
            $markets[] = [
                'lat' => $value->lat,
                'lng' => $value->lng,
                'id'  => $value->id,
            ];
        }

        return $markets;
    }

    public function getProperties($object)
    {
        $properties = [];

        foreach ($object->toArray() as $key => $value) {
            if ( ! is_array($value)
                 && $key != 'id'
                 && $key != 'user_id'
                 && $key != 'city_id'
                 && $key != 'address'
                 && $key != 'slug'
                 && $key != 'lat'
                 && $key != 'lng'
                 && $key != 'price'
                 && $key != 'featured'
                 && $key != 'publish'
                 && $key != 'validate'
                 && $key != 'created_at'
                 && $key != 'updated_at'
                 && $key != 'translation'
                 && $key != 'image') {
                $properties[$key] = $value;
            }
        }

        return $properties;
    }

    public function getOrSetCookie($name, $slug)
    {
        $cookie = \Cookie::get($name);

        if (is_array($cookie)) {

            $index = array_search($slug, $cookie);

            if ($index !== false) {
                array_splice($cookie, $index, 1);
            }

            array_unshift($cookie, $slug);

            if (count($cookie) > 20) {
                array_pop($cookie);
            }

        } else {
            $cookie = [$slug];
        }

        \Cookie::queue($name, $cookie);
    }

    public function setOrDeleteCookie($name, $slug)
    {
        $cookie = \Cookie::get($name);

        if ( ! is_array($cookie)) {
            $cookie = [$slug];

            \Cookie::queue($name, $cookie);

            return;
        }

        $index = array_search($slug, $cookie);

        if ($index !== false) {
            array_splice($cookie, $index, 1);

            \Cookie::queue($name, $cookie);

            return;
        }

        if (count($cookie) > 20) {
            array_pop($cookie);
        }

        array_unshift($cookie, $slug);

        \Cookie::queue($name, $cookie);
    }

    public function getObjectsByCookie($name)
    {
        $cookie = \Cookie::get($name);

        if (is_array($cookie) && count($cookie)) {
            return Object::grid()
                         ->lastSeen($cookie)
                         ->limit(20)
                         ->get();
        }

        return [];
    }
}