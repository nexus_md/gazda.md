<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageRequest;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view( 'admin.pages.index' )
			->withData( Page::with( 'translation' )->orderByDesc( 'created_at' )->paginate( 20 ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'admin.pages.create' )
			->withLocales( config( 'locales.locales' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( PageRequest $request )
	{
		$page = Page::updateOrCreate( [
			'id' => $request->page_id,
		], $request->all() );

		$page->translation()->updateOrCreate( [
			'iso_code' => $request->iso_code,
		], $request->all() );

		return $page->id;
	}

	/**
	 * @param $locale
	 * @param $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 */
	public function show( Request $request, $locale, $id )
	{
		return Page::findorfail( $id )
		           ->translation()
		           ->withoutGlobalScopes()
		           ->locale( $request->input( 'iso_code' ) )
		           ->first();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $id )
	{
		return view( 'admin.pages.create' )
			->withFormData( Page::with( 'translation' )->findorfail( $id ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( PageRequest $request, $locale, $id )
	{
		$page = Page::findorfail( $id );

		$page->fill( $request->all() );
		$page->save();

		$page->translation()->updateOrCreate( [
			'iso_code' => $request->input( 'iso_code' ),
		], $request->all() );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $locale, $id )
	{
		Page::findorfail( $id )->delete();
	}
}
