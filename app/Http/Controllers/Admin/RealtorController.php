<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RealtorRequest;
use App\Mail\AccountCreated;
use App\Number;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class RealtorController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request )
	{
		$data = User::realtor()
		            ->search( $request )
		            ->objectCount()
		            ->orderBy( 'id', 'DESC' )
		            ->paginate( 20 );

		return view( 'admin.realtor.index' )->withData( $data );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'admin.realtor.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( RealtorRequest $request )
	{
		$user = new User();

		$data = $request->all();

		$data['password'] = str_random( 6 );

		Mail::to( $data['email'] )->queue( new AccountCreated( $data ) );

		$data['password'] = bcrypt( $data['password'] );

		$user->fill( $data );
		$user->save();

		if ( $request->number_id ) {
			Number::whereIn( 'id', $request->number_id )->update( [ 'user_id' => $user->id ] );
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $id )
	{
		return view( 'admin.realtor.create' )->withFormData( User::realtor()->findorfail( $id ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( $locale, RealtorRequest $request, $id )
	{
		$user = User::realtor()->findorfail( $id );

		$user->fill( $request->all() );
		$user->save();

		if ( $request->number_id ) {
			Number::whereIn( 'id', $request->number_id )->update( [ 'user_id' => $user->id ] );
		} else {
			Number::where( 'user_id', $user->id )->update( [ 'user_id' => Auth::user()->id ] );
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $locale, $id )
	{
		$realtor = User::realtor()->findorfail( $id );

		$realtor->delete();
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function getUsers( Request $request )
	{
		return User::search( $request )->orderBy( 'id', 'DESC' )->paginate( 20 );
	}

	/**
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getNumberUser( $locale, $id )
	{
		return Number::findorfail( $id )->user()->select( 'id', 'name' )->first();
	}
}
