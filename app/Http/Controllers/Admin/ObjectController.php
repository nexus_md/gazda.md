<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Currency;
use App\Facility;
use App\ObjectTranslation;
use App\Plan;
use App\Gallery;
use App\Http\Requests\ObjectRequest;
use App\Http\Requests\ObjectTranslationRequest;
use App\Object;
use App\Image;
use App\Repository\ObjectControllerHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ObjectController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request, $locale )
	{
		$objects = Object::with( [
			'city.city.city',
            'translation'
		] )->filter( $request )->orderByDesc( 'id' )->paginate( 20 );

		return view( 'admin.object.index' )->withData( $objects );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'admin.object.create' )
			->withCities( City::parent()->get() )
			->withCurrency( Currency::get() )
			->withFacilities( Facility::with( 'translation' )->get() );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( ObjectRequest $request )
	{
		$request->request->add( [ 'validate' => 1 ] );

		return ObjectControllerHelper::store( $request );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $id )
	{
		$object = Object::with( [
			'image'   => function ( $query )
			{
				return $query->size( Image::BASE )->first();
			},
			'gallery' => function ( $query )
			{
				return $query->size( Gallery::THUMB )->orderBy( 'position' );
			},
			'city.city.city',
			'city.city.regions',
			'city.city.city.regions',
			'facilities',
		] )->findorfail( $id );

		return view( 'admin.object.create' )
			->withFormData( $object )
			->withCities( City::parent()->get() )
			->withCurrency( Currency::get() )
			->withFacilities( Facility::with('translation')->get() );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return array
	 */
	public function update( $locale, ObjectRequest $request, $id )
	{
		$request->request->add( [ 'validate' => 1 ] );

		return ObjectControllerHelper::update( $request, $id );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $locale, $id )
	{
		ObjectControllerHelper::destroy( $id );
	}

	/**
	 * Show the form for editing | creating the translation for the specified resource.
	 *
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function translation( $locale, $id )
	{
		return view( 'admin.object.create_translation' )->withId( $id );
	}

	/**
	 * Update | create translation for the specified resource.
	 *
	 * @param $locale
	 * @param ObjectTranslationRequest $request
	 * @param $id
	 */
	public function processTranslation( $locale, ObjectTranslationRequest $request, $id )
	{
		$object = Object::findorfail( $id );

		$object->translation()->updateOrCreate( [
			'iso_code' => $request->input( 'locale' ),
		], $request->all() );
	}

	/**
	 * Get current translation for the specified resource.
	 *
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getTranslation( $locale, Request $request, $id )
	{
		$object = Object::findorfail( $id );

		return $object->translation()->withoutGlobalScopes()->locale( $request->input( 'locale' ) )->first();
	}

	/**
	 * Destroy current image from gallery.
	 *
	 * @param $locale
	 * @param $id
	 */
	public function destroyImageGallery( $locale, $id )
	{
		ObjectControllerHelper::destroyImageGallery( $id );
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function getObjects( Request $request )
	{
		return Object::with( 'city' )
		             ->select( 'id', 'city_id', 'address' )
		             ->search( $request )
		             ->orderBy( 'id', 'DESC' )
		             ->paginate( 20 );
	}

	/**
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getPlanObjects( $locale, $id )
	{
		return Plan::findorfail( $id )->objects()->select( 'id', 'city_id', 'address' )->with( 'city' )->get();
	}

    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
	public function setUrgentState (Request $request, $locale, $id) {
        $object = Object::findorfail( $id );

        $object->urgent = $request->value;

        $object->save();

        return new JsonResponse([
            'success' => true
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
	public function setPublishedState (Request $request, $locale, $id) {
        $object = Object::findorfail( $id );

        $object->publish = $request->value;

        $object->save();

        return new JsonResponse([
            'success' => true
        ]);
    }
}
