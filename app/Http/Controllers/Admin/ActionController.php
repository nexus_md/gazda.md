<?php

namespace App\Http\Controllers\Admin;

use App\Action;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActionController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	public function index()
	{
		$actions = Action::with( 'user:id,name', 'object:id,address,city_id', 'object.city:id,name' )
		                 ->orderBy( 'id', 'DESC' )
		                 ->paginate( 20 );

		return view( 'admin.action.index' )->withData( $actions );
	}
}
