<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PlanRequest;
use App\Object;
use App\Plan;
use App\Image;
use App\Repository\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PlanController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$plans = Plan::orderBy( 'id', 'DESC' )->paginate( 20 );

		return view( 'admin.plan.index' )->withData( $plans );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create( $locale )
	{
		return view( 'admin.plan.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( PlanRequest $request )
	{
		$data = $request->all();

		$path = ImageHelper::saveImage( $request->image );

		$data['image'] = $path['name'];

		$plan = Plan::create( $data );

		if ( $request->object_id ) {
			$plan->objects()->attach( $request->object_id );
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $id )
	{
		return view( 'admin.plan.create' )->withFormData( Plan::findorfail( $id ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( $locale, PlanRequest $request, $id )
	{
		$plan = Plan::findorfail( $id );

		if ( $request->object_id ) {
			$plan->objects()->sync( $request->object_id );
		} else {
			$plan->objects()->detach();
		}

		$data = $request->all();

		if ( $request->image ) {
			$path = ImageHelper::getDirs( $plan->image );

			Storage::disk( 'public' )->delete( "images/{$path}/{$plan->image}" );

			$path = ImageHelper::saveImage( $request->image, Image::BASE );

			$data['image'] = $path['name'];
		}

		$plan->fill( $data );

		$plan->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $locale, $id )
	{
		$plan = Plan::findorfail( $id );

		$path = ImageHelper::getDirs( $plan->image );

		Storage::disk( 'public' )->delete( "images/{$path}/{$plan->image}" );

		$plan->delete();
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function getPlans( Request $request )
	{
		return Plan::search( $request )->orderBy( 'id', 'DESC' )->paginate( 20 );
	}

	/**
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getObjectPlans( $locale, $id )
	{
		return Object::findorfail( $id )->plans()->select( 'id', 'name' )->orderBy( 'id', 'DESC' )->get();
	}
}
