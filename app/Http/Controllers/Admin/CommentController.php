<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware( 'auth' );
        $this->middleware( 'admin' );
    }

    public function index(){
        return view( 'admin.comment.index' )->withComments( Comment::orderByDesc( 'id' )->paginate( 20 ) );
    }

    public function store( CommentRequest $request )
    {
        Comment::create( $request->all() );

        return redirect()->back()->with(['header' => 'Отлично!', 'body' => 'Спасибо за отзыв!', 'type' => 'success']);
    }
    // $lang is useless but when it's deleted the route goes apeshit so leave it there please :)

    // No destroy because no time to debug y it doesn't work :\
    public function delete( Request $request )
    {
        Comment::find($request->id)->delete();
        return redirect()->back()->with(['header' => 'Отлично!', 'body' => 'Комментарии удален', 'type' => 'success']);
    }

    // Ugly but update doesn't work for some reason :/

    public function approve( Request $request ){
        $comment = Comment::find($request->id);
        $comment->publish = 1;
        $comment->save();
        return redirect()->back()->with(['header' => 'Отлично!', 'body' => 'Комментарии опубликован', 'type' => 'success']);
    }

    public function disable( Request $request ){
        $comment = Comment::find($request->id);
        $comment->publish = 0;
        $comment->save();
        return redirect()->back()->with(['header' => 'Отлично!', 'body' => 'Комментарии скрыт', 'type' => 'success']);
    }

}
