<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NumberRequest;
use App\Number;
use App\Object;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NumberController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$numbers = Number::with( 'user' )
		                 ->objectCount()
		                 ->orderBy( 'id', 'DESC' )
		                 ->paginate( 20 );

		return view( 'admin.number.index' )->withData( $numbers );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'admin.number.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( NumberRequest $request )
	{
		Number::create( $request->all() );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $id )
	{
		return view( 'admin.number.create' )->withFormData( Number::findorfail( $id ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( $locale, NumberRequest $request, $id )
	{
		$number = Number::findorfail( $id );
		$number->fill( $request->all() );
		$number->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $locale, $id )
	{
		Number::findorfail( $id )->delete();
	}

	/**
	 * Get paginated resource.
	 *
	 * @param Request $request
	 */
	public function getNumbers( Request $request )
	{
		$numbers = Number::with( 'user:id,name' )
		                 ->realtorOrNull()
		                 ->objectCount()
		                 ->search( $request )
		                 ->orderBy( 'id', 'DESC' )
		                 ->paginate( 20 );

		return $numbers;
	}

	/**
	 * Get numbers based on current used id.
	 *
	 * @param $locale
	 * @param $id
	 */
	public function getUserNumbers( $locale, $id )
	{
		return User::findorfail( $id )->numbers()->orderBy( 'id', 'DESC' )->get();
	}

	/**
	 * @param $locale
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getObjectNumber( $locale, $id )
	{
		return Object::findorfail( $id )->numbers()->orderBy( 'id', 'DESC' )->get();
	}
}
