<?php

namespace App\Http\Controllers\Admin;

use App\Facility;
use App\Http\Requests\FacilityRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacilityController extends Controller
{
	public function __construct()
	{
		$this->middleware( 'auth' );
		$this->middleware( 'admin' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view( 'admin.facility.index' )
			->withData( Facility::orderByDesc( 'id' )->paginate( 20 ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'admin.facility.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( FacilityRequest $request )
	{
		$facility = Facility::updateOrCreate( [
			'id' => $request->facility_id,
		], [] );

		$facility->translation()->updateOrCreate( [
			'iso_code' => $request->iso_code,
		], $request->all() );

		return $facility->id;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( Request $request, $locale, $id )
	{
		return Facility::findorfail( $id )->translation()->withoutGlobalScopes()->locale( $request->iso_code )->first();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $locale, $id )
	{
		return view( 'admin.facility.create' )
			->withFormData( Facility::with( 'translation' )->findOrFail( $id ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( FacilityRequest $request, $locale, $id )
	{
		$facility = Facility::findorfail( $id );

		$facility->translation()->updateOrCreate( [
			'iso_code' => $request->iso_code,
		], $request->all() );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $locale, $id )
	{
		Facility::findorfail( $id )->delete();
	}
}
