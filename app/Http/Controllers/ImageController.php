<?php

namespace App\Http\Controllers;

use App\Repository\ImageHelper;
use Illuminate\Http\Request;

class ImageController extends Controller
{
	/**
	 * @param $imageName
	 *
	 * @return mixed
	 */
	public function index( $imageName )
	{
		if ( strlen( $imageName ) < 32 ) {
			abort( 404 );
		}

		$path = ImageHelper::getDirs( $imageName );

		$fileRef = storage_path( "app/public/images/{$path}/{$imageName}" );

		if ( ! file_exists( $fileRef ) ) {
			abort( 404 );
		}

		return response()->file( $fileRef,
			[
				'Content-Type' => 'image/jpeg',
			] );
	}
}
