<?php

namespace App\Http\Controllers;

use App\City;
use App\Object;
use Illuminate\Http\Request;

class ApartmentController extends Controller {

	/**
	 * Return all apartments in paginated manner.
	 *
	 * @return mixed
	 */
	public function index( $locale, Request $request ) {
		$objects = Object::grid()->filter( $request )->orderByDesc( 'created_at' )->paginate( 12 );

		$markets = [];

		foreach ( $objects as $value ) {
			$markets[] = [ 'lat' => $value->lat, 'lng' => $value->lng ];
		}

		return view( 'apartments' )
			->withObjects( $objects )
			->withCities( City::orderBy( 'name' )->get() )
			->withMarkers( $markets );
	}

	public function show( $locale, $slug ) {
		$object = Object::product( $slug )->firstOrFail();

		return view( 'apartment' )
			->withObject( $object );
	}
}
