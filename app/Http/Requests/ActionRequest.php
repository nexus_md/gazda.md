<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActionRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		$request = $this->request->all();

		return [
			'advance_price' => $request['advance_price'] ? 'integer' : '',
			'passive_price' => $request['passive_price'] ? 'integer' : '',
			'total_price'   => $request['total_price'] ? 'integer' : '',
			'to'            => $request['from'] && $request['to'] ? 'date_format:d.m.Y|after:from' : '',
		];
	}
}
