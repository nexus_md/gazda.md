<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObjectRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'area'              => 'required|integer',
			'entrance'          => 'integer',
			'room_count'        => 'required|integer',
			'floor'             => 'required|integer',
			'image'             => [
				'image',
				'max:5120',
			],
			'images.*'          => 'image|max:5120',
			'type'              => 'required|integer',
			'construction_type' => 'required|integer',
			'city_id'           => 'required',
			'address'           => 'required',
			'lat'               => 'required',
			'lng'               => 'required',
			'price'             => 'required',
			'price_type'        => 'required',
			'outer_type'        => 'required',
		];

		if ( strstr( \Request::route()->getName(), 'store' ) != false ) {
			$rules['image'][] = 'required';
		}

		return $rules;
	}
}
