<?php

namespace App\Http\Middleware;

use App\Repository\Traits\Redirect;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {
	use Redirect;

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param  string|null $guard
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next, $guard = null ) {
		if ( Auth::guard( $guard )->check() ) {

			return \redirect( $this->redirectTo() );
		}

		return $next( $request );
	}
}
