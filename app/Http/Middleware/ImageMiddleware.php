<?php

namespace App\Http\Middleware;

use Closure;

class ImageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	// Todo filter out invalid format.
        return $next($request);
    }
}
