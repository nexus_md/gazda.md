<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class OwnerMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$user = Auth::user();

		if ( $user->role != User::ADMIN && ( $user->role != User::REALTOR || $user->id != $request->route( 'user' ) ) ) {
			abort( 404 );
		}

		return $next( $request );
	}
}
