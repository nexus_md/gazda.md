<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
	// Scopes

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeAcronym( $query, $value )
	{
		return $query->where( 'acronym', $value );
	}

	// Relations

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function quotients()
	{
		return $this->hasMany( CurrencyQuotient::class, 'numerator' );
	}
}
