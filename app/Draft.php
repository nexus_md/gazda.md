<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Draft extends Model {

	protected $fillable = [
		'user_id',
		'json',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
		return $this->belongsTo(User::class);
	}
}
