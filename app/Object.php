<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Object extends Model
{
	use Sluggable;

	protected $fillable = [
		'object_id',
		'area',
		'floor',
		'room_count',
		'type',
		'outer_type',
		'construction_type',
		'publish',
		'validate',
		'user_id',
		'city_id',
		'address',
		'lat',
		'lng',
		'price',
		'price_type',
		'featured',
		'slug',
		'entrance',
		'apartment_nr',
		'floor_count',
		'living_area',
		'kitchen_area',
		'balcony_count',
		'parking_lot',
		'currency_id',
        'object_insights',
        'owner_name',
        'owner_number',
        'owner_email'
	];


	public function sluggable(): array
	{
		return [
			'slug' => [
				'source' => [ 'city.name', 'address', 'id' ],
			],
		];
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeProduct( $query, $value )
	{
		return $query->with( [
			'translation',
			'city.city.city',
			'gallery' => function ( $query )
			{
				return $query->size( Gallery::BASE )->orderBy( 'position' );
			},
			'image',
			'plans',
			'numbers',
			'image',
			'videos',
			'facilities',
			'currency'
		] )->slug( $value );
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopePosition( $query, $value )
	{
		dd( $value->all() );

		return $query->where( 'lat', $value->lat )->where( 'lng', $value->lng );
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeSlug( $query, $value )
	{
		return $query->where( 'slug', $value );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeValid( $query )
	{
		return $query->where( 'validate', 1 );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeNotValid( $query )
	{
		return $query->where( 'validate', 0 );
	}

	/**
	 * @param $query
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function scopeSearch( $query, Request $request )
	{
		if ( $request->search ) {
			return $query->where( 'id', $request->search )
			             ->orWhere( 'address', 'like', "%{$request->search}%" )
			             ->orWhereRaw( "city_id in (SELECT id FROM cities WHERE name LIKE ?)", "%$request->search%" );
		}
	}

	/**
	 * @param $query
	 * @param User $user
	 *
	 * @return mixed
	 */
	public function scopeByUser( $query, User $user )
	{
		return $query->whereRaw( "id IN (SELECT object_id FROM number_object WHERE number_id IN (SELECT id FROM numbers WHERE user_id = {$user->id}))" )
		             ->orWhere( 'user_id', $user->id );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePublished( $query )
	{
		return $query->where( 'publish', 1 );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeGrid( $query )
	{
		return $query->with( [
			'image' => function ( $query )
			{
				return $query->size( Image::BASE );
			},
			'city.city.city',
			'currency',
		] )->published()->valid();
	}

	public function scopeLastSeen( $query, $value )
	{
		$query->whereIn( 'slug', $value );

		$qStr = 'FIELD(`slug`,';

		foreach ( $value as $v ) {
			$qStr .= '?,';
		}

		$qStr = substr( $qStr, 0, - 1 ) . ')';

		return $query->orderByRaw( $qStr, $value );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeMap( $query )
	{
		return $query->select( 'id', 'lat', 'lng' )->published()->valid();
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePerHour( $query )
	{
		return $query->grid()->where( 'price_type', 0 );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeFeatured( $query )
	{
		return $query->grid()->where( 'featured', 1 );
	}

	/**
	 * @param $query
	 * @param array $cities
	 *
	 * @return mixed
	 */
	public function scopeCities( $query, array $cities )
	{
		if ( $cities ) {
			return $query->whereIn( 'city_id', $cities );
		}
	}

	/**
	 * @param $query
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function scopeFilter( $query, Request $request )
	{
		return $query->cities( (array) $request->cities )
		             ->price( $request->fromPrice, $request->toPrice )
                     ->roomCount( $request->fromroom_count, $request->toroom_count)
		             ->area( $request->minArea, $request->maxArea )
		             ->floor( $request->floor )
		             ->priceType( $request->price_type )
		             ->types( $request->types )
                     ->urgent( $request->urgent )
                     ->objectId( $request->objectId )
                     ->userId( $request->realtor_id )
                     ->ownerName( $request->owner_name )
		             ->search( $request );
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopePriceType( $query, $value )
	{
		if ( $value ) {
			return $query->whereIn( 'price_type', $value );
		}
	}

    /**
     * @param $query
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public function scopeRoomCount( $query, $from, $to )
    {
        if ( $from && $to ) {
            return $query->whereBetween( 'room_count', [ $from, $to ] );
        }

        if ( $from ) {
            return $query->where( 'room_count', '>=', $from );
        }

        if ( $to ) {
            return $query->where( 'room_count', '<=', $to );
        }
    }

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeTypes( $query, $value )
	{
		if ( $value ) {
			return $query->whereIn( 'outer_type', $value );
		}
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeFloor( $query, $value )
	{
		if ( $value ) {
			return $query->where( 'floor', '>=', $value );
		}
	}

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeArea( $query, $from, $to )
	{
		if ( $from && $to ) {
			return $query->whereBetween( 'area', [ $from, $to ] );
		}

		if ( $from ) {
			return $query->where( 'area', '>=', $from );
		}

		if ( $to ) {
			return $query->where( 'area', '<=', $to );
		}
	}

	/**
	 * @param $query
	 * @param $from
	 * @param $to
	 *
	 * @return mixed
	 */
	public function scopePrice( $query, $from, $to )
	{
		if ( $from && $to ) {
			return $query->whereBetween( 'price', [ $from, $to ] );
		}

		if ( $from ) {
			return $query->where( 'price', '>=', $from );
		}

		if ( $to ) {
			return $query->where( 'price', '<=', $to );
		}
	}

    /**
     * @param $query
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public function scopeUrgent( $query, $value )
    {
        if ( $value == true ) {
            return $query->where( 'urgent', 1 );
        }
    }


    /**
     * @param $query
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public function scopeObjectId( $query, $value )
    {
        if ( $value ) {
            return $query->where('id', 'LIKE', "%$value%")->orderBy('id', 'asc');
        }
    }

    /**
     * @param $query
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public function scopeUserId( $query, $value )
    {
        if ( $value ) {
            return $query->where('user_id', 'LIKE', "%$value%")->orderBy('user_id', 'asc');
        }
    }

    /**
     * @param $query
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public function scopeOwnerName( $query, $value )
    {
        if ( $value ) {
            return $query->where('owner_name', 'LIKE', "%$value%")->orderBy('owner_name', 'asc');
        }
    }

	/**
	 * @param $value
	 */
	public function setPublishAttribute( $value )
	{
		$this->attributes['publish'] = $value ? $value : 0;
	}

	public function setFeaturedAttribute( $value )
	{
		$this->attributes['featured'] = $value ? $value : 0;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function numbers()
	{
		return $this->belongsToMany( Number::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function translation()
	{
		return $this->hasOne( ObjectTranslation::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function gallery()
	{
		return $this->hasMany( Gallery::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function images()
	{
		return $this->hasMany( Image::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function image()
	{
		return $this->hasOne( Image::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function plans()
	{
		return $this->belongsToMany( Plan::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function actions()
	{
		return $this->hasMany( Action::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function city()
	{
		return $this->belongsTo( City::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function facilities()
	{
		return $this->belongsToMany( Facility::class, 'facilities_objects' );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function videos()
	{
		return $this->hasMany( Video::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function video()
	{
		return $this->hasOne( Video::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function currency()
	{
		return $this->belongsTo( Currency::class );
	}
}
