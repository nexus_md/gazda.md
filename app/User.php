<?php

namespace App;

use App\Repository\Constants\Role;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	const ADMIN = 0;
	const REALTOR = 1;
	const SIMPLE = 2;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
		'role',
        'pers_number'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeObjectCount( $query )
	{
		return $query->selectRaw( 'id, id index_id, name, email, ( SELECT count(*) FROM objects WHERE id IN (SELECT object_id FROM number_object WHERE number_id IN (SELECT id FROM numbers WHERE user_id = index_id))) object_count' );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeRealtor( $query )
	{
		return $query->where( 'role', self::REALTOR );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeAdmin( $query )
	{
		return $query->where( 'role', self::ADMIN );
	}

	public function scopeSearch( $query, Request $request )
	{
		if ( $request->search ) {
			return $query->where( 'name', 'like', "%{$request->search}%" )
			             ->orWhere( 'email', 'like', "%{$request->search}%" );
		}
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function numbers()
	{
		return $this->hasMany( Number::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function actions()
	{
		return $this->hasMany( Action::class );
	}
}
