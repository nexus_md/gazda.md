<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {
	const BASE = 1170;
	const THUMB = 200;

	protected $guarded = [
		'object_id',
	];

	protected $table = 'gallery';

	/**
	 * @param $query
	 * @param $value
	 *
	 * @return mixed
	 */
	public function scopeSize( $query, $value ) {
		return $query->where( 'type', $value );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function object() {
		return $this->belongsTo( Object::class );
	}
}
