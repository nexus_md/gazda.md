<?php
return [
	'object_state' => [
        'object.euro',
        'object.individual_design',
        'object.cosmetic',
        'object.white',
        'object.gray',
        'object.no_renovation',
        'object.needs_renovation',
        'object.unfinished_building',
        'object.for_demolishion',
	],

	'object_type' => [
		'object.apartment',
		'object.house',
        'object.shared',
        'object.one_room',
	],

	'construction_type' => [
		'object.new',
		'object.old',
	],

	'action_type' => [
		'object.bron',
		'object.rent',
		'object.sold',
		'object.client_refuse',
		'object.canceled_contract',
	],

	'object_price_type' => [
		'object.month',
		'object.hour',
	],

	'parking_lot' => [
		'object.open',
		'object.parking_lot',
		'object.under_roof',
		'object.under_ground'
	]
];