<?php

return [
	'locales' => [
		'en' => 'English',
		'ro' => 'Româna',
		'ru' => 'Руский'
	],
	'default_locale' => 'en'
];