var formValidation = {
  imageList: []
}

$(document).ready(function () {
    $('form.ajax').submit(function (event) {
        event.preventDefault();

        var form = $(this);

        var button = form.find('button');
        var url = form.attr('data-redirect');
        var successCallback = form.attr('data-success-callback')
        var errorCallback = form.attr('data-error-callback')

        button.attr('disabled', '');

        $formData = new FormData(this);

        parseImagesList($formData);

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: $formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                parseSuccess(form);

                if (successCallback) {
                    window[successCallback](form, data);
                }

                if (url) {
                    window.location.replace(url);
                    return;
                }

                $("html, body").stop().animate({scrollTop: 0}, 500, 'swing');

                button.removeAttr('disabled');
            },
            error: function (data) {
                if (errorCallback) {
                    window[errorCallback](form, data);
                }

                if (data.status == 422 || data.status == 423) {
                    parseError(form, data.responseJSON !== undefined ? data.responseJSON.errors : JSON.parse(data.responseText).errors);
                } else {
                    alert('Unexpected error !');
                }

                $("html, body").stop().animate({scrollTop: 0}, 500, 'swing');

                button.removeAttr('disabled');
            }
        });
    });

    $('form.ajax').on('change keypress', function (event) {
        var parent = getParent($(event.target));

        if (parent.hasClass('has-error')) {
            parent.removeClass('has-error');
            parent.find('.error-message').text('');
        }

        if (parent.hasClass('has-success')) {
            parent.removeClass('has-success');
        }
    });
});

function parseError(form, data)
{
    for (var key in data) {
        var value = data[key];

        var parent = getParent(form.find('input[name="' + key + '"], input[name="' + key.split('.')[0] + '[]"], textarea[name="' + key + '"], select[name="' + key + '"]'));

        parent.addClass('has-error');
        parent.removeClass('has-success');
        parent.find('.error-message').text(value[0]);
    }
}

function getParent(element)
{
    var parent = element.parent();
    var grandParent = parent.parent();

    if (grandParent.hasClass('input-group')) {
        return grandParent;
    }

    return parent;
}

function parseSuccess(form)
{
    var inputs = form.find('input, textarea').not('input[type="hidden"]');

    var index = inputs.length;

    while (index--) {
        var parent = getParent($(inputs[index]));

        parent.removeClass('has-error');
        parent.addClass('has-success');

        parent.find('.error-message').text('');
    }
}

function parseImagesList ($formData) {
  for(key in formValidation.imageList){
    var fileList = formValidation.imageList[key];

    for (var i = 0; i < fileList.length; i++){
      $formData.append('images[]', fileList[i]);
    }
  }
}