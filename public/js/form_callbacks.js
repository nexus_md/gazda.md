$(document).ready(function () {
    $('form.ajax').submit(function () {
        $('.overlay').removeClass('hidden');
    });
});

function successCallback(form, data) {
    $('.overlay').addClass('hidden');
    $('#modal-success').modal();
}

function errorCallback(form, data) {
    $('.overlay').addClass('hidden');
    $('#modal-danger').modal();
}