$(document).ready(function () {
    $('form .image').change(function () {
        var reader = new FileReader();
        var imagePlaceholder = $(this).parent().find('.image-placeholder');

        reader.readAsDataURL(this.files[0]);

        reader.onload = function () {
            var image = new Image();
            image.src = reader.result;

            image.onload = function () {
                var canvas = document.createElement('canvas');

                var width = imagePlaceholder.width();
                var height = imagePlaceholder.height();

                canvas.width = width;
                canvas.height = height;

                canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                imagePlaceholder.css('background', 'url("' + canvas.toDataURL() + '")');
            };
        }
    });

    $('form .image-trigger').click(function () {
        $(this).parent().parent().find('.image').click();
    });
});