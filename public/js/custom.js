// On load bug
$(document).ready(function () {
    $("html").scrollTop(0);
});

// Dropdown filter on click mobile
$('#filter-md-display').click(function () {
    $('.filter-section').slideToggle(500, function () {
        $('#filter-md-display').toggleClass('open');
    });
});


// Make map full height
$('.view-map-button').click(function () {
    $('#gmaps').toggleClass('screenHeight');
    $(window).scrollTop( $('#gmaps').offset().top );
});

// Sidebar stuff

$('div.sticky-sidebar-right-item').hover(function(){
    $(this).find('.sticky-sidebar-right-content-wrapper').toggleClass('open')
});
