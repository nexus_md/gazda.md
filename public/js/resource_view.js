$(document).ready(function () {
    $('.remove-entity').click(function (event) {
        event.preventDefault();

        $('#modal-warning').modal();
        $('.confirm-action').attr('data-href', $(this).attr('href'));
    });

    $('.confirm-action').click(function () {
        var overlay = $('.overlay');
        overlay.removeClass('hidden');

        var element = $("a[href='" + $(this).attr('data-href') + "']");

        $.ajax({
            url: $(this).attr('data-href'),
            method: 'DELETE',
            data: {_token: Laravel.csrfToken},
            success: function (data) {
                overlay.addClass('hidden');

                var row = element.parent().parent();

                row.fadeOut(500, function () {
                    row.remove();
                });
            },
            error: function (data) {
                alert('Unexpected error !');
            }
        });
    });
});