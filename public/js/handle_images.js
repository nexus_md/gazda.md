$(document).ready(function () {
    $('form .images').change(function () {
        var imagesPlaceholder = $(this).parent().parent().find('.images-placeholder');

        var count = imagesPlaceholder.children().length

        var anyWindow = window.URL || window.webkitURL;
        var fileList = this.files;

        formValidation.imageList.push(fileList);

        for (var i = 0; i < fileList.length; i++) {
            //get a blob to play with
            var objectUrl = anyWindow.createObjectURL(fileList[i]);
            // for the next line to work, you need something class="preview-area" in your html
            imagesPlaceholder.append('<div class="pull-left image-container"><img data-pos="' + (count + i) + '" class="pull-left image-preview" src="' + objectUrl + '" /><span data-id="u' + (count + i) + '" onclick="callWarningModal(event)"><i data-id="u' + (count + i) + '" class="fa fa-remove"></i></span></div>');
            // get rid of the blob
            window.URL.revokeObjectURL(fileList[i]);
        }
    });
});