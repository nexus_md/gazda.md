var gulp            = require('gulp');
var browserSync     = require('browser-sync').create();
var sass            = require('gulp-sass');
var watch = require('gulp-watch');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function () {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', './scss/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest("./css"))
        .pipe(browserSync.stream());
});

// Move the javascript files into our /./js folder
gulp.task('js', function () {
       return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/jquery.min.js', 'node_modules/popper.js/dist/popper.js'])
           .pipe(gulp.dest("./js"))
           .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function () {

        browserSync.init({
            proxy: 'http://gazda.md/**'
        });

        gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', './scss/**/*.scss'], ['sass']);
        gulp.watch("./**/*.php").on('change', browserSync.reload);
});

gulp.task('default', ['js', 'serve']);
