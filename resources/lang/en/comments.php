<?php

return[
    'comments'      => 'Comments',
    'write_comment' => 'Write a comment',
    'leave_comment' => 'Leave a comment',
    'awesome'       => 'Awesome!',
    'thx'           => 'Thanks for leaving a comment!',
    'name'          => 'Name',
    'text'          => 'Text',
    'score'         => 'Score'
];