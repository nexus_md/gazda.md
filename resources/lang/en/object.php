<?php
return [
	'bron'              => 'Reserved',
	'rent'              => 'Rented',
	'sold'              => 'Sold',
	'client_refuse'     => 'Client refused',
	'canceled_contract' => 'Canceled contract',

	'white'     => 'White',
	'not_white' => 'Not white',
	'apartment' => 'Apartment',
	'house'     => 'House',
	'hour'      => '/ hour',
	'month'     => '/ month',
	'old'       => 'Old',
	'new'       => 'New',
    'room'      => 'Room',
    'rooms'     => 'Rooms'
];