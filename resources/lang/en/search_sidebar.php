<?php

return [
    'region'          => 'Region',
    'all_regions'     => 'Get all regions',
    'types'           => 'Types',
    'lease'           => 'Lease type',
    'price'           => 'Price',
    'construction'    => 'Construction Type',
    'room_count'      => 'Room Count',
    'area'            => 'Area',
    'floor'           => 'Floor'
];