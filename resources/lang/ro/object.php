<?php
return [
    'bron' => 'Rezervad',
    'rent' => 'Arendat',
    'sold' => 'Vîndut',
    'client_refuse' => 'Client refuzat',
    'canceled_contract' => 'Contract anulat',

    // Apartment sttes
    'euro' => 'Euroreparație',
    'individual_design' => 'Design individual',
    'cosmetic' => 'Reparație cosmetică',
    'white' => 'La alb',
    'gray' => 'La gri',
    'no_renovation' => 'Fără reparație ',
    'needs_renovation' => 'Are nevoie de reparație',
    'unfinished_building' => 'Construcție nefinisată',
    'for_demolishion' => 'Pentru demolare',

    // House type
    'apartment' => 'Apartment',
    'house' => 'Casă',
    'shared' => 'Garsonieră',
    'one_room' => 'Cameră',

    // Rent Type
    'hour' => '/ zi',
    'month' => '/ lună',

    // Building Type
    'old' => 'Fond Secundar',
    'new' => 'Construcție Nouă',

    'room' => 'Odaie',
    'rooms' => 'Odai',

//    experiment
    'area' => 'Suprafață',
    'kitchen_area' => 'Suprafața bucătăriei',
    'living_area' => 'Suprafața locativă',
    'room_count' => 'Număr de camere',
    'floor_count' => 'Număr de nivele',
    'floor' => 'Nivel',
    'select_realtor' => 'Alege Realtoru',
    'image' => 'Imagine',
    'select_number' => 'Alege numărul',
    'select_plan' => 'Plan locativ',
    'publish' => 'Publică',
    'advance_price' => 'Preț avans',
    'passive_price' => 'Venit pasiv',
    'total_price' => 'Prețul total',
    'comment' => 'Comentariu',
    'type' => 'Tip',
    'from' => 'De la',
    'to' => 'Pînă',
    'search' => 'Caută',
    'address' => 'Adresă',
    'city' => 'Oraș',
    'settlement' => 'Localitate',
    'section' => 'Sector',
    'entrance' => 'Blocul',
    'apartment_nr' => 'Apartamentul',

    'select_object_type' => 'Alegeți tipul obiectului',
    'select_object_state' => 'Alegeți starea obiectului',
    'select_construction_type' => 'Alegeți tipul de construcție ',
    'click_to_upload_images' => 'Click ca să încarci imaginea',
    'select_action_type' => 'Tipul acțiunii',
    'select_object' => 'Alege obiectul',

    'parking_lot' => 'Lot de parcare',
    'balcony_count' => 'Balcoane',

    'price_type' => 'Tipul prețului',
    'price' => 'Prețul',
    'form.currency' => 'Valuta',
    'featured' => 'Featured',
    'title' => 'Titlu',
    'description' => 'Descriere',
    'text' => 'Text',
    'select_locale' => 'Alege limba',
];