<?php

return [
    'region'          => 'Regiunea',
    'all_regions'     => 'Arata toate regiunile',
    'types'           => 'Tipul',
    'lease'           => 'Tipul de închiriere',
    'price'           => 'Preț',
    'construction'    => 'Tip de construcție',
    'room_count'      => 'Numărul camerelor',
    'area'            => 'Arie',
    'floor'           => 'Etajul'
];