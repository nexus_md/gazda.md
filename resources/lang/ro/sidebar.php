<?php
return [
	'realtor'        => 'Realtor',
	'create_realtor' => 'Create realtor',
	'update_realtor' => 'Update realtor',
	'view_realtors'  => 'View realtors',

	'object'        => 'Object',
	'create_object' => 'Create object',
	'update_object' => 'Update object',
	'view_objects'  => 'View objects',

	'number'        => 'Number',
	'create_number' => 'Create number',
	'view_numbers'  => 'View numbers',
	'update_number' => 'Update number',

	'plan'        => 'Plan',
	'create_plan' => 'Create plan',
	'view_plans'  => 'View plans',
	'update_plan' => 'Update plan',

	'action'        => 'Action',
	'create_action' => 'Create action',
	'view_actions'  => 'View actions',

	'pages'       => 'Pages',
	'create_page' => 'Create page',
	'view_pages'  => 'View pages',
	'update_page' => 'Update page',

	'facility'        => 'Facility',
	'create_facility' => 'Create facility',
	'view_facilities' => 'View facilities',
];