<?php

return[
    'comments'      => 'Comentarii',
    'write_comment' => 'Scrie un comentariu',
    'leave_comment' => 'Lasă un comentariu',
    'awesome'       => 'Succes!',
    'thx'           => 'Multumim pentru comentariu!',
    'name'          => 'Nume',
    'text'          => 'Text',
    'score'         => 'Scor'
];