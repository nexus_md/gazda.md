<?php

return [
    'comments'      => 'Отзывы',
    'write_comment' => 'Написать отзыв',
    'leave_comment' => 'Оставить отзыв',
    'awesome'       => 'Отлично!',
    'thx'           => 'Спасибо за отзыв!',
    'name'          => 'Имя',
    'text'          => 'Текст',
    'score'         => 'Оценка'
];