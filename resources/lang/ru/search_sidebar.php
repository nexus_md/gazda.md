<?php

return [
    'region'          => 'Область',
    'all_regions'     => 'Получить все регионы',
    'types'           => 'Типы',
    'lease'           => 'Тип аренды',
    'price'           => 'Цена',
    'construction'    => 'Тип конструкции',
    'room_count'      => 'Количество комнат',
    'area'            => 'Площадь',
    'floor'           => 'Этаж'
];