<?php
return [
    'delete'  => 'Delete',
    'no_data' => 'No data available in table',
    'edit'    => 'Edit',

    'go_to_realtor_page'    => 'Go to realtor page',
    'add_edit_translations' => 'Add / Edit translations',

    'object_count' => 'Nr. of objects',
    'no_title'     => 'No title',
];