@extends('layouts.app')

@section('content')
    <div class="info-page-wrapper container">

        <div class="row">
            <div class="col-xs-12 col-md-12 info-page-title">
                <h2>{{ $data->title }}</h2>
            </div>
            <div class="col-xs-12 col-md-12 info-page-content">
                {!! $data->content !!}
            </div>
        </div>
    </div>
@stop