<p>Hi {{ $data['name'] }}, your account on gazda.md has been created.</p>
<p>Here is your password: {{ $data['password'] }}</p>
<p>We highly recommend to change password after login.</p>
<p><a href="{{ route('login') }}" target="_blank">Login page</a></p>