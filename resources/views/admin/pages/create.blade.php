@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($formData) ? __('sidebar.update_page') : __('sidebar.create_page') }}</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ isset($formData) ? route('pages.update', [app()->getLocale(), $formData->id]) : route('pages.store', app()->getLocale()) }}"
                  method="POST"
                  data-success-callback="createCallback" data-error-callback="errorCallback">
                {!! csrf_field() !!}

                @if(isset($formData))
                    <input type="hidden" name="_method" value="PUT">
                @endif

                <input type="hidden" name="page_id" id="page_id" value="0">

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-2">
                        <label for="iso_code">*@lang('form.select_locale')</label>
                        <select class="form-control select2" name="iso_code" id="iso_code">
                            @foreach($locales as $key => $locale)
                                <option value="{{ $key }}" {{ $key == app()->getLocale() ? 'selected' : '' }}>{{ $locale }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="title">*@lang('form.title')</label>
                        <input type="text" class="form-control" name="title" id="title"
                               value="{{ isset($formData) && $formData->translation ? $formData->translation->title : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="content">*@lang('form.text')</label>
                        <textarea name="content" id="content" rows="10"
                                  class="form-control">{{ isset($formData) && $formData->translation ? $formData->translation->content : '' }}</textarea>
                        <span class="help-block error-message"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group col-xs-12 col-sm-6">
                            <p></p>
                            <label>
                                @lang('form.header')
                                <input type="hidden" name="header" value="0">
                                <input type="checkbox" class="minimal" name="header"
                                       value="1" {{ isset($formData) && $formData->header ? 'checked' : '' }}>
                            </label>
                            <p></p>
                            <label>
                                @lang('form.footer')
                                <input type="hidden" name="footer" value="0">
                                <input type="checkbox" class="minimal" name="footer"
                                       value="1" {{ isset($formData) && $formData->footer ? 'checked' : '' }}>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('pages.index', app()->getLocale())])
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        function createCallback(form, data) {
            successCallback(form, data);

            if (data) {
                $('#page_id').val(data);
            }
        }
    </script>

    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('.select2').select2();

        CKEDITOR.replace('content', {
            on: {
                change: function () {
                    this.updateElement();
                    $('#content').change();
                }
            }
        });

        @if (isset($formData))
        $('#iso_code').change(function () {
            $('.overlay').removeClass('hidden');

            $.ajax({
                url: '{{ route('pages.show', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                data: {iso_code: $(this).val()},
                success: function (data) {
                    if (data) {
                        for (var key in data) {
                            var input = $('input[name="' + key + '"], select[name="' + key + '"], textarea[name="' + key + '"]');
                            input.val(data[key]);
                        }
                    } else {
                        $('input:not([name="_token"]):not([name="_method"]), textarea, select:not([name="iso_code"])').val('');
                    }

                    CKEDITOR.instances.content.setData($('#content').val());

                    $('.overlay').addClass('hidden');
                },
                error: function (data) {
                    console.log(data);
                    alert('Unexpected error.');
                }
            })
        });
        @endif
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@stop