@extends('layouts.admin')

@section('content')
    <div class="comments">
        <div class="row top-row">
            <div class="col-md-6 col-xs-12">
                <h3 class="top-heading">Отзывы</h3>
            </div>
            <div class="col-md-6 col-xs-12">
                <button class="btn add-btn" data-toggle="modal" data-target="#addModal"><i class="fas fa-plus"></i> Написать отзыв</button>
            </div>
        </div>
        <hr>

        <div class="row testimonials-row">
            @foreach($comments as $comment)
                <div class="col-md-6 col-sm-6 col-xs-12 testemonial">
                    <div class="card panel" style="border: none;">
                        <div class="card-body panel-body">
                            <span>« {{$comment->text}} »</span>
                        </div>
                        <div class="card-footer panel-footer">
                            <div class="row">
                                <div class="col-md-6 col-xs-6 col-12">
                                    <div class="name-section">
                                        {{$comment->name}}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6 col-12">
                                    <div class="card-rating">
                                        @for($i = 0; $i < $comment->mark; $i++)
                                            <img src="/images/star.svg" alt="">
                                        @endfor
                                        @if($comment->mark !== 5)
                                            @for($i = 0; $i < 5-$comment->mark; $i++)
                                                <img src="/images/star_gray.svg" alt="">
                                            @endfor
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class='action_btns'>
                                @if($comment->publish == 0)
                                    <form action="{{route('comment.approve', ['locale'=> app()->getLocale()] )}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <button type="submit" class="btn btn-success" href="">Accept</button>
                                    </form>
                                @else
                                    <form action="{{route('comment.disable', ['locale'=> app()->getLocale()] )}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <button type="submit" class="btn btn-info">Disable</button>
                                    </form>
                                @endif
                                    <form action="{{route('comment.delete', ['locale'=> app()->getLocale(), 'id' => $comment->id] )}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <button type="submit" class="btn btn-danger" >Delete</button>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            {{$comments}}
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Написать отзыв</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body addCommentModal">
                    <form class="form-horizontal" method="POST" action="{{ route('comments.store', ['locale'=> app()->getLocale()]) }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" class="form-control" name="name" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="text">Text</label>
                            <textarea name="text" id="text" class="form-control" cols="20" rows="10"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Score</label>
                            <div class="rating">
                                <span><input type="radio" name="mark" id="str5" value="5"><label for="str5"></label></span>
                                <span><input type="radio" name="mark" id="str4" value="4"><label for="str4"></label></span>
                                <span><input type="radio" name="mark" id="str3" value="3"><label for="str3"></label></span>
                                <span><input type="radio" name="mark" id="str2" value="2"><label for="str2"></label></span>
                                <span><input type="radio" name="mark" id="str1" value="1"><label for="str1"></label></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">
                                    Оставить отзыв
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if (Session::has('header'))
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>swal(`{{Session::get('header')}}`, `{{Session::get('body')}}`, `{{Session::get('type')}}`);</script>
    @endif
@stop

@section('css')
    <link rel="stylesheet" href="/css/comments.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Sorry I had to :( -->

    <style>
        .action_btns{
            text-align: center;
            margin-top: 10px;
        }

        @media screen and (min-width: 991px){
            .card-footer{
                height: 100px !important;
            }
        }

        @media screen and (max-width: 991px){
            .card-footer{
                height: 120px !important;
            }
        }

        .addCommentModal{
            width: 90%;
            margin: auto;
            display: block;
        }

        .action_btns form{
            display: inline !important;
        }
    </style>
@stop

@section('js')
    <script>
        $(document).ready(function(){
            //  Check Radio-box
            $(".rating input:radio").attr("checked", false);
            $('.rating input').click(function () {
                $(".rating span").removeClass('checked');
                $(this).parent().addClass('checked');
            });

            $('input:radio').change(
                function(){
                    var userRating = this.value;
                });
        });
    </script>
@stop