@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($formData) ? __('sidebar.update_plan') : __('sidebar.create_plan') }}</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ isset($formData) ? route('plan.update', [app()->getLocale(), $formData->id]) : route('plan.store', app()->getLocale()) }}"
                  method="POST"
                  data-success-callback="successCallback"
                  data-error-callback="errorCallback">
                {!! csrf_field() !!}

                @if(isset($formData))
                    <input type="hidden" name="_method" value="PUT">
                @endif

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="name">*@lang('form.name')</label>
                        <input type="text" name="name" class="form-control" id="name"
                               placeholder="@lang('form.name')..."
                               value="{{ isset($formData) ? $formData->name : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="object_id">@lang('form.select_object')</label>
                        <select name="object_id[]" id="object_id" class="select2" multiple></select>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="image">*@lang('form.image')</label>
                        <input type="file" name="image" class="form-controll image hidden" id="image">
                        <div class="image-placeholder"
                             style="background: url('{{ isset($formData) ? route('image', $formData->image) : '' }}') no-repeat; background-size: cover">
                            <div class="image-trigger"><i class="fa fa-plus"></i></div>
                        </div>
                        <span class="help-block error-message"></span>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('plan.index', app()->getLocale())])
    @include('partials.view.warning')
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('js/handle_image.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $('#object_id.select2').select2({
            ajax: {
                url: '{{ route('plan.get-objects', app()->getLocale()) }}',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page || 1
                    };
                },

                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var result = data.data.map(function (value) {
                        return {
                            id: value.id,
                            text: value.id + ' - ' + value.city.name + ', ' + value.address
                        }
                    });

                    return {
                        results: result,
                        pagination: {
                            more: (params.page * data.per_page) < data.total
                        }
                    }
                }
            }
        });
    </script>

    @if(\Route::currentRouteName() == 'plan.edit')
        <script>
            $.ajax({
                url: '{{ route('plan.get-plan-objects', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                success: function (data) {
                    var options = [];

                    for (var key in data) {
                        options.push(new Option(data[key].id + ' - ' + data[key].city.name + ', ' + data[key].address, data[key].id, true, true));
                    }

                    $('#object_id').append(options).trigger('change');
                }
            });
        </script>
    @endif
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@stop