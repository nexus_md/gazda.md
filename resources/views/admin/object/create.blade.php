@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h1 class="box-title">{{ isset($formData) ? __('sidebar.update_object') : __('sidebar.create_object') }}</h1>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ isset($formData) ? route('object.update', [app()->getLocale(), $formData->id]) : route('object.store', app()->getLocale()) }}"
                  method="POST"
                  data-success-callback="{{ isset($formData) ? 'successUpdate' : 'successRedirect' }}"
                  data-error-callback="errorCallback">
                {!! csrf_field() !!}

                @if(isset($formData))
                    <input type="hidden" name="_method" value="PUT">
                @endif

                <div class="image-positions"></div>

                <div class="uploaded-images-positions">
                    @if (isset($formData))
                        @foreach($formData->gallery as $image)
                            <input type="hidden" name="uploaded_images_positions[]" value="{{ $image->id }}">
                        @endforeach
                    @endif
                </div>

                {{-- Owner data section --}}
                <div class="row owner-data">
                    <div class="col-xs-12">
                        <h4>Owner data</h4>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="owner_name">*@lang('form.owner_name') </label>
                        <input type="text" name="owner_name" class="form-control" id="owner_name"
                               placeholder="@lang('form.owner_name')..."
                               value="{{ isset($formData) ? $formData->owner_name : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="owner_number">*@lang('form.owner_number') </label>
                        <input type="text" name="owner_number" class="form-control" id="owner_number"
                               placeholder="@lang('form.owner_number')..."
                               value="{{ isset($formData) ? $formData->owner_number : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="owner_email">*@lang('form.owner_email') </label>
                        <input type="text" name="owner_email" class="form-control" id="owner_email"
                               placeholder="@lang('form.owner_email')..."
                               value="{{ isset($formData) ? $formData->owner_email : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="object_insights">*@lang('form.object_insights') </label>
                        <textarea type="text" name="object_insights" class="form-control" id="object_insights"
                                  placeholder="@lang('form.object_insights')..."
                        >{{ isset($formData) ? $formData->object_insights : '' }}</textarea>
                        <span class="help-block error-message"></span>
                    </div>
                </div>
                <hr>
                {{-- Owner data section end --}}

                {{-- Address section --}}
                <div class="row">
                    <div class="col-xs-12">
                        <h4>Object details</h4>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-4">
                        <label for="city">@lang('form.city')</label>
                        <select class="form-control regions" id="city">
                            <option value="0">Select</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{ isset($formData->city->city->city) && $formData->city->city->city->id == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6 col-md-4">
                        <label for="settlement">@lang('form.settlement')</label>
                        <select class="form-control regions" id="settlement">
                            @if(isset($formData->city->city->city))
                                @foreach($formData->city->city->city->regions as $value)
                                    <option value="{{ $value->id }}" {{ $formData->city->city->id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6 col-md-4">
                        <label for="city_id">@lang('form.section')</label>
                        <select class="form-control regions" id="city_id" name="city_id">
                            @if(isset($formData))
                                @foreach($formData->city->city->regions as $value)
                                    <option value="{{ $value->id }}" {{ $formData->city->id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-sm-8 col-xs-8">
                        <label for="address">@lang('form.address')</label>
                        <input type="text" name="address" class="form-control" id="address"
                               value="{{ isset($formData) ? $formData->address : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-sm-2 col-xs-2">
                        <label class="col-xs-12">&nbsp;</label>
                        <div class="btn btn-primary" id="search-addr">@lang('form.search')</div>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="room_count">@lang('form.entrance')</label>
                        <input type="text" name="entrance" class="form-control" id="entrance"
                               placeholder="@lang('form.entrance')..."
                               value="{{ isset($formData) ? $formData->entrance : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="room_count">*@lang('form.apartment_nr')</label>
                        <input type="text" name="apartment_nr" class="form-control" id="apartment_nr"
                               placeholder="@lang('form.apartment_nr')..."
                               value="{{ isset($formData) ? $formData->apartment_nr : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="col-xs-12">
                        <div id="gmaps"></div>
                        <input type="hidden" name="lat" id="lat" value="{{ isset($formData) ? $formData->lat : '' }}">
                        <input type="hidden" name="lng" id="lng" value="{{ isset($formData) ? $formData->lng : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <hr>
                {{-- till here --}}
                <div class="row">

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="area">*@lang('form.area') m<sup>2</sup> </label>
                        <input type="text" name="area" class="form-control" id="area"
                               placeholder="@lang('form.area')..."
                               value="{{ isset($formData) ? $formData->area : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="area">*@lang('form.living_area') m<sup>2</sup> </label>
                        <input type="text" name="living_area" class="form-control" id="living_area"
                               placeholder="@lang('form.living_area')..."
                               value="{{ isset($formData) ? $formData->living_area : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="area">*@lang('form.kitchen_area') m<sup>2</sup> </label>
                        <input type="text" name="kitchen_area" class="form-control" id="kitchen_area"
                               placeholder="@lang('form.kitchen_area')..."
                               value="{{ isset($formData) ? $formData->kitchen_area : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="room_count">*@lang('form.room_count')</label>
                        <input type="text" name="room_count" class="form-control" id="room_count"
                               placeholder="@lang('form.room_count')..."
                               value="{{ isset($formData) ? $formData->room_count : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="floor">*@lang('form.floor')</label>
                        <input type="text" name="floor" class="form-control" id="floor"
                               placeholder="@lang('form.floor')..."
                               value="{{ isset($formData) ? $formData->floor : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="floor_count">*@lang('form.floor_count')</label>
                        <input type="text" name="floor_count" class="form-control" id="floor_count"
                               placeholder="@lang('form.floor_count')..."
                               value="{{ isset($formData) ? $formData->floor_count : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="floor">*@lang('form.balcony_count')</label>
                        <input type="text" name="balcony_count" class="form-control" id="balcony_count"
                               placeholder="@lang('form.balcony_count')..."
                               value="{{ isset($formData) ? $formData->balcony_count : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="number_id">@lang('form.select_number')</label>
                        <select class="form-control" name="number_id[]" id="number_id" multiple>
                        </select>
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12">
                        <label for="plan_id">@lang('form.select_plan')</label>
                        <select class="form-control" name="plan_id[]" id="plan_id" multiple>
                        </select>
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label>*@lang('form.select_object_state')</label>
                                <select class="form-control" name="type" id="type">
                                    @foreach(config('estate.object_state') as $key => $value)
                                        <option value="{{ $key }}" {{ isset($formData) && $formData->type == $key ? 'selected' : '' }}>@lang($value)</option>
                                    @endforeach
                                </select>
                                <span class="help-block error-message"></span>
                            </div>

                            <div class="form-group col-xs-12 col-sm-6">
                                <label>*@lang('form.select_object_type')</label>
                                <select class="form-control" name="outer_type" id="outer_type">
                                    @foreach(config('estate.object_type') as $key => $value)
                                        <option value="{{ $key }}" {{ isset($formData) && $formData->outer_type == $key ? 'selected' : '' }}>@lang($value)</option>
                                    @endforeach
                                </select>
                                <span class="help-block error-message"></span>
                            </div>

                            <div class="form-group col-xs-12">
                                <label>*@lang('form.parking_lot')</label>
                                <select class="form-control" name="parking_lot" id="parking_lot">
                                    @foreach(config('estate.parking_lot') as $key => $value)
                                        <option value="{{ $key }}" {{ isset($formData) && $formData->parking_lot == $key ? 'selected' : '' }}>@lang($value)</option>
                                    @endforeach
                                </select>
                                <span class="help-block error-message"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>*@lang('form.select_construction_type')</label>
                        <select class="form-control" name="construction_type" id="construction_type">
                            @foreach(config('estate.construction_type') as $key => $value)
                                <option value="{{ $key }}" {{ isset($formData) && $formData->construction_type == $key ? 'selected' : '' }}>@lang($value)</option>
                            @endforeach
                        </select>
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group col-xs-12 col-sm-5">
                                <label>*@lang('form.price_type')</label>
                                <select class="form-control" name="price_type" id="price_type">
                                    @foreach(config('estate.object_price_type') as $key => $value)
                                        <option value="{{ $key }}" {{ isset($formData) && $formData->price_type == $key ? 'selected' : '' }}>@lang($value)</option>
                                    @endforeach
                                </select>
                                <span class="help-block error-message"></span>
                            </div>

                            <div class="form-group col-xs-12 col-sm-5">
                                <label>*@lang('form.price')</label>
                                <input type="text" name="price" class="form-control" id="price"
                                       placeholder="@lang('form.price')..."
                                       value="{{ isset($formData) ? $formData->price : '' }}">
                                <span class="help-block error-message"></span>
                            </div>

                            <div class="form-group col-xs-12 col-sm-2">
                                <label>*@lang('form.currency')</label>
                                <select class="form-control" name="currency_id" id="currency">
                                    @foreach($currency as $key => $value)
                                        <option value="{{ $value->id }}" {{ isset($formData) && $formData->currency->id == $value->id ? 'selected' : '' }}>{{ $value->acronym }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block error-message"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-12 form-group">
                                <label for="video">@lang('form.video')</label>
                                <textarea name="video" id="video" class="form-control"
                                          rows="10">{{ isset($formData) && $formData->video ? $formData->video->url : '' }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            @foreach($facilities as $key => $value)
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <p></p>
                                    <label for="facility-{{ $key }}">
                                        {{ $value->translation ? $value->translation->name : '' }}
                                        <input type="checkbox" class="minimal" name="facilities[]"
                                               value="{{ $value->id }}" {{ isset($formData) && $formData->facilities && array_filter($formData->facilities->all(), function($element) use ($value) { return $value->id === $element->id; }) ? 'checked' : '' }}>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="image">*@lang('form.image')</label>
                        <input type="file" name="image" class="form-controll image hidden" id="image">
                        <div class="image-placeholder"
                             style="background: url('{{ isset($formData) && $formData->image ? route('image', $formData->image->image) : '' }}') no-repeat; background-size: cover">
                            <div class="image-trigger"><i class="fa fa-plus"></i></div>
                        </div>
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="images" class="upload-images">@lang('form.click_to_upload_images')</label>
                        <input type="file" id="images" multiple class="hidden images">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="col-xs-12 images-placeholder"></div>

                    @if(isset($formData))
                        <div class="col-xs-12 uploaded-images-placeholder">
                            @include('admin.object.partials.uploaded_images', ['gallery' => $formData->gallery])
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group col-xs-12 col-sm-6">
                                <p></p>
                                <label>
                                    @lang('form.publish')
                                    <input type="checkbox" class="minimal" name="publish"
                                           value="1" {{ isset($formData) && $formData->publish ? 'checked' : '' }}>
                                </label>
                                <p></p>
                                <label>
                                    @lang('form.featured')
                                    <input type="checkbox" class="minimal" name="featured"
                                           value="1" {{ isset($formData) && $formData->featured ? 'checked' : '' }}>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('object.index', app()->getLocale())])
    @include('partials.view.warning')
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('js/handle_image.js') }}"></script>
    <script src="{{ asset('js/handle_images.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR3nISjK4nrFsrCM2te7WdEG2s2ANzpME"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>
    <script src="{{ asset('js/sortable.js') }}"></script>

    @include('partials.form.select2', ['route' => route('number.get-numbers', app()->getLocale())])

    <script>
        $('.regions').change(function () {
            var select = $(this).parent().next().find('.regions');
            if (!select.length) return;

            var current = this;
            var clear = false;
            $('.regions').each(function (i, item) {
                if (current === item) {
                    clear = true;
                    return;
                }

                if (clear) {
                    $(item).html('');
                }
            });

            $('.overlay').removeClass('hidden');

            $.ajax({
                url: '{{ route('get-regions', [app()->getLocale(), '']) }}/' + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $(select).append($('<option>', {
                        value: 0,
                        text: 'Select'
                    }));
                    $.each(data, function (i, item) {
                        $(select).append($('<option>', {
                            value: item.id,
                            text: item.name,
                        }));
                    });

                    $('.overlay').addClass('hidden');
                },
                error: function (data) {

                }
            });
        });

        var gmap = new GMaps({
            div: '#gmaps',
            lat: {{ isset($formData) ? $formData->lat : 47.010284180836166 }},
            lng: {{ isset($formData) ? $formData->lng : 28.86056900024414 }},
            click: function (e) {
                gmap.removeMarkers();
                gmap.addMarker({
                    lat: e.latLng.lat(),
                    lng: e.latLng.lng()
                });

                $('#lat').val(e.latLng.lat()).change();
                $('#lng').val(e.latLng.lng()).change();

                $('#address').blur();
            },
            dragend: function (e) {
                $('#address').blur();
            }
        });

        @if(isset($formData))
        gmap.addMarker({
            lat: {{ $formData->lat }},
            lng: {{ $formData->lng }}
        });
        @endif

        $('#search-addr').click(function (e) {
            var name = '';

            $('.regions').each(function (i, item) {
                name += $(item).find('option:selected').text() + ', ';
            });

            console.log(name + $('#address').val());

            GMaps.geocode({
                address: name + $('#address').val(),
                callback: function (results, status) {
                    if (status === 'OK') {
                        var latlng = results[0].geometry.location;
                        gmap.removeMarkers();
                        gmap.setCenter(latlng.lat(), latlng.lng());
                        gmap.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng()
                        });

                        $('#lat').val(latlng.lat()).change();
                        $('#lng').val(latlng.lng()).change();
                    }
                }
            });
        });


        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('.confirm-action').click(function () {
            var url = $(this).attr('data-href');
            var id = $(this).attr('data-id');
            var obj = $('span[data-id="' + id + '"]');
            $('.overlay').removeClass('hidden');

            if (!url) {
                obj.parent().fadeOut(500, function () {
                    obj.parent().remove();
                });

                $('.overlay').addClass('hidden');

                return;
            }

            $.ajax({
                url: url,
                method: 'DELETE',
                data: {_token: Laravel.csrfToken},
                success: function (data) {
                    obj.parent().fadeOut(500, function () {
                        obj.parent().remove();
                    });

                    $('.overlay').addClass('hidden');
                    $('input[name="uploaded_images_positions[]"][value="' + id + '"]').remove();
                },
                error: function (data) {
                    console.log(data);
                    alert('Unexpected error.');
                    $('.overlay').addClass('hidden');
                }
            });
        });

        $('#plan_id').select2({
            ajax: {
                url: '{{ route('plan.get-plans', app()->getLocale()) }}',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page || 1
                    };
                },

                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var result = data.data.map(function (value) {
                        return {
                            id: value.id,
                            text: value.name
                        }
                    });

                    return {
                        results: result,
                        pagination: {
                            more: (params.page * data.per_page) < data.total
                        }
                    }
                }
            }
        });

        var imagePositions = $('.image-positions');
        var uploadedImagesPositions = $('.uploaded-images-positions');

        $('.images-placeholder').sortable({
            stop: function (event, ui) {
                imagePositions.html('');
                $(event.target).children().each(function (key, value) {
                    imagePositions.append('<input type="hidden" name="image_positions[' + $(this).attr('data-pos') + ']" value="' + key + '">');
                });
            }
        });

        $('.uploaded-images-placeholder').sortable({
            stop: function (event, ui) {
                uploadedImagesPositions.html('');
                $(event.target).children().each(function (key, value) {
                    uploadedImagesPositions.append('<input type="hidden" name="uploaded_images_positions[]" value="' + $(this).find('span').attr('data-id') + '">');
                });
            }
        });

        $('form .images').change(function () {
            imagePositions.html('');
        });
    </script>


    @if(\Route::currentRouteName() == 'object.edit')
        <script>
            $.ajax({
                url: '{{ route('object.get-number', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                success: function (data) {
                    var options = [];

                    for (var key in data) {
                        options.push(new Option(data[key].number, data[key].id, true, true));
                    }

                    $('#number_id').append(options).trigger('change');
                }
            });

            $.ajax({
                url: '{{ route('plan.get-object-plans', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                success: function (data) {
                    var options = [];

                    for (var key in data) {
                        options.push(new Option(data[key].name, data[key].id, true, true));
                    }

                    $('#plan_id').append(options).trigger('change');
                }
            });
        </script>
    @endif

    <script>
        function successRedirect(form, data) {
            successCallback(form, data);
//            window.location.replace(data);
        }

        function successUpdate(form, data) {
            successCallback(form, data);
            form.find('#images').val('');
            form.find('#image').val('');

            $('.uploaded-images-placeholder').append(data);
            $('.images-placeholder, .image-positions').html('');
            formValidation.imageList = [];
        }

        function callWarningModal(event) {
            $('#modal-warning').modal();

            var dataHref = $(event.target).attr('data-href');
            var dataId = $(event.target).attr('data-id');

            console.log(event)

            $('.confirm-action').attr('data-href', dataHref ? dataHref : '');
            $('.confirm-action').attr('data-id', dataId ? dataId : '');
        }
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sortable.css') }}">
@stop