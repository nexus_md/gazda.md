<div class="modal fade" id="more_info_modal" tabindex="-1" role="dialog" aria-labelledby="more_info_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="more_info_modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="more_info_modal_body">
            </div>
        </div>
    </div>
</div>