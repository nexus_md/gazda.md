@foreach($gallery as $image)
    <div class="pull-left image-container">
        <img class="image-preview" src="{{ route('image', $image->image) }}"/>
        <span data-id="{{ $image->id }}"
              data-href="{{ route('object.delete-image-gallery', [app()->getLocale(), $image->id]) }}"
              onclick="callWarningModal(event)"><i data-id="{{ $image->id }}"
                                                   data-href="{{ route('object.delete-image-gallery', [app()->getLocale(), $image->id]) }}"
                                                   class="fa fa-remove"></i></span>
    </div>
@endforeach