@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">@lang('sidebar.view_objects')</h3>
            <hr>
            <form action='{{ route('object.index', app()->getLocale()) }}' method='GET' id="filters">
                <div class="row search-row">
                    <div class="col-md-2 col-xs-12 col-12">
                        <label for="">Object Id</label>
                        <input type="number" class="form-control" name="objectId">
                    </div>
                    <div class="col-md-2 col-xs-12 col-12">
                        <label for="">Realtor Id</label>
                        <input type="number" class="form-control" name="realtor_id">
                    </div>
                    <div class="col-md-4 col-xs-12 col-12">
                        <label for="">Owner Name</label>
                        <input type="text" class="form-control" name="owner_name">
                    </div>
                    <div class="col-md-2 col-xs-12 col-12">
                        <div class="checkbox bottom-row">
                            <input type="checkbox" id="is_important" name="urgent">
                            <label for="is_important">
                                Important
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-12 col-12">
                        <button class='btn btn-success search-btn bottom-row' type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>@lang('form.name')</th>
                    <th>@lang('form.published')</th>
                    <th>@lang('form.urgent')</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($data))
                    @foreach($data as $key => $value)
                        <tr class="{{ !$value->validate ? 'not-valid' : '' }}">
                            <td>{{ $value->id }}</td>
                            @if(isset($value->city->city->city))
                                <td>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}, {$value->address}" }}

                            @elseif(isset($value->city->city))
                                <td>{{ "{$value->city->city->name}, {$value->city->name}, {$value->address}" }}

                            @else
                                <td>{{ "{$value->city->name}, {$value->address}" }}
                            @endif

                            @if ($value->urgent == 1)
                                <span class="urgent">
                                    <i class="fa fa-exclamation" aria-hidden="true"></i>
                                </span>
                            @endif
                            </td>
                            <td>
                                <form action="{{  route('object.set.published', [app()->getLocale(),$value->id]) }}" method="post">
                                    <label><input class="minimal updateColumn" value="1" type="checkbox" name="publish" {{ $value->publish ? 'checked' : '' }}></label>
                                </form>
                            </td>
                            <td>
                                <form action="{{  route('object.set.urgent', [app()->getLocale(),$value->id]) }}" method="post">
                                    <label><input class="minimal updateColumn" value="1" type="checkbox" name="urgent" {{ $value->urgent ? 'checked' : '' }}></label>
                                </form>
                            </td>
                            <td>
                                <a class="object-info" data-id="{{$value->id}}"
                                        data-owner_name="{{$value->owner_name}}" data-owner_number="{{$value->owner_number}}" data-description="{{$value->object_insights}}" data-address="{{$value->address}}">
                                    <i class="fa fa-info" aria-hidden="true"></i> More Info
                                </a>
                            </td>
                            <td><a href="{{ route('object.translation', [app()->getLocale(), $value->id]) }}"><i
                                            class="fa fa-edit"></i>
                                    <span>@lang('resource_view.add_edit_translations')</span></a></td>
                            <td><a href="{{ route('object.edit', [app()->getLocale(), $value->id]) }}"><i
                                            class="fa fa-edit"></i> <span>@lang('resource_view.edit')</span></a></td>
                            <td id="{{ $value->id }}"><a class="remove-entity"
                                                         href="{{ route('object.destroy', [app()->getLocale(), $value->id]) }}"><i
                                            class="fa fa-remove"></i>
                                    @lang('resource_view.delete')</a></td>
                        </tr>
                    @endforeach
                @else
                    <tr class="odd text-center">
                        <td valign="top" colspan="5" class="dataTables_empty">@lang('resource_view.no_data')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            <div class="pull-right">
                {!! $data->links() !!}
            </div>
        </div>
        <!-- /.box-body -->
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>

    @include('partials.view.warning')
    @include('admin.object.partials.more_info_modal')
@stop

@section('js')
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        // $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        //     checkboxClass: 'icheckbox_minimal-blue',
        //     radioClass: 'iradio_minimal-blue'
        // });
    </script>
    <script>

        $('.updateColumn').change(function () {
            $.ajax({
                url: $(this).parent().parent().attr('action'),
                method: 'POST',
                data: {_token: Laravel.csrfToken, value: $(this).is(':checked') ? 1 : 0},
                success: function (data) {
                    alert('Status changed')
                },
                error: function (data) {
                    alert('Unexpected error !');
                }
            });
        });

    </script>
    <script src="{{ asset('js/resource_view.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.object-info').click(function () {
                var object_id = $(this).data('id');
                var owner_name = $(this).data('owner_name');
                var owner_number = $(this).data('owner_number');
                var description = $(this).data('description');
                var address = $(this).data('address');

                var modalBody = `<p><span class='heading'>Owner's Name: </span>${owner_name}</p>
                                 <p><span class='heading'>Owner's Phone: </span>${owner_number}</p>
                                 <p><span class='heading'>Address: </span>${address}</p>
                                 <p><span class='heading'>Description: </span>${description}</p>`;

                $('#more_info_modal_body').html(modalBody);
                $('#more_info_modal_title').html(`Object #${object_id} detailed information`);
                $('#more_info_modal').modal('toggle');
            });

            $('#filters').change(function(){
                var is_important = $('#is_important').prop('checked');
            });

            var url = new URL(window.location.href);
            if (url.searchParams.get('urgent') == 'on'){
                var is_important = $('#is_important').prop('checked', true);
            }
        });
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">

    <style>
        .modal-title{
            display: inline;
        }
        .modal-body .heading{
            font-weight: 600;
            padding-right: 5px;
        }
        table a{
            cursor: pointer;
        }
        .urgent{
            height: 38px;
            width: 30px;
            text-align: center;
            padding-top: 7px;
            background: #ff4d5f;
            color: white;
            float: right;
            margin-top: -10px;
            margin-bottom: -10px;
            margin-right: -9px;
            font-size: 16px;
        }
        .radio,
        .checkbox {
            padding-left: 20px;
            position: relative;
            display: block;
        }
        .radio input,
        .checkbox input {
            opacity: 0;
            position: absolute;
            z-index: 1;
            cursor: pointer;
            margin-left: -20px;
        }
        .radio input:checked + label::before,
        .checkbox input:checked + label::before {
            border-color: #9575cd;
        }
        .radio input:checked + label::after,
        .checkbox input:checked + label::after {
            content: '';
            display: inline-block;
            position: absolute;
            width: 13px;
            height: 13px;
            left: 2px;
            top: 4px;
            margin-left: -20px;
            border: 1px solid #9575cd;
            border-radius: 50%;
            background-color: #9575cd;
        }
        .radio label,
        .checkbox label {
            display: inline-block;
            position: relative;
            padding-left: 5px;
        }
        .radio label::before,
        .checkbox label::before {
            content: '';
            display: inline-block;
            position: absolute;
            width: 17px;
            height: 17px;
            left: 0;
            top: 2px;
            margin-left: -20px;
            border: 1px solid #cccccc;
            border-radius: 50%;
            background-color: #fff;
        }
        .radio.disabled label,
        .checkbox.disabled label {
            color: #cccccc;
        }
        .radio.disabled label::before,
        .checkbox.disabled label::before {
            opacity: .54;
            border-color: #cccccc;
        }
        .checkbox input:checked + label::before {
            border-color: #9575cd;
            background-color: #9575cd;
        }
        .checkbox input:checked + label::after {
            content: "\f00c";
            font-family: FontAwesome;
            font-size: 13px;
            color: #fff;
            top: 0;
            left: 1px;
            border-color: transparent;
            background-color: transparent;
        }
        .checkbox label::before {
            border-radius: 2px;
        }
        .search-btn{
            width: 100%;
        }
        .search-row .form-control{
            border-radius: 6px;
        }
        .search-row .checkbox{
            text-align: center;
        }
        @media screen and (min-width: 991px){
            .search-row .bottom-row{
                margin-top: 20px;
            }

            .search-row .checkbox{
                padding-top: 8px;
            }
        }
    </style>
@stop