@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($formData) ? __('sidebar.update_page') : __('sidebar.create_page') }}</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ isset($formData) ? route('facility.update', [app()->getLocale(), $formData->id]) : route('facility.store', app()->getLocale()) }}"
                  method="POST"
                  data-success-callback="createCallback" data-error-callback="errorCallback">
                {!! csrf_field() !!}

                @if(isset($formData))
                    <input type="hidden" name="_method" value="PUT">
                @endif

                <input type="hidden" name="facility_id" id="facility_id" value="0">

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-2">
                        <label for="iso_code">*@lang('form.select_locale')</label>
                        <select class="form-control select2" name="iso_code" id="iso_code">
                            @foreach($locales as $key => $locale)
                                <option value="{{ $key }}" {{ $key == app()->getLocale() ? 'selected' : '' }}>{{ $locale }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="title">*@lang('form.title')</label>
                        <input type="text" class="form-control" name="name" id="title"
                               value="{{ isset($formData) && $formData->translation ? $formData->translation->name : '' }}">
                        <span class="help-block error-message"></span>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('facility.index', app()->getLocale())])
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        function createCallback(form, data) {
            successCallback(form, data);

            if (data) {
                $('#facility_id').val(data);
            }
        }
    </script>

    <script>
        $('.select2').select2();

        @if (isset($formData))
        $('#iso_code').change(function () {
            $('.overlay').removeClass('hidden');

            $.ajax({
                url: '{{ route('facility.show', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                data: {iso_code: $(this).val()},
                success: function (data) {
                    if (data) {
                        for (var key in data) {
                            var input = $('input[name="' + key + '"], select[name="' + key + '"], textarea[name="' + key + '"]');
                            input.val(data[key]);
                        }
                    } else {
                        $('input:not([name="_token"]):not([name="_method"]), textarea, select:not([name="iso_code"])').val('');
                    }

                    $('.overlay').addClass('hidden');
                },
                error: function (data) {
                    $('.overlay').addClass('hidden');

                    console.log(data);
                    alert('Unexpected error.');
                }
            })
        });
        @endif
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@stop