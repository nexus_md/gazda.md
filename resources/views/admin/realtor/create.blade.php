@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($formData) ? __('sidebar.update_realtor') : __('sidebar.create_realtor') }}</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ isset($formData) ? route('realtor.update', [app()->getLocale(), $formData->id]) : route('realtor.store', app()->getLocale()) }}"
                  method="POST"
                  data-success-callback="successCallback" data-error-callback="errorCallback">
                {!! csrf_field() !!}

                @if(isset($formData))
                    <input type="hidden" name="_method" value="PUT">
                @endif

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="name">*@lang('form.name')</label>
                        <input type="text" name="name" class="form-control" id="name"
                               placeholder="@lang('form.name')..."
                               value="{{ isset($formData) ? $formData->name : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="email">*@lang('form.email_address')</label>
                        <input type="email" name="email" class="form-control" id="email"
                               placeholder="@lang('form.email_address')..."
                               value="{{ isset($formData) ? $formData->email : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4">
                        <label>@lang('form.select_number')</label>
                        <select multiple class="form-control select2" name="number_id[]" id="number_id">
                        </select>
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('realtor.index', app()->getLocale())])
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    @include('partials.form.select2', ['route' => route('number.get-numbers', app()->getLocale())])

    @if(\Route::currentRouteName()  == 'realtor.edit')
        <script>
            $.ajax({
                url: '{{ route('realtor.get-numbers', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                success: function (data) {
                    var options = [];

                    for (var key in data) {
                        options.push(new Option(data[key].number, data[key].id, true, true))
                    }

                    $('#number_id').append(options).trigger('change');
                }
            });
        </script>
    @endif
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@stop