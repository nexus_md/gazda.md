@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($formData) ? __('sidebar.update_number') : __('sidebar.create_number') }}</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ isset($formData) ? route('number.update', [app()->getLocale(), $formData->id]) : route('number.store', app()->getLocale()) }}"
                  method="POST"
                  data-success-callback="successCallback" data-error-callback="errorCallback">
                {!! csrf_field() !!}

                @if(isset($formData))
                    <input type="hidden" name="_method" value="PUT">
                @endif

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="number">*@lang('sidebar.number')</label>
                        <input type="text" name="number" class="form-control" id="number"
                               placeholder="@lang('sidebar.number')..."
                               value="{{ isset($formData) ? $formData->number : '' }}">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label>*@lang('form.select_realtor')</label>
                        <select class="form-control select2" name="user_id" id="user_id">
                        </select>
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('number.index', app()->getLocale())])
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $('#user_id').select2({
            ajax: {
                url: '{{ route('realtor.get-users', app()->getLocale()) }}',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page || 1
                    };
                },

                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var result = data.data.map(function (value) {
                        return {
                            id: value.id,
                            text: value.name
                        }
                    });

                    return {
                        results: result,
                        pagination: {
                            more: (params.page * data.per_page) < data.total
                        }
                    }
                }
            }
        });
    </script>

    @if(\Route::currentRouteName() == 'number.edit')
        <script>
            $.ajax({
                url: '{{ route('realtor.get-number-user', [app()->getLocale(), $formData->id]) }}',
                method: 'GET',
                success: function (data) {
                    $('#user_id').append(new Option(data.name, data.id, true, true)).trigger('change');
                }
            });
        </script>
    @endif
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@stop