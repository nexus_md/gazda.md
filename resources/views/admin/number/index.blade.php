@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">@lang('sidebar.view_numbers')</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>@lang('sidebar.number')</th>
                    <th>@lang('form.name')</th>
                    <th>@lang('resource_view.object_count')</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($data))
                    @foreach($data as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->number }}</td>
                            <td>{{ $value->user ? $value->user->name : '' }}</td>
                            <td>{{ $value->object_count }}</td>
                            <td><a href="{{ route('number.edit', [app()->getLocale(), $value->id]) }}"><i
                                            class="fa fa-edit"></i> <span>@lang('resource_view.edit')</span></a></td>
                            <td id="{{ $value->id }}"><a class="remove-entity"
                                                         href="{{ route('number.destroy', [app()->getLocale(), $value->id]) }}"><i
                                            class="fa fa-remove"></i>
                                    @lang('resource_view.delete')</a></td>
                        </tr>
                    @endforeach
                @else
                    <tr class="odd text-center">
                        <td valign="top" colspan="4" class="dataTables_empty">@lang('resource_view.no_data')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            <div class="pull-right">
                {!! $data->links() !!}
            </div>
        </div>
        <!-- /.box-body -->
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>

    @include('partials.view.warning')
@stop

@section('js')
    <script src="{{ asset('js/resource_view.js') }}"></script>
@stop