@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">@lang('sidebar.view_actions')</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>id</th>
                    <th>@lang('sidebar.object')</th>
                    <th>@lang('sidebar.realtor')</th>
                    <th>@lang('form.type')</th>
                    <th>@lang('form.from')</th>
                    <th>@lang('form.to')</th>
                    <th>@lang('form.advance_price')</th>
                    <th>@lang('form.passive_price')</th>
                    <th>@lang('form.total_price')</th>
                </tr>
                </thead>
                <tbody>
                @if(count($data))
                    @foreach($data as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->object->id }}
                                - {{ "{$value->object->city->name}, {$value->object->address}" }}</td>
                            <td>{{ $value->user->name }}</td>
                            <td>@lang(config('estate.action_type')[$value->type])</td>
                            <td>{{ $value->from ? $value->from->format('d.m.Y') : '' }}</td>
                            <td>{{ $value->to ? $value->to->format('d.m.Y') : '' }}</td>
                            <td>{{ $value->advance_price }}</td>
                            <td>{{ $value->passive_price }}</td>
                            <td>{{ $value->total_price }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="odd text-center">
                        <td valign="top" colspan="9" class="dataTables_empty">@lang('resource_view.no_data')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            <div class="pull-right">
                {!! $data->links() !!}
            </div>
        </div>
        <!-- /.box-body -->
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
@stop