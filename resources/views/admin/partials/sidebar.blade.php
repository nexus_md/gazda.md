<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview {{ strpos($currentRoute, 'realtor') !== false ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>@lang('sidebar.realtor')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'realtor.create' ? 'active' : '' }}"><a
                                href="{{ route('realtor.create', app()->getLocale()) }}"><i
                                    class="fa fa-user-plus"></i> @lang('sidebar.create_realtor')</a></li>
                    <li class="{{ $currentRoute == 'realtor.index' ? 'active' : '' }}"><a
                                href="{{ route('realtor.index', app()->getLocale()) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_realtors')</a></li>
                </ul>
            </li>

            <li class="treeview {{ strpos($currentRoute, 'object') !== false ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>@lang('sidebar.object')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'object.create' ? 'active' : '' }}"><a
                                href="{{ route('object.create', app()->getLocale()) }}"><i
                                    class="fa fa-plus"></i> @lang('sidebar.create_object')</a></li>
                    <li class="{{ $currentRoute == 'object.index' ? 'active' : '' }}"><a
                                href="{{ route('object.index', app()->getLocale()) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_objects')</a></li>
                </ul>
            </li>

            <li class="treeview {{ strpos($currentRoute, 'number') !== false ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-phone-square"></i>
                    <span>@lang('sidebar.number')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'number.create' ? 'active' : '' }}"><a
                                href="{{ route('number.create', app()->getLocale()) }}"><i
                                    class="fa fa-plus"></i> @lang('sidebar.create_number')</a></li>
                    <li class="{{ $currentRoute == 'number.index' ? 'active' : '' }}"><a
                                href="{{ route('number.index', app()->getLocale()) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_numbers')</a></li>
                </ul>
            </li>

            <li class="treeview {{ strpos($currentRoute, 'plan') !== false ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-image"></i>
                    <span>@lang('sidebar.plan')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'plan.create' ? 'active' : '' }}"><a
                                href="{{ route('plan.create', app()->getLocale()) }}"><i
                                    class="fa fa-plus"></i> @lang('sidebar.create_plan')</a></li>
                    <li class="{{ $currentRoute == 'plan.index' ? 'active' : '' }}"><a
                                href="{{ route('plan.index', app()->getLocale()) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_plans')</a></li>
                </ul>
            </li>

            <li class="{{ $currentRoute == 'action.index' ? 'active' : '' }}"><a
                        href="{{ route('action.index', app()->getLocale()) }}"><i
                            class="fa fa-exclamation"></i> @lang('sidebar.view_actions')</a></li>

            <li class="treeview {{ strpos($currentRoute, 'pages') !== false ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-file"></i>
                    <span>@lang('sidebar.pages')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'pages.create' ? 'active' : '' }}"><a
                                href="{{ route('pages.create', app()->getLocale()) }}"><i
                                    class="fa fa-plus"></i> @lang('sidebar.create_page')</a></li>
                    <li class="{{ $currentRoute == 'pages.index' ? 'active' : '' }}"><a
                                href="{{ route('pages.index', app()->getLocale()) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_pages')</a></li>
                </ul>
            </li>

            <li class="treeview {{ strpos($currentRoute, 'facility') !== false ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-check"></i>
                    <span>@lang('sidebar.facility')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'facility.create' ? 'active' : '' }}"><a
                                href="{{ route('facility.create', app()->getLocale()) }}"><i
                                    class="fa fa-plus"></i> @lang('sidebar.create_facility')</a></li>
                    <li class="{{ $currentRoute == 'facility.index' ? 'active' : '' }}"><a
                                href="{{ route('facility.index', app()->getLocale()) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_facilities')</a></li>
                </ul>
            </li>

            <li class="{{ $currentRoute == 'comment.index' ? 'active' : '' }}"><a
                        href="{{ route('comment.index', app()->getLocale()) }}"><i
                            class="fa fa-exclamation"></i> @lang('sidebar.view_comments')</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>