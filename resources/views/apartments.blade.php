@extends('layouts.app')

@section('content')
    <div class="grid-map-section d-none d-lg-block">
        <div class="view-map-wrapper">
            <button class="view-map-button">
                <i class="fas fa-expand-arrows-alt"></i>
                Развернуть карту
            </button>
        </div>
        <div id="gmaps" width="100%" style="height: 150px;"></div>
        <div class="overlay d-none">
            <i class="fas fa-sync-alt fa-spin"></i>
        </div>
    </div>

    <div class="top-offset grid-page-wrapper side-offset">
        <div class="product-grid-wrapper row">

            <div class="col-lg-4 col-12 grid-filter-section">
                <div class="grid-filter-inner">

                    <div class="filter-mobile-toggle">
                        <button id="filter-md-display" class="btn-lg d-lg-none">
                            Пойск по фильтрам
                            <i class="fas fa-angle-down"></i>
                        </button>
                    </div>

                    <div class="filter-section d-lg-block">
                        <form action="{{ route('apartments.index', app()->getLocale()) }}" method="GET">
                            <!-- Regions -->
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.region')
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse {{ Input::get('cities') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body" id="cities">
                                        @foreach($cities as $value)
                                            <div class="form-group col-12 regions">
                                                <input type="checkbox"
                                                       value="{{ $value->id }}"
                                                       {{ in_array($value->id, (array) Input::get('parent_cities')) ? 'checked' : '' }} name="parent_cities[]">
                                                <label data-id="{{ $value->id }}">{{ $value->name }}</label>
                                                <div class="sub"></div>
                                            </div>
                                        @endforeach
                                        <a href="{{ route('city.get-parent-regions') }}" class="get-parent-regions">
                                            @lang('search_sidebar.all_regions')</a>
                                    </div>
                                </div>
                                <div class="overlay d-none">
                                    <i class="fas fa-sync-alt fa-spin"></i>
                                </div>
                            </div>

                            <!-- House / Apartment -->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#types"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.types')
                                        </a>
                                    </h5>
                                </div>

                                <div id="types" class="collapse {{ Input::get('types') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        @foreach(config('estate.object_type') as $key => $value)
                                            <div class="form-group col-12">
                                                <input type="checkbox" id="{{ $value.$key }}" name="types[]"
                                                       value="{{ $key }}" {{ in_array($key, (array) Input::get('types')) ? 'checked' : '' }}>
                                                <label for="{{ $value.$key }}">@lang($value)</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <!-- Price type -->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#price_type"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.lease')
                                        </a>
                                    </h5>
                                </div>

                                <div id="price_type" class="collapse {{ Input::get('price_type') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        @foreach(config('estate.object_price_type') as $key => $value)
                                            <div class="form-group col-12">
                                                <input type="checkbox" id="{{ $value.$key }}" name="price_type[]"
                                                       value="{{ $key }}" {{ in_array($key, (array) Input::get('price_type')) ? 'checked' : '' }}>
                                                <label for="{{ $value.$key }}">@lang($value)</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <!-- Price -->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#price"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.price')
                                        </a>
                                    </h5>
                                </div>

                                <div id="price"
                                     class="collapse {{ Input::get('fromPrice') || Input::get('toPrice') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-12 col-md-6">
                                                <input type="text" class="form-control" name="fromPrice"
                                                       placeholder="From..."
                                                       value="{{ Input::get('fromPrice') }}">
                                            </div>
                                            <div class="form-group col-12 col-md-6">
                                                <input type="text" class="form-control" name="toPrice"
                                                       placeholder="To..."
                                                       value="{{ Input::get('toPrice') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--  Construction type	-->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#construction_type"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.construction')
                                        </a>
                                    </h5>
                                </div>

                                <div id="construction_type"
                                     class="collapse {{ Input::get('construction_type') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        @foreach(config('estate.construction_type') as $key => $value)
                                            <div class="form-group col-12">
                                                <input type="checkbox" id="{{ $value.$key }}" name="construction_type[]"
                                                       value="{{ $key }}" {{ in_array($key, (array) Input::get('construction_type')) ? 'checked' : '' }}>
                                                <label for="{{ $value.$key }}">@lang($value)</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <!--  Room Count  -->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#room_count"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.room_count')
                                        </a>
                                    </h5>
                                </div>

                                <div id="room_count"
                                     class="collapse {{ Input::get('fromroom_count') || Input::get('toroom_count') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-12 col-md-6">
                                                <input type="text" class="form-control" name="fromroom_count"
                                                       placeholder="From..."
                                                       value="{{ Input::get('fromroom_count') }}">
                                            </div>
                                            <div class="form-group col-12 col-md-6">
                                                <input type="text" class="form-control" name="toroom_count"
                                                       placeholder="To..."
                                                       value="{{ Input::get('toroom_count') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Object State -->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#object_state"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.lease')
                                        </a>
                                    </h5>
                                </div>

                                <div id="object_state" class="collapse {{ Input::get('object_state') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        @foreach(config('estate.object_state') as $key => $value)
                                            <div class="form-group col-12">
                                                <input type="checkbox" id="{{ $value.$key }}" name="object_state[]"
                                                       value="{{ $key }}" {{ in_array($key, (array) Input::get('object_state')) ? 'checked' : '' }}>
                                                <label for="{{ $value.$key }}">@lang($value)</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                            <!-- Area -->
                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#area"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.area')
                                        </a>
                                    </h5>
                                </div>

                                <div id="area"
                                     class="collapse {{ Input::get('minArea') || Input::get('maxArea') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-6">
                                                <input type="text" class="form-control" name="minArea"
                                                       placeholder="From..."
                                                       value="{{ Input::get('minArea') }}">
                                            </div>
                                            <div class="form-group col-6">
                                                <input type="text" class="form-control" name="maxArea"
                                                       placeholder="To..."
                                                       value="{{ Input::get('maxArea') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card top-offset">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#floor"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            @lang('search_sidebar.floor')
                                        </a>
                                    </h5>
                                </div>

                                <div id="floor" class="collapse {{ Input::get('floor') ? 'show' : '' }}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <input type="text" class="form-control" name="floor"
                                                       placeholder="Floor..."
                                                       value="{{ Input::get('floor') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn add-item-btn col-12 top-offset bg-blue text-white">Search
                            </button>
                        </form>

                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-12 main-grid-wrap product-grid">
                <div class="row product-grid-wrapper grid-of-3">
                    @foreach($objects as $value)
                        <div class="product-item-card-wrapper col-xs-12 col-sm-6 col-lg-4">
                            <div class="product-item-card">
                                <div class="product-item-header">
                                    <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                        <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                    </a>
                                </div>

                                <div class="product-item-content d-flex nogut">
                                    <div class="col-md-8 col-lg-8 col-xs-8 product-address">
                                        <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                            @if(isset($value->city->city->city))
                                                <h5>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}" }}</h5>
                                            @elseif(isset($value->city->city))
                                                <h5>{{ "{$value->city->city->name}, {$value->city->name}" }}</h5>
                                            @else
                                                <h5>{{ $value->city->name }}</h5>
                                            @endif
                                        </a>
                                        <p>{{ $value->address }}</p>
                                    </div>
                                    <div class="col-md-4 col-xs-4 price-block">
                                    <span class="price">
                                        {{ $value->price }} €
                                        @if(!$value->price_type)
                                            @lang('object.hour')
                                        @endif
                                    </span>
                                    </div>


                                </div>
                                <div class="description">
                                    <hr class="product-hr">
                                    <div class="col-md-12 col-xs-12 product-type">
                                        <div class="row description-row">
                                            <div class="col-md-4 col-xs-4 col-4 desc-el">
                                                @if($value->outer_type == 0)
                                                    <i class="far fa-building"></i>
                                                    <span class="apartment">
                                                @lang('object.apartment')
                                            </span>
                                                @else
                                                    <i class="fas fa-home"></i>
                                                    <span class="house">
                                                @lang('object.house')
                                            </span>
                                                @endif
                                            </div>
                                            <div class="col-md-4 col-xs-4 col-4 desc-el">
                                                <i class="fas fa-bed"></i>
                                                @if($value->room_count == 1)
                                                    <span>{{$value->room_count}}</span>
                                                @elseif($value->room_count > 1)
                                                    <span>{{$value->room_count}}</span>
                                                @endif
                                            </div>
                                            <div class="col-md-4 col-xs-4 col-4 desc-el">
                                                <i class="fas fa-ruler-horizontal"></i>
                                                <span>{{$value->area}}m<sup>2</sup></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            {!! $objects->appends(request()->query())->links() !!}
        </div>
    </div>
@stop

@section('css')
    <style>
        .side-offset{
            margin-left: 15px;
            margin-right: 15px;
        }
    </style>
@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR3nISjK4nrFsrCM2te7WdEG2s2ANzpME"></script>
    <script src="{{ asset('js/marker_clusterer.js') }}"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
      $('.get-parent-regions').click(function (e) {
        e.preventDefault()
        var url = $(this).attr('href')

        $('.card .overlay').removeClass('d-none')
        $.ajax({
          url: url,
          method: 'GET',
          success: function (data) {
            $('#cities').html(data)
            $('.card .overlay').addClass('d-none')
          },

          error: function (data) {

          }
        })
      })

      $('body').on('change', '.regions > input', function () {
        var id = $(this).val()

        if (!id) return

        var obj = $(this)

        if (!obj.prop('checked')) {
          obj.parent().find('.sub').html('')
          return
        }

        $('.card .overlay').removeClass('d-none')

        $.ajax({
          url: '{{ route('city.get-regions-tree', ['']) }}/' + id,
          method: 'GET',
          success: function (data) {
            obj.parent().find('.sub').html(data)
            $('.card .overlay').addClass('d-none')
          },

          error: function (data) {
            alert('Error !')
          }
        })
      })

      $('body').on('click', '.regions > label', function () {
        var id = $(this).attr('data-id')

        if (!id) return

        var obj = $(this)

        if (obj.hasClass('opened')) {
          obj.removeClass('opened')
          obj.parent().find('.sub').html('')
          return
        }

        obj.addClass('opened')

        $('.card .overlay').removeClass('d-none')

        $.ajax({
          url: '{{ route('city.get-regions', ['']) }}/' + id,
          method: 'GET',
          success: function (data) {
            obj.parent().find('.sub').html(data)
            $('.card .overlay').addClass('d-none')
          },

          error: function (data) {
            alert('Error !')
          }
        })
      })

      var parent_cities =
      {!! json_encode(Input::get('parent_cities')) !!}

      if (parent_cities) {
        for (var key in parent_cities) {
          var value = parent_cities[key]
          $('.regions input[value="' + value + '"]').attr('checked', '').change()
        }
      }
    </script>

    <script>
      function formatMarkers (markers) {
        for (var key in markers) {
          var value = markers[key]
          value['icon'] = '{{ url('img/marker.png') }}'
          value['click'] = function (e) {
            if (!$.isNumeric(e.infoWindow.getContent())) return

            var id = e.infoWindow.getContent()

            e.infoWindow.setContent('')
            $.ajax({
              url: '{{ route('apartments.show', [app()->getLocale(), '']) }}/' + id,
              method: 'GET',
              success: function (data) {
                e.infoWindow.setContent(data)
              },

              error: function (data) {

              }
            })
          }

          value['infoWindow'] = {
            'content': value.id.toString()
          }
        }
      }
    </script>

    <script>
      var gmap = new GMaps({
        div: '#gmaps',
        zoom: 8,
        lat: 47.010284180836166,
        lng: 28.86056900024414,
        markerClusterer: function (map) {
          options = {
            gridSize: 40,
            styles: [
              {
                textColor: 'white',
                url: '{{ url('img/cluster-marker.png') }}',
                width: 59,
                height: 59
              },
              {
                textColor: 'white',
                url: '{{ url('img/cluster-marker.png') }}',
                width: 59,
                height: 59
              },
              {
                textColor: 'white',
                url: '{{ url('img/cluster-marker.png') }}',
                width: 59,
                height: 59
              }
            ]
          }

          return new MarkerClusterer(map, [], options)
        }
      })

      $('#filters').accordion({
        collapsible: true,
      })

      $('.view-map-button').click(function () {
        var button = $(this)

        button.toggleClass('map-opened')

        if (button.attr('data-loaded') || !button.hasClass('map-opened')) {
          return
        }

        gmap.removeMarkers()
        button.attr('disabled', true)

        $('.grid-map-section .overlay').removeClass('d-none')

        $.ajax({
          url: '{{ route('apartments.get-markers', app()->getLocale()) }}' + window.location.search,
          method: 'GET',
          success: function (data) {
            formatMarkers(data)

            gmap.addMarkers(data)

            button.removeAttr('disabled')

            $('.grid-map-section .overlay').addClass('d-none')

            button.attr('data-loaded', true)
          },

          error: function (data) {
            alert('Unexpected error !')

            $('.grid-map-section .overlay').addClass('d-none')
          }
        })
      })
    </script>
@stop