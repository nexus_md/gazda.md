@extends('layouts.app')

@section('content')
    <div class="product-single product-single-wrapper container">
        <div class="product-single-title col-12">
            <h1>{{ "{$object->city->name}, {$object->address}" }} ID: {{ $object->id }}</h1>
            <p class="product-single-meta">
                <span class="type">
                    @if ($object->outer_type == 0)
                        <i class="far fa-building"></i>
                    @else
                        <i class="fas fa-home"></i>
                    @endif
                    @lang(config('estate.object_type')[$object->outer_type])
                </span>
                <span class="separator"> / </span>
                <span class="views">
                    <i class="fas fa-eye"></i>
                    100 vizualizari
                </span>
            </p>
        </div>
        {{--{{ dd($seenApartments) }}--}}
        <div class="product-single-content-wrapper col-12">
            <div class="row product-single-content-inner">
                <div class="col-12 col-lg-8 product-single-content">
                    <!-- Product Gallery -->
                    <div class="product-single-gallery">
                        <div id="gallery-for" class="row gallery-for">
                            @if($object->image)
                                <div class="featured-image col-12 col-md-12"
                                     data-src="{{ route('image', $object->image->image) }}">
                                    <img src="{{ route('image', $object->image->image) }}">
                                </div>
                            @endif

                            @foreach($object->plans as $value)
                                <div class="col-6 col-sm-4 col-md-3 col-md-2"
                                     data-src="{{ route('image', $value->image) }}">
                                    <img src="{{ route('image', $value->image) }}" alt="{{ $value->name }}"
                                         title="{{ $value->name }}">
                                </div>
                            @endforeach

                            @foreach($object->gallery as $value)
                                <div class="col-6 col-sm-4 col-md-3 col-md-2"
                                     data-src="{{ route('image', $value->image) }}">
                                    <img src="{{ route('image', $value->image) }}">
                                </div>
                            @endforeach
                        </div>
                        {{--<div class="gallery-nav">--}}
                        {{--@foreach($object->gallery as $value)--}}
                        {{--<div>--}}
                        {{--<img src="{{ route('image', $value->image) }}">--}}
                        {{--</div>--}}
                        {{--@endforeach--}}
                        {{--@foreach($object->plans as $value)--}}
                        {{--<div>--}}
                        {{--<img src="{{ route('image', $value->image) }}" alt="{{ $value->name }}"--}}
                        {{--title="{{ $value->name }}">--}}
                        {{--</div>--}}
                        {{--@endforeach--}}
                        {{--</div>--}}
                    </div>
                    <hr class="gray-separator">

                    {{-- Mobile / Tablet only price info --}}
                    <div class="product-single-sidebar d-lg-none">

                        <div class="product-single-rent-info">
                            <div class="card">
                                <div class="card-header price">
                                    <h5 class="value">
                                        <span>цена: </span>
                                        {{ $object->price }} €
                                        @lang(config('estate.object_price_type')[$object->price_type])
                                    </h5>
                                </div>

                                <div class="card-body">

                                    <div class="deal-desc">
                                        {{ $object->translation ? $object->translation->description : '' }}
                                    </div>

                                    <hr class="gray-separator">

                                    <div class="deal-phone">
                                        @foreach($object->numbers as $value)
                                            <h5>
                                                <a href="tel:{{ $value->number }}">
                                                    <i class="fas fa-phone"></i>
                                                    <span>Kонтакты: </span>
                                                    {{ $value->number }}
                                                </a>
                                            </h5>
                                        @endforeach
                                    </div>

                                    <hr class="gray-separator">

                                    <div class="deal-available">
                                        <p>доступен в:</p>
                                        <div class="col-12">
                                            <a href="#">
                                                <i class="fab fa-whatsapp-square"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fab fa-telegram"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fab fa-viber"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- Page Description -->
                    <div class="product-single-description">
                        {!! $object->translation ? $object->translation->text : '' !!}
                    </div>

                    <div class="gray-box col-12 col-md-12 product-single-attributes">
                        <h3>Атрибуты объекта</h3>
                        <ul>
                            @foreach($properties as $key => $value)
                                @if ($value)
                                    <li>
                                        <span>
                                            @lang('object.'.$key)
                                        </span>

                                        <span>
                                            @if($key == 'parking_lot' || $key == 'construction_type' || $key == 'object_type')
                                                @lang( config('estate.'.$key)[$object->$key] )
                                            @else
                                                {{ $value }}
                                                {!! $key == 'kitchen_area' || $key == 'living_area' || $key == 'area' ? '<span>m<sup>2</sup></span>' : '' !!}
                                            @endif
                                        </span>

                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>

                    <div id="accordion" class="product-single-planning">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne">
                                        Планировка
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                 data-parent="#accordion">
                                @foreach($object->plans as $value)
                                    <div class="card-body">
                                        <img src="{{ route('image', $value->image) }}" alt="{{ $value->name }}"
                                             title="{{ $value->name }}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @if( $object->facilities->count() )
                        <div class="gray-box col-12 col-md-12 product-single-additional">
                            <h3>дополнительно</h3>
                            <ul>
                                @foreach($object->facilities as $value)
                                    <li>{{ $value->translation->name }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="col-12 col-lg-4 product-single-sidebar">

                    <div class="d-none d-lg-block product-single-rent-info">
                        <div class="card">
                            <div class="card-header price">
                                <h5 class="value">
                                    <span>цена: </span>
                                    {{ $object->price }} €
                                    @lang(config('estate.object_price_type')[$object->price_type])
                                </h5>
                            </div>

                            <div class="card-body">

                                <div class="deal-desc">
                                    {{ $object->translation ? $object->translation->description : '' }}
                                </div>

                                <hr class="gray-separator">

                                <div class="deal-phone">
                                    @foreach($object->numbers as $value)
                                        <a href="tel:{{  $value->number }}">
                                            <h5>
                                                <i class="fas fa-phone"></i>
                                                <span>Kонтакты: </span>
                                                {{ $value->number }}
                                            </h5>
                                        </a>
                                    @endforeach
                                </div>

                                <hr class="gray-separator">

                                <div class="deal-available">
                                    <p>доступен в:</p>
                                    <div class="col-12">
                                        <a href="#">
                                            <i class="fab fa-whatsapp-square"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-telegram"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-viber"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product-single-map">
                        <div id="gmaps" style="width: 100%; height: 450px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($seenApartments))
        <div class="product-single-viewed-wrap container-fluid">
            <div class="product-single-viewed container main-grid-wrap product-grid">
                <div class="row product-single-viewed-inner product-grid-wrapper grid-of-4">
                @foreach($seenApartments as $value)

                        <div class="product-item-card-wrapper col-xs-12">
                            <div class="product-item-card">
                                <div class="product-item-header">
                                    <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                        <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                    </a>
                                </div>

                                <div class="product-item-content d-flex nogut">
                                    <div class="col-xs-8 col-md-8 product-address">
                                        <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                            @if(isset($value->city->city->city))
                                                <h5>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}" }}</h5>
                                            @elseif(isset($value->city->city))
                                                <h5>{{ "{$value->city->city->name}, {$value->city->name}" }}</h5>
                                            @else
                                                <h5>{{ $value->city->name }}</h5>
                                            @endif
                                        </a>
                                        <p>{{ $value->address }}</p>
                                    </div>
                                    <div class="col-md-4 col-xs-4 price-block">
                                        <span class="price">
                                            {{ $value->price }} €
                                            @if(!$value->price_type)
                                                @lang('object.hour')
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                @endforeach
                </div>
            </div>
        </div>
    @endif

@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}">

    <style>
        .product-single-viewed .product-item-card{
            margin-right: 15px;
        }
        .product-single-viewed .product-item-content{
            height: 135px;
        }
    </style>
@stop

@section('js')
    <script src="{{ asset('js/slick.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR3nISjK4nrFsrCM2te7WdEG2s2ANzpME"></script>
    <script src="{{ asset('js/marker_clusterer.js') }}"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>
    <script src="{{ asset('js/lightgallery-all.min.js') }}"></script>

    <script>
        //        $('.gallery-for').slick({
        //            slidesToShow: 1,
        //            slidesToScroll: 1,
        //            arrows: false,
        //            fade: true,
        //            asNavFor: '.gallery-nav'
        //        });
        //        $('.gallery-nav').slick({
        //            slidesToShow: 3,
        //            slidesToScroll: 1,
        //            asNavFor: '.gallery-for',
        //            dots: true,
        //            centerMode: true,
        //            focusOnSelect: true
        //        });
        // Slick for seen posts

        $('.product-single-viewed-inner').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
        });

        $('#gallery-for').lightGallery({
            thumbnail: true,
            animateThumb: true,
            showThumbByDefault: true
        });
    </script>

    <script>
        var gmap = new GMaps({
            div: '#gmaps',
            zoom: 15,
            lat: {{ $object->lat }},
            lng: {{ $object->lng }},
        });

        gmap.addMarker(
            {
                lat: {{ $object->lat }},
                lng: {{ $object->lng }},
            }
        );

    </script>
@stop