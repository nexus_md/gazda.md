@extends('layouts.app')

@section('content')
    <div class="container">

        <form class="ajax"
              id="create_object_form"
              action="{{ route('apartments.store', app()->getLocale()) }}"
              method="POST"
              data-success-callback="successRedirect"
              data-error-callback="errorCallback">
            {!! csrf_field() !!}


            <div class="image-positions"></div>

            <div class="uploaded-images-positions">
            </div>
            <div class="card">
                <div class="card-header" id="headingAddress">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseAddress"
                                aria-expanded="true" aria-controls="collapseAddress" type="button">
                            Address
                        </button>
                    </h5>
                </div>
                <div id="collapseAddress" class="collapse show container-fluid" aria-labelledby="headingAddress">
                    <div class="row">
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="city">@lang('form.city')</label>
                            <select class="form-control regions" id="city">
                                <option value="0">Select</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="settlement">@lang('form.settlement')</label>
                            <select class="form-control regions" id="settlement">
                                    {{--@foreach($formData->city->city->city->regions as $value)--}}
                                        {{--<option value="{{ $value->id }}" {{ $formData->city->city->id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>--}}
                                    {{--@endforeach--}}
                            </select>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="city_id">@lang('form.section')</label>
                            <select class="form-control regions" id="city_id" name="city_id">
                                    {{--@foreach($formData->city->city->regions as $value)--}}
                                        {{--<option value="{{ $value->id }}" {{ $formData->city->id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>--}}
                                    {{--@endforeach--}}
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 address">
                            <label for="address">@lang('form.address')</label>
                            <input type="text" name="address" class="form-control col-8" id="address">
                            <div class="btn btn-primary" id="search-addr">@lang('form.search')</div>
                            <span class="help-block error-message"></span>
                        </div>


                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="room_count">@lang('form.entrance')</label>
                            <input type="text" name="entrance" class="form-control" id="entrance"
                                   placeholder="@lang('form.entrance')...">
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="room_count">*@lang('form.apartment_nr')</label>
                            <input type="text" name="apartment_nr" class="form-control" id="apartment_nr"
                                   placeholder="@lang('form.apartment_nr')...">
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="col-12">
                            <div id="gmaps"></div>
                            <input type="hidden" name="lat" id="lat" >
                            <input type="hidden" name="lng" id="lng" >
                            <span class="help-block error-message"></span>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingAttributes">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseAttributes"
                                aria-expanded="true" aria-controls="collapseAttributes" type="button">
                            Apartment Attributes
                        </button>
                    </h5>
                </div>
                <div id="collapseAttributes" class="collapse show container-fluid" aria-labelledby="headingAttributes">

                    <div class="row">

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="area">*@lang('form.area') m<sup>2</sup> </label>
                            <input type="text" name="area" class="form-control" id="area"
                                   placeholder="@lang('form.area')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="area">*@lang('form.living_area') m<sup>2</sup> </label>
                            <input type="text" name="living_area" class="form-control" id="living_area"
                                   placeholder="@lang('form.living_area')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="area">*@lang('form.kitchen_area') m<sup>2</sup> </label>
                            <input type="text" name="kitchen_area" class="form-control" id="kitchen_area"
                                   placeholder="@lang('form.kitchen_area')..." ">
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="room_count">*@lang('form.room_count')</label>
                            <input type="text" name="room_count" class="form-control" id="room_count"
                                   placeholder="@lang('form.room_count')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="floor">*@lang('form.floor_count')</label>
                            <input type="text" name="floor_count" class="form-control" id="floor_count"
                                   placeholder="@lang('form.floor_count')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="floor">*@lang('form.floor')</label>
                            <input type="text" name="floor" class="form-control" id="floor"
                                   placeholder="@lang('form.floor')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="floor">*@lang('form.balcony_count')</label>
                            <input type="text" name="balcony_count" class="form-control" id="balcony_count"
                                   placeholder="@lang('form.balcony_count')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-sm-6 col-md-4 col-12">
                            <label>*@lang('form.select_object_state')</label>
                            <select class="form-control" name="type" id="type">
                                @foreach(config('estate.object_state') as $key => $value)
                                    <option value="{{ $key }}" >@lang($value)</option>
                                @endforeach
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label>*@lang('form.select_object_type')</label>
                            <select class="form-control" name="outer_type" id="outer_type">
                                @foreach(config('estate.object_type') as $key => $value)
                                    <option value="{{ $key }}" }}>@lang($value)</option>
                                @endforeach
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label>*@lang('form.parking_lot')</label>
                            <select class="form-control" name="parking_lot" id="parking_lot">
                                @foreach(config('estate.parking_lot') as $key => $value)
                                    <option value="{{ $key }}" >@lang($value)</option>
                                @endforeach
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label>*@lang('form.select_construction_type')</label>
                            <select class="form-control" name="construction_type" id="construction_type">
                                @foreach(config('estate.construction_type') as $key => $value)
                                    <option value="{{ $key }}" >@lang($value)</option>
                                @endforeach
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="plan_id">@lang('form.select_plan')</label>
                            <select class="form-control" name="plan_id[]" id="plan_id" multiple>
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                    </div>

                </div>
            </div>

            <div class="card">

                <div class="card-header" id="headingPrice">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapsePrice"
                                aria-expanded="true" aria-controls="collapsePrice" type="button">
                            Price
                        </button>
                    </h5>
                </div>

                <div id="collapsePrice" class="collapse show container-fluid" aria-labelledby="headingPrice">
                    <div class="row">

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label>*@lang('form.price')</label>
                            <input type="text" name="price" class="form-control" id="price"
                                   placeholder="@lang('form.price')..." >
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label>*@lang('form.currency')</label>
                            <select class="form-control" name="currency_id" id="currency_id">
                                @foreach($currency as $key => $value)
                                    <option value="{{ $value->id }}" >{{ $value->acronym }}</option>
                                @endforeach
                            </select>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label>*@lang('form.price_type')</label>
                            <select class="form-control" name="price_type" id="price_type">
                                @foreach(config('estate.object_price_type') as $key => $value)
                                    <option value="{{ $key }}" >@lang($value)</option>
                                @endforeach
                            </select>
                            <span class="help-block error-message"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">

                <div class="card-header" id="headingMedia">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseMedia"
                                aria-expanded="true" aria-controls="collapseMedia" type="button">
                            Media
                        </button>
                    </h5>
                </div>

                <div id="collapseMedia" class="collapse show container-fluid" aria-labelledby="headingMedia">
                    <div class="row">

                        <div class="form-group col-12">
                            <label for="image">*@lang('form.image')</label>
                            <input type="file" name="image" class="form-controll image invisible" id="image">
                            <div class="image-placeholder"
                                 style="no-repeat; background-size: cover">
                                <div class="image-trigger"><i class="fa fa-plus"></i></div>
                            </div>
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="form-group col-12">
                            <label for="images" class="upload-images">@lang('form.click_to_upload_images')</label>
                            <input type="file" name="images[]" id="images" multiple class="hidden invisible images">
                            <span class="help-block error-message"></span>
                        </div>

                        <div class="col-12 images-placeholder"></div>

                    <input type="text" class="hidden invisible" name="owner_name" value="{{ Auth::user()->name }}">
                    <input type="text" class="hidden invisible" name="owner_number" value="{{ Auth::user()->pers_number }}">
                    <input type="text" class="hidden invisible" name="owner_email" value="{{ Auth::user()->email }}">
                    </div>

                    {{--<div class="row">--}}
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="video">@lang('form.video')</label>--}}
                                {{--<textarea name="video" id="video" class="form-control"--}}
                                          {{--rows="10"></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>
            </div>

            <div class="card">

                <div class="card-header" id="headingFacilities">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFacilities"
                                aria-expanded="true" aria-controls="collapseFacilities" type="button">
                            Facilities
                        </button>
                    </h5>
                </div>

                <div id="collapseFacilities" class="collapse show container-fluid" aria-labelledby="headingFacilities">
                    <div class="row">
                        @foreach($facilities as $key => $value)
                            <div class="col-12 col-sm-6 col-md-4">
                                <p></p>
                                <label for="facility-{{ $key }}">
                                    {{ $value->translation ? $value->translation->name : '' }}
                                    <input type="checkbox" class="minimal" name="facilities[]"
                                           value="{{ $value->id }}"
                                            {{--{{ isset($formData) && $formData->facilities && array_filter($formData->facilities->all(), function($element) use ($value) { return $value->id === $element->id; }) ? 'checked' : '' }}--}}
                                    >
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


            {{--<div class="something">--}}


                {{--<div class="row">--}}
                    {{--<div class="col-12 form-group">--}}
                        {{--@foreach($facilities as $key => $value)--}}
                            {{--<div class="col-12 col-sm-6 col-md-4">--}}
                                {{--<p></p>--}}
                                {{--<label for="facility-{{ $key }}">--}}
                                    {{--{{ $value->translation ? $value->translation->name : '' }}--}}
                                    {{--<input type="checkbox" class="minimal" name="facilities[]"--}}
                                           {{--value="{{ $value->id }}" {{ isset($formData) && $formData->facilities && array_filter($formData->facilities->all(), function($element) use ($value) { return $value->id === $element->id; }) ? 'checked' : '' }}>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--@if(isset($formData))--}}
                    {{--<div class="col-12 uploaded-images-placeholder">--}}
                        {{--@include('admin.object.partials.uploaded_images', ['gallery' => $formData->gallery])--}}
                    {{--</div>--}}
                {{--@endif--}}
            {{--</div>--}}
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
            </div>
        </form>

    </div>
@stop

@section('js')

    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('js/handle_image.js') }}"></script>
    <script src="{{ asset('js/handle_images.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR3nISjK4nrFsrCM2te7WdEG2s2ANzpME"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>
    <script src="{{ asset('js/sortable.js') }}"></script>

    <script>
        $('.regions').change(function () {
            var select = $(this).parent().next().find('.regions');
            if (!select.length) return;

            var current = this;
            var clear = false;
            $('.regions').each(function (i, item) {
                if (current === item) {
                    clear = true;
                    return;
                }

                if (clear) {
                    $(item).html('');
                }
            });

            $('.overlay').removeClass('hidden invisible');

            $.ajax({
                url: '{{ route('city.get-raw-regions', ['']) }}/' + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $(select).append($('<option>', {
                        value: 0,
                        text: 'Select'
                    }));
                    $.each(data, function (i, item) {
                        $(select).append($('<option>', {
                            value: item.id,
                            text: item.name,
                        }));
                    });

                    $('.overlay').addClass('hidden invisible');
                },
                error: function (data) {

                }
            });

        });

        var gmap = new GMaps({
            div: '#gmaps',
            lat: {{ isset($formData) ? $formData->lat : 47.010284180836166 }},
            lng: {{ isset($formData) ? $formData->lng : 28.86056900024414 }},
            click: function (e) {
                gmap.removeMarkers();
                gmap.addMarker({
                    lat: e.latLng.lat(),
                    lng: e.latLng.lng()
                });

                $('#lat').val(e.latLng.lat()).change();
                $('#lng').val(e.latLng.lng()).change();

                $('#address').blur();
            },
            dragend: function (e) {
                $('#address').blur();
            }
        });

        @if(isset($formData))
        gmap.addMarker({
            lat: {{ $formData->lat }},
            lng: {{ $formData->lng }}
        });
        @endif

        $('#search-addr').click(function (e) {
            var name = '';

            $('.regions').each(function (i, item) {
                name += $(item).find('option:selected').text() + ', ';
            });

            console.log(name + $('#address').val());

            GMaps.geocode({
                address: name + $('#address').val(),
                callback: function (results, status) {
                    if (status === 'OK') {
                        var latlng = results[0].geometry.location;
                        gmap.removeMarkers();
                        gmap.setCenter(latlng.lat(), latlng.lng());
                        gmap.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng()
                        });

                        $('#lat').val(latlng.lat()).change();
                        $('#lng').val(latlng.lng()).change();
                    }
                }
            });
        });


        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('.confirm-action').click(function () {
            var url = $(this).attr('data-href');
            var id = $(this).attr('data-id');
            var obj = $('span[data-href="' + url + '"]');
            $('.overlay').removeClass('hidden invisible');

            $.ajax({
                url: url,
                method: 'DELETE',
                data: {_token: Laravel.csrfToken},
                success: function (data) {
                    obj.parent().fadeOut(500, function () {
                        obj.parent().remove();
                    });

                    $('.overlay').addClass('hidden invisible');
                    $('input[name="uploaded_images_positions[]"][value="' + id + '"]').remove();
                },
                error: function (data) {
                    console.log(data);
                    alert('Unexpected error.');
                    $('.overlay').addClass('hidden invisible');
                }
            });
        });

        $('.uploaded-images-placeholder span').on('click', function () {
            $('#modal-warning').modal();
            $('.confirm-action').attr('data-href', $(this).attr('data-href'));
            $('.confirm-action').attr('data-id', $(this).attr('data-id'));
        });

        $('#plan_id').select2({
            ajax: {
                url: '{{ route('plan.get-plans', app()->getLocale()) }}',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page || 1
                    };
                },

                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var result = data.data.map(function (value) {
                        return {
                            id: value.id,
                            text: value.name
                        }
                    });

                    return {
                        results: result,
                        pagination: {
                            more: (params.page * data.per_page) < data.total
                        }
                    }
                }
            }
        });

        var imagePositions = $('.image-positions');
        var uploadedImagesPositions = $('.uploaded-images-positions');

        $('.images-placeholder').sortable({
            stop: function (event, ui) {
                imagePositions.html('');
                $(event.target).children().each(function (key, value) {
                    imagePositions.append('<input type="hidden" name="image_positions[' + $(this).attr('data-pos') + ']" value="' + key + '">');
                });
            }
        });

        $('.uploaded-images-placeholder').sortable({
            stop: function (event, ui) {
                uploadedImagesPositions.html('');
                $(event.target).children().each(function (key, value) {
                    uploadedImagesPositions.append('<input type="hidden" name="uploaded_images_positions[]" value="' + $(this).find('span').attr('data-id') + '">');
                });
            }
        });

        $('form .images').change(function () {
            imagePositions.html('');
        });
    </script>

    <script>
        function successRedirect(form, data) {
            successCallback(form, data);
//            window.location.replace(data);
        }

        function successUpdate(form, data) {
            successCallback(form, data);
            form.find('#images').val('');
            form.find('#image').val('');

            $('.uploaded-images-placeholder').append(data);
            $('.images-placeholder, .image-positions').html('');
        }
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/add_item.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sortable.css') }}">
@stop