<div class="map-info-card product-item-card-wrapper">
    <div class="product-item-card">
        <div class="product-item-header">
            <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
            </a>
            <span class="price">
                                   {{ $value->price }} €
                @if(!$value->price_type)
                    @lang('object.hour')
                @endif
            </span>
        </div>

        <div class="product-item-content d-flex nogut">
            <div class="col-12 product-address">
                <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                    <h5>{{ $value->city->name }}</h5>
                </a>
                <p>{{ $value->address }}</p>
            </div>
        </div>
    </div>
</div>