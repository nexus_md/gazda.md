<div class="modal modal-warning fade" id="modal-warning">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to complete this action ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right confirm-action" data-dismiss="modal">Yes</button>
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>