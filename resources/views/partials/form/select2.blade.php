<script>
    $('#number_id').select2({
        ajax: {
            url: '{{ $route }}',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term,
                    page: params.page || 1
                };
            },

            processResults: function (data, params) {
                params.page = params.page || 1;

                var result = data.data.map(function (value) {
                    return {
                        id: value.id,
                        text: (value.user ? value.user.name : 'Free') + ' - ' + value.number + ': ' + value.object_count
                    }
                });

                return {
                    results: result,
                    pagination: {
                        more: (params.page * data.per_page) < data.total
                    }
                }
            }
        }
    });
</script>