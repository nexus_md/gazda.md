@foreach($cities as $value)
    <div class="form-group {{ $grid }} regions">
        <input type="checkbox"
               value="{{ $value->id }}" {{ $checked }} name="cities[]">
        <label data-id="{{ $value->id }}">{{ $value->name }}</label>

        <div class="sub">
            @foreach(collect($value->regions) as $region)
                <div class="form-group col-11 offset-1">
                    <input type="checkbox" name="cities[]" id="city-{{ $region->id }}"
                           value="{{ $region->id }}" {{ $checked }}>
                    <label for="city-{{ $region->id }}">{{ $region->name }}</label>
                </div>
            @endforeach
        </div>
    </div>
@endforeach