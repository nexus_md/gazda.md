<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Gazda.md</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--    Font family     -->
    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat" rel="stylesheet">

    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js"
            integrity="sha384-4oV5EgaV02iISL2ban6c/RmotsABqE4yZxZLcYMAdG7FAPsyHYAPpywE9PJo+Khy"
            crossorigin="anonymous"></script>

    <!--  I have no clue why I'm even commenting this, but still - BOOTSTRAP  -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/products.css') }}">

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    @yield('css')
</head>

<body>
<header>

    <!--    Top bar    -->
    <div class="top-bar container-fluid ">

        <div class="container d-flex flex-row align-items-stretch justify-content-between nogut">

            <!--Contacts -->
            <div class="contacts">
                <a class="phone d-none d-sm-inline" href="mailto:info@gazda.md">
                    <i class="fas fa-envelope"></i>
                    <span>
                            info@gazda.md
                        </span>
                </a>
                <a class="phone-nr" href="tel:079278997">
                    <i class="fas fa-phone"></i>
                    <span>022789635</span>
                    <span class="separator">/</span>
                    <span>079278997</span>
                </a>
            </div>

            <!-- Social Links -->
            <div class="socials d-none d-md-block">

                <a class="facebook" href="#">
                    <i class="fab fa-facebook-f"></i>
                </a>

                <a class="odnoklassniki" href="#">
                    <i class="fab fa-odnoklassniki"></i>
                </a>

                <a class="vk" href="#">
                    <i class="fab fa-vk"></i>
                </a>

                <a class="instagram" href="#">
                    <i class="fab fa-instagram"></i>
                </a>

            </div>

            <!-- Language Switching side -->
            <div class="langs d-none d-md-block">
                @foreach($locales as $key => $url)
                    <a href="{{url($url)}}"
                       class="{{ $key === app()->getLocale() ? 'active' : '' }}">{{ strtoupper($key) }}</a>
                    <span class="separator">/</span>
                @endforeach
            </div>

            <!-- The Search Box -->
            <div class="search-bar d-none d-lg-block">
                <form class="search-form" action="{{ route('apartments.index', app()->getLocale()) }}" method="get">
                    <input type="text" placeholder="Пойск по ID" name="search">
                    <button class="clear-elem" type="submit"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>

    </div>

    <!--    Navigation Bar    -->
    <div class="navbar-wrapper container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light container nogut">
            <a class="navbar-brand" href="{{ route('home', app()->getLocale()) }}">GAZDA.MD</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse menu" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home', app()->getLocale()) }}">
                            @lang('home.home')
                        </a>
                    </li>
                    @foreach($headerLinks as $value)
                        @if($value->translation)
                            <li class="nav-item">
                                <a class="nav-link"
                                   href="{{ route('info.show', [app()->getLocale(), $value->translation->slug]) }}">{{ $value->translation->title }}</a>
                            </li>
                        @endif
                    @endforeach
                    {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"--}}
                    {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--Dropdown--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                    {{--<a class="dropdown-item" href="#">Action</a>--}}
                    {{--<a class="dropdown-item" href="#">Another action</a>--}}
                    {{--<div class="dropdown-divider"></div>--}}
                    {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                    {{--</div>--}}
                    {{--</li>--}}

                </ul>
                <div class="form-inline my-2 my-lg-0">
                    @if(Auth::check())
                        <a class="btn add-item-btn my-2 my-sm-0"
                           href="{{route('apartments.create', [app()->getLocale()] )}}">
                            <i class="fas fa-plus"></i>
                            <span>
                                Добавить обьявление
                            </span>
                        </a>
                    @else
                        <button class="btn add-item-btn my-2 my-sm-0" data-toggle="modal" data-target="#registerModal">
                            <i class="fas fa-plus"></i>
                            <span>
                                Добавить обьявление
                            </span>
                        </button>
                    @endif
                </div>
            </div>
        </nav>
    </div>
</header>

@yield('content')

{{-- Sticky sidebar --}}
{{--<div class="sticky-sidebar-right">--}}

    {{--<div class="sticky-sidebar-right-item sidebar-item-wrapper viber">--}}
    {{--<div class="sticky-sidebar-right-heading">--}}
    {{--<img src="{{ asset('img/viber.png') }}">--}}
    {{--</div>--}}
    {{--<div class="sticky-sidebar-right-content-wrapper">--}}
    {{--<div class="sticky-sidebar-right-content">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="sticky-sidebar-right-item sidebar-item-wrapper whatsapp">--}}
    {{--<div class="sticky-sidebar-right-heading">--}}
    {{--<img src="{{ asset('img/WhatsApp.png') }}">--}}
    {{--</div>--}}
    {{--<div class="sticky-sidebar-right-content-wrapper">--}}
    {{--<div class="sticky-sidebar-right-content">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="sticky-sidebar-right-item sidebar-item-wrapper curs">--}}
        {{--<div class="sticky-sidebar-right-heading">--}}
            {{--<img src="{{ asset('img/currency-icon.png') }}">--}}
        {{--</div>--}}
        {{--<div class="sticky-sidebar-right-content-wrapper">--}}
            {{--<div class="sticky-sidebar-right-content">--}}
                {{--<!--  START: Curs.md Widget HTML 1.0-->--}}
                {{--<script language="JavaScript" type="text/javascript"--}}
                        {{--src="http://www.curs.md/ro/curs_provider/ffffff/160/595657"></script>--}}
                {{--<!--  END: Curs.md Widget HTML 1.0-->--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}

{{--</div>--}}

<footer>
    <div class="footer-inner container">
        <div class="row">
            <div class="col-xs-12 col-md-3 footer-column">

                <a class="navbar-brand" href="#">GAZDA.MD</a>

                <div class="footer-contacts">
                    <!-- Social Links -->
                    <div class="socials">

                        <a class="facebook" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>

                        <a class="odnoklassniki" href="#">
                            <i class="fab fa-odnoklassniki"></i>
                        </a>

                        <a class="vk" href="#">
                            <i class="fab fa-vk"></i>
                        </a>

                        <a class="instagram" href="#">
                            <i class="fab fa-instagram"></i>
                        </a>

                    </div>
                    <p class="copyright">© 2018 Gazda.md. All rights reserved</p>
                </div>

            </div>

            <div class="col-xs-12 col-md-6 footer-column">

                <h5>Подробнее о нас</h5>

                <ul class="sitemap">
                    @foreach($footerLinks as $value)
                        @if($value->translation)
                            <li>
                                <a href="{{ route('info.show', [app()->getLocale(), $value->translation->slug]) }}">{{ $value->translation->title }} </a>
                            </li>
                        @endif
                    @endforeach
                </ul>

            </div>

            <div class="col-xs-12 col-md-3 footer-column">

                <h5>ПОДПИШИТЕСЬ НА РАССЫЛКУ</h5>

                <form class="mail-subscription">

                    <input type="email" class="subscribe mailchimp">

                    <button type="submit">
                        <i class="far fa-envelope"></i>
                    </button>

                </form>
            </div>
        </div>
    </div>
</footer>

{{--  Register Form  --}}
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Регистрация</h5>

                <div>
                    <span>или</span>
                    <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#authModal">Войти</a>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('blocks/register')
            </div>
        </div>
    </div>
</div>

{{--  Login Form  --}}
<div class="modal fade" id="authModal" tabindex="-1" role="dialog" aria-labelledby="authModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Войти</h5>

                <div>
                    <span>или</span>
                    <a href="" data-dismiss="modal" data-toggle="modal" data-target="#registerModal">Регистрация</a>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('blocks/login')
            </div>
        </div>
    </div>
</div>

<!-- Libraries -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/popper.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>

<!-- Custom Javascript File -->
<script src="{{ asset('/js/custom.js') }}"></script>

<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


@yield('js')
</body>
</html>
