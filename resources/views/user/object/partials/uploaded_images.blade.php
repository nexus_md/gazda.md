@foreach($gallery as $image)
    <div class="pull-left image-container">
        <img class="image-preview" src="{{ route('image', $image->image) }}"/>
        <span data-id="{{ $image->id }}"
              data-href="{{ route('user.object.delete-image-gallery', [app()->getLocale(), request()->route('user'), $image->id]) }}"><i
                    class="fa fa-remove"></i></span>
    </div>
@endforeach