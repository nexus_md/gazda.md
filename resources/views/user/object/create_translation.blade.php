@extends('layouts.user')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('resource_view.add_edit_translations')</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ route('user.object.process-translation', [app()->getLocale(), request()->route('user'), $id]) }}"
                  method="POST"
                  data-success-callback="successCallback" data-error-callback="errorCallback">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-2">
                        <label for="locale">*@lang('form.select_locale')</label>
                        <select class="form-control select2" name="locale" id="locale">
                            @foreach($locales as $key => $locale)
                                <option value="{{ $key }}" {{ $key == app()->getLocale() ? 'selected' : '' }}>{{ $locale }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="description">*@lang('form.description')</label>
                        <textarea name="description" id="description" rows="5" class="form-control"></textarea>
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="text">*@lang('form.text')</label>
                        <textarea name="text" id="text" rows="10" class="form-control"></textarea>
                        <span class="help-block error-message"></span>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('object.index', app()->getLocale())])
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
    <script>
        $('.select2').select2();

        CKEDITOR.replace('text', {
            on: {
                change: function () {
                    this.updateElement();
                    $('#text').change();
                }
            }
        });

        $('#locale').change(function () {
            $('.overlay').removeClass('hidden');

            $.ajax({
                url: '{{ route('user.object.get-translation', [app()->getLocale(), request()->route('user'), $id]) }}',
                method: 'GET',
                data: {locale: $(this).val()},
                success: function (data) {
                    if (data) {
                        for (var key in data) {
                            var input = $('input[name="' + key + '"], select[name="' + key + '"], textarea[name="' + key + '"]');
                            input.val(data[key]);
                        }
                    } else {
                        $('input:not([name="_token"]), textarea, select:not([name="locale"])').val('');
                    }

                    CKEDITOR.instances.text.setData($('#text').val());

                    $('.overlay').addClass('hidden');
                },
                error: function (data) {
                    console.log(data);
                    alert('Unexpected error.');
                }
            })
        });

        $('#locale').change();
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@stop