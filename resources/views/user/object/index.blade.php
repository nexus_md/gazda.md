@extends('layouts.user')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">@lang('sidebar.view_realtors')</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>@lang('form.name')</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($data))
                    @foreach($data as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            @if(isset($value->city->city->city))
                                <td>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}, {$value->address}" }}</td>

                            @elseif(isset($value->city->city))
                                <td>{{ "{$value->city->city->name}, {$value->city->name}, {$value->address}" }}</td>

                            @else
                                <td>{{ "{$value->city->name}, {$value->address}" }}</td>
                            @endif
                            <td>
                                <a class="object-info" data-id="{{$value->id}}"
                                   data-owner_name="{{$value->owner_name}}" data-owner_number="{{$value->owner_number}}" data-description="{{$value->object_insights}}" data-address="{{$value->address}}">
                                    <i class="fa fa-info" aria-hidden="true"></i> More Info
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('user.object.translation', [app()->getLocale(), request()->route('user'), $value->id]) }}"><i
                                            class="fa fa-edit"></i>
                                    <span>@lang('resource_view.add_edit_translations')</span></a></td>
                            <td>
                                <a href="{{ route('user.object.edit', [app()->getLocale(), request()->route('user'), $value->id]) }}"><i
                                            class="fa fa-edit"></i> <span>@lang('resource_view.edit')</span></a></td>
                            <td>
                                <a href="{{ route('user.action.create', [app()->getLocale(), request()->route('user'), 'object' => $value->id]) }}"><i
                                            class="fa fa-plus"></i> <span>@lang('sidebar.create_action')</span></a></td>
                        </tr>
                    @endforeach
                @else
                    <tr class="odd text-center">
                        <td valign="top" colspan="4" class="dataTables_empty">@lang('resource_view.no_data')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            <div class="pull-right">
                {!! $data->links() !!}
            </div>
        </div>
        <!-- /.box-body -->
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>

    @include('admin.object.partials.more_info_modal')
@stop

@section('js')
    <script src="{{ asset('js/resource_view.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.object-info').click(function () {
                var object_id = $(this).data('id');
                var owner_name = $(this).data('owner_name');
                var owner_number = $(this).data('owner_number');
                var description = $(this).data('description');
                var address = $(this).data('address');

                var modalBody = `<p><span class='heading'>Owner's Name: </span>${owner_name}</p>
                                 <p><span class='heading'>Owner's Phone: </span>${owner_number}</p>
                                 <p><span class='heading'>Address: </span>${address}</p>
                                 <p><span class='heading'>Description: </span>${description}</p>`;

                $('#more_info_modal_body').html(modalBody);
                $('#more_info_modal_title').html(`Object #${object_id} detailed information`);
                $('#more_info_modal').modal('toggle');
            });
        });
    </script>
@stop

@section('css')
    <style>
        .modal-title{
            display: inline;
        }
        .modal-body .heading{
            font-weight: 600;
            padding-right: 5px;
        }
        table a{
            cursor: pointer;
        }
    </style>
@stop