<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview {{ strpos($currentRoute, 'user.object') !== FALSE ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>@lang('sidebar.object')</span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $currentRoute == 'user.object.create' ? 'active' : '' }}"><a
                                href="{{ route('user.object.create', [app()->getLocale(), request()->route('user')]) }}"><i
                                    class="fa fa-plus"></i> @lang('sidebar.create_object')</a></li>
                    <li class="{{ $currentRoute == 'user.object.index' ? 'active' : '' }}"><a
                                href="{{ route('user.object.index', [app()->getLocale(), request()->route('user')]) }}"><i
                                    class="fa fa-eye"></i> @lang('sidebar.view_objects')</a></li>
                </ul>
            </li>

            <li class="{{ $currentRoute == 'user.action.index' ? 'active' : '' }}"><a
                        href="{{ route('user.action.index', [app()->getLocale(), request()->route('user')]) }}"><i
                            class="fa fa-eye"></i> <span>@lang('sidebar.view_actions')</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>