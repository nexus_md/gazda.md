@extends('layouts.user')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('sidebar.create_action')</h3>
        </div>
        <div class="box-body">
            <form class="ajax"
                  action="{{ route('user.action.store', [app()->getLocale(), request()->route('user')]) }}"
                  method="POST"
                  data-success-callback="successCallback" data-error-callback="errorCallback">
                {!! csrf_field() !!}

                <input type="hidden" name="object_id" value="{{ request()->object }}">

                <div class="row">
                    <div class="form-group col-xs-12">
                        <label for="type">*@lang('form.select_action_type')</label>
                        <select class="form-control select2" name="type" id="type">
                            @foreach(config('estate.action_type') as $key => $action)
                                <option value="{{ $key }}">@lang($action)</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="advance_price">@lang('form.advance_price')</label>
                        <input type="text" class="form-control" name="advance_price" id="advance_price">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="passive_price">@lang('form.passive_price')</label>
                        <input type="text" class="form-control" name="passive_price" id="passive_price">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="total_price">@lang('form.total_price')</label>
                        <input type="text" class="form-control" name="total_price" id="total_price">
                        <span class="help-block error-message"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="from">@lang('form.from')</label>
                        <input type="text" class="form-control date-picker" name="from" id="from">
                    </div>

                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="to">@lang('form.to')</label>
                        <input type="text" class="form-control date-picker" name="to" id="to">
                        <span class="help-block error-message"></span>
                    </div>

                    <div class="form-group col-xs-12">
                        <label for="text">@lang('form.comment')</label>
                        <textarea name="text" id="text" class="form-control" rows="10"></textarea>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">@lang('form.submit')</button>
                </div>
            </form>
        </div>
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
    </div>

    @include('partials.form.modals', ['url' => route('user.action.index', [app()->getLocale(), request()->route('user')])])
@stop

@section('js')
    <script src="{{ asset('js/form_validation.js') }}"></script>
    <script src="{{ asset('js/form_callbacks.js') }}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('.date-picker').datepicker({autoclose: true, format: 'dd.mm.yyyy'});
    </script>
@stop

@section('css')
    <link rel="stylesheet"
          href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@stop