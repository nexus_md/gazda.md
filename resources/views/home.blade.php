@extends('layouts.app')

@section('content')
    <div class="home_page-map-wrapper">
        <div class="home_page-map">
            <div id="gmaps" width="100%" height="400px" style="height: 400px;"></div>
        </div>
        <div class="filter-section-wrapper container">

            {{--<div class="filter-mobile-toggle">--}}
                {{--<button id="filter-md-display" class="btn-lg d-md-none">--}}
                    {{--Пойск по фильтрам--}}
                    {{--<i class="fas fa-angle-down"></i>--}}
                {{--</button>--}}
            {{--</div>--}}

            <div class="filter-section">
                <a class="d-none d-md-inline" href="{{ route('apartments.index', app()->getLocale()) }}">
                    <button class="btn btn-mapSearch">
                        <i class="fas fa-map-marker-alt"></i>
                        Пойск по карте
                    </button>
                </a>
                <form action="{{ route('apartments.index', app()->getLocale()) }}" method="get">
                    <div class="row">

                        <!--         Region           -->
                        <div class="form-group col col-12 col-md-4">
                            <label for="regionFormControlSelect">Район</label>
                            <select class="form-control" id="regionFormControlSelect" name="parent_cities[]">
                                <option value="">Select city</option>
                                @foreach($cities as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>

                            <div class="regions">

                            </div>
                        </div>

                        <!--         House Type           -->
                        <div class="form-group col col-12 col-md-4">
                            <label for="regionFormControlSelect">Тип</label>
                            <select class="form-control" id="regionFormControlSelect" name="types[]">
                                @foreach(config('estate.object_type') as $key => $value)
                                    <option value="{{ $key }}">@lang($value)</option>
                                @endforeach
                            </select>
                        </div>

                        <!--         Rent Type           -->
                        <div class="form-group col col-12 col-md-4">
                            <label for="regionFormControlSelect">Тип аренды</label>
                            <select class="form-control" id="regionFormControlSelect" name="price_type[]">
                                @foreach(config('estate.object_price_type') as $key => $value)
                                    <option value="{{ $key }}">@lang($value)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">

                        <!--         Rooms           -->
                    {{--<div class="form-group col col-12 col-md-4">--}}
                    {{--<label for="regionFormControlSelect">Район</label>--}}
                    {{--<select class="form-control" id="regionFormControlSelect">--}}
                    {{--<option>1</option>--}}
                    {{--<option>2</option>--}}
                    {{--<option>3</option>--}}
                    {{--<option>4</option>--}}
                    {{--<option>5</option>--}}
                    {{--</select>--}}
                    {{--</div>--}}

                    <!--         Price           -->
                        <div class="form-group col d-none d-md-block col-12 col-md-4">
                            <label for="regionFormControlSelect">Цена</label>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <input type="number" class="form-control" placeholder="@lang('object.from')"
                                           name="fromPrice">
                                </div>
                                <div class="col-12 col-md-6">
                                    <input type="number" class="form-control" placeholder="@lang('object.to')"
                                           name="toPrice">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col col-12 col-md-8">
                            <label for="regionFormControlSelect">Комнат</label>
                            <div class="row">
                                <div class="col-6 col-md-3">
                                    <input type="number" class="form-control" placeholder="@lang('object.from')"
                                           name="fromroom_count">
                                </div>
                                <div class="col-6 col-md-3">
                                    <input type="number" class="form-control" placeholder="@lang('object.to')"
                                           name="toroom_count">
                                </div>

                                <!--         Search Button           -->
                                <div class="form-group col col-12 col-md-6">
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        <i class="fas fa-search"></i>
                                        Пойск
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
                <a class="extended-search" href="{{ route('apartments.index', app()->getLocale()) }}">Расширеный
                    пойск</a>

                <div class="overlay d-none">
                    <i class="fas fa-sync-alt fa-spin"></i>
                </div>
            </div>
        </div>

    </div>

    <div class="main-content container-fluid">

        <!--    Main Grid    -->
        <div class="main-grid-wrap product-grid container nogut">
            <!--  Product Heading -->
            <div class="main-grid-heading-wrap  grid-heading container-fluid">
                <div class="main-grid-heading row">
                    <div class="col-xs-12 col-md-8 nogut form-inline">
                        <h2>Популярные Объявления в Кишиневе</h2>
                    </div>
                </div>
            </div>

            <!--  Product Wrapper  -->
            <div class="product-grid-wrapper row grid-of-3">
                @foreach($featuredObjects as $value)
                    <div class="product-item-card-wrapper col-xs-12 col-sm-6 col-lg-4">
                        <div class="product-item-card">
                            <div class="product-item-header">
                                <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                    <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                </a>
                            </div>

                            <div class="product-item-content d-flex nogut">
                                <div class="col-md-8 col-lg-8 col-xs-8 product-address">
                                    <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                        @if(isset($value->city->city->city))
                                            <h5>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}" }}</h5>
                                        @elseif(isset($value->city->city))
                                            <h5>{{ "{$value->city->city->name}, {$value->city->name}" }}</h5>
                                        @else
                                            <h5>{{ $value->city->name }}</h5>
                                        @endif
                                    </a>
                                    <p>{{ $value->address }}</p>
                                </div>
                                <div class="col-md-4 col-xs-4 price-block">
                                    <span class="price">
                                        {{ $value->price }} €
                                        @if(!$value->price_type)
                                            @lang('object.hour')
                                        @endif
                                    </span>
                                </div>


                            </div>
                            <div class="description">
                                <hr class="product-hr">
                                <div class="col-md-12 col-xs-12 product-type">
                                    <div class="row description-row">
                                        <div class="col-md-4 col-xs-4 col-4 desc-el">
                                            @if($value->outer_type == 0)
                                                <i class="far fa-building"></i>
                                                <span class="apartment">
                                                @lang('object.apartment')
                                            </span>
                                            @else
                                                <i class="fas fa-home"></i>
                                                <span class="house">
                                                @lang('object.house')
                                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-4 col-xs-4 col-4 desc-el">
                                            <i class="fas fa-bed"></i>
                                            @if($value->room_count == 1)
                                                <span>{{$value->room_count}}</span>
                                            @elseif($value->room_count > 1)
                                                <span>{{$value->room_count}}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-4 col-xs-4 col-4 desc-el">
                                            <i class="fas fa-ruler-horizontal"></i>
                                            <span>{{$value->area}}m<sup>2</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <!--      See All button      -->
            <div class="see-all row">
                <a href="{{ route('apartments.index', app()->getLocale()) }}" class="btn btn-md">Все объявления</a>
            </div>
        </div>

        <!--    Featured Grid   -->
        <div class="featured-grid-wrap product-grid container nogut">
            <div class="featured-grid-heading grid-heading">
                <h2>Последние объявления в Молдове</h2>
            </div>
            <!--  Product Wrapper  -->
            <div class="product-grid-wrapper row grid-of-4">
                @for($i = 0; $i < $lastObjects->count(); $i++)
                    @php ($value = $lastObjects[$i])

                    <div class="product-item-card-wrapper col-xs-12 col-sm-6 col-lg-3">
                        <div class="product-item-card">
                            <div class="product-item-header">
                                <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                    <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                </a>
                            </div>

                            <div class="product-item-content d-flex nogut">
                                <div class="col-xs-8 col-md-8 product-address">
                                    <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                        @if(isset($value->city->city->city))
                                            <h5>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}" }}</h5>
                                        @elseif(isset($value->city->city))
                                            <h5>{{ "{$value->city->city->name}, {$value->city->name}" }}</h5>
                                        @else
                                            <h5>{{ $value->city->name }}</h5>
                                        @endif
                                    </a>
                                    <p>{{ $value->address }}</p>
                                </div>
                                <div class="col-md-4 col-xs-4 price-block">
                                    <span class="price">
                                        {{ $value->price }} €
                                        @if(!$value->price_type)
                                            @lang('object.hour')
                                        @endif
                                    </span>
                                </div>
                            </div>

                            @if($lastObjects->count() - 1 == $i)
                                <div class="last-item-overlay blue">
                                    <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                    <a href="{{ route('apartments.index', app()->getLocale()) }}"
                                       class="item-overlay-text">
                                        Посмотреть все 100+ вариантов жилья
                                        </br>
                                        <i class="fas fa-arrow-right"></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endfor
            </div>
        </div>

        <!--    Featured Grid  per hour -->
        <div class="featured-grid-wrap product-grid container nogut">
            <div class="featured-grid-heading grid-heading">
                <h2>Последние объявления квартиры посуточно</h2>
            </div>
            <!--  Product Wrapper  -->
            <div class="product-grid-wrapper row grid-of-4">
                @for($i = 0; $i < $perHourObjects->count(); $i++)
                    @php ($value = $perHourObjects[$i])

                    <div class="product-item-card-wrapper col-xs-12 col-sm-6 col-lg-3">
                        <div class="product-item-card">
                            <div class="product-item-header">
                                <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                    <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                </a>
                            </div>

                            <div class="product-item-content d-flex nogut">
                                <div class="col-xs-8 col-md-8 product-address">
                                    <a href="{{ route('apartments.show', [app()->getLocale(), $value->slug]) }}">
                                        @if(isset($value->city->city->city))
                                            <h5>{{ "{$value->city->city->city->name}, {$value->city->city->name}, {$value->city->name}" }}</h5>
                                        @elseif(isset($value->city->city))
                                            <h5>{{ "{$value->city->city->name}, {$value->city->name}" }}</h5>
                                        @else
                                            <h5>{{ $value->city->name }}</h5>
                                        @endif
                                    </a>
                                    <p>{{ $value->address }}</p>
                                </div>
                                <div class="col-md-4 col-xs-4 price-block">
                                    <span class="price">
                                        {{ $value->price }} €
                                        @if(!$value->price_type)
                                            @lang('object.hour')
                                        @endif
                                    </span>
                                </div>
                            </div>

                            @if($perHourObjects->count() - 1 == $i)
                                <div class="last-item-overlay yellow">
                                    <img src="{{ $value->image ? route('image', $value->image->image) : url('images/placeholder.png') }}">
                                    <a href="{{ route('apartments.index', app()->getLocale()) }}"
                                       class="item-overlay-text">
                                        Посмотреть все 100+ вариантов жилья
                                        </br>
                                        <i class="fas fa-arrow-right"></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endfor
            </div>
        </div>

        <!--    Info blocks    -->
        <div class="info-blocks-wrap row">
            <div class="info-blocks-wrap container nogut">
                <div class="row info-blocks-inner">

                    <div class="info-block-item-card-wrapper col-sm-12 col-lg-4">
                        <div class="info-block-item-card">

                            <div>
                                <div class="info-block-item-header">
                                    <h5>
                                        <i class="fas fa-star reviews"></i>
                                        отзывы
                                    </h5>
                                </div>
                                <div class="info-block-item-content">
                                    <div class="review-content">
                                        <span class="review-bracket bracket-start"> « </span>
                                        @if (isset($comment))
                                            <span>{{ $comment->text }}</span>
                                        @else
                                            {{--Placeholder--}}
                                            <span>Am ramas multumit de conlucrarea noastracu Gazda.md, in urma careaia am inchiriat un apartament pe plac si confortabil.</span>
                                        @endif
                                        <span class="review-bracket bracket-end"> » </span>
                                    </div>
                                </div>
                            </div>

                            <div class="info-block-item-footer">

                                <span class="review-author">
                                    @if (isset($comment))
                                        {{ $comment->name }}
                                    @else
                                        {{--Placeholder--}}
                                        Mihai
                                    @endif
                                </span>

                                <span class="review-read-all"> <a
                                            href="{{ route('comments.index', ['locale'=> app()->getLocale()]) }}"> Все отзывы </a> </span>

                            </div>

                        </div>
                    </div>

                    <div class="info-block-item-card-wrapper col-sm-12 col-lg-4">
                        <div class="info-block-item-card">

                            <div>
                                <div class="info-block-item-header">
                                    <h5>
                                        <i class="far fa-file-alt prod-post"></i>
                                        объявления
                                    </h5>
                                </div>
                                <div class="info-block-item-content">
                                    <span>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                    </span>
                                </div>
                            </div>

                            <div class="info-block-item-footer">
                                <button class="btn add-item-btn my-2 my-sm-0" data-toggle="modal"
                                        data-target="#registerModal">
                                    <i class="fas fa-plus"></i>
                                    <span>
                                        Добавить обьявление
                                    </span>
                                </button>
                            </div>

                        </div>
                    </div>

                    <div class="info-block-item-card-wrapper col-sm-12 col-lg-4">
                        <div class="info-block-item-card">

                            <div class="info-block-item-header">
                                <h5>
                                    <i class="fas fa-key rent-list"></i>
                                    Снять квартиру
                                </h5>
                            </div>

                            <div class="info-block-item-content">
                                <ul class="rent-list">
                                    <li>1-комнатные <a href="#">32 533</a></li>
                                    <li>2-комнатные <a href="#">44 634</a></li>
                                    <li>3-комнатные <a href="#">37 190</a></li>
                                    <br>
                                    <li>Свободная планировка <a href="#">3 677</a></li>
                                    <li>Квартиры-студии <a href="#">2 407</a></li>
                                    {{--<li>Комнаты в квартире <a href="#">2 053</a></li>--}}
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="info-block-item-card-wrapper col-sm-12 col-lg-12">
                        <div class="info-block-item-card info-item-fw">
                            <div>

                                <div class="info-block-item-header">
                                    <i class="far fa-map info-map"></i>
                                </div>
                                <div class="info-block-item-content">
                                    <h5>
						<span>
							Поиск на карте все недвижимости
						</span>
                                    </h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut
                                        labore et dolore magna aliqua. Ut enim ad minim</p>
                                </div>
                            </div>

                            <div class="info-block-item-footer">
                                <button class="btn add-item-btn my-2 my-sm-0" type="submit">
                                    <i class="fas fa-map-marker-alt map-marker"></i>
                                    <span> Показати на карте </span>
                                </button>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Useful blocks -->
        <div class="useful-block-wrap container nogut">

            <div class="useful-block-grid-heading grid-heading">
                <h2>Мы можем вам помочь</h2>
            </div>

            <div class="useful-block-grid-wrapper row grid-of-3">

                <div class="useful-block-grid-item-wrapper col-sm-12 col-lg-4">

                    <div class="useful-block-grid-item-card">
                        <a target="_blank" href="https://www.curs.md">
                            <img src="../../img/currency-icon.png">
                            <h5>Обменный курс</h5>
                        </a>

                        <a target="_blank" href="#">
                            <i class="fas fa-arrow-right"></i>
                        </a>
                    </div>
                </div>

                <div class="useful-block-grid-item-wrapper col-sm-12 col-lg-4">

                    <div class="useful-block-grid-item-card">

                        <a target="_blank" href="#">
                            <img src="../../img/rental-icon.png">
                            <h5>Аренда автомобилей</h5>
                        </a>

                        <a target="_blank" href="#">
                            <i class="fas fa-arrow-right"></i>
                        </a>

                    </div>
                </div>

                <div class="useful-block-grid-item-wrapper col-sm-12 col-lg-4">
                    <div class="useful-block-grid-item-card">

                        <a target="_blank" href="#">
                            <img src="../../img/routing-icon.png">
                            <h5>Карта доступа</h5>
                        </a>

                        <a target="_blank" href="#">
                            <i class="fas fa-arrow-right"></i>
                        </a>

                    </div>
                </div>

            </div>

        </div>
    </div>

    </div>
@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR3nISjK4nrFsrCM2te7WdEG2s2ANzpME"></script>
    <script src="{{ asset('js/marker_clusterer.js') }}"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>

    <script>
        $('#regionFormControlSelect').change(function () {
            if (!$(this).val()) {
                $('div .regions').html('');
                return;
            }

            $('.filter-section .overlay').removeClass('d-none');

            $.ajax({
                url: '{{ route('ajax-get-regions') }}',
                method: 'GET',
                data: {city_id: $(this).val()},
                success: function (data) {
                    var html = '';

                    for (key in data) {
                        var id = data[key];

                        html += '<input type="hidden" name="cities[]" value="' + id + '">';

                        $('div .regions').html(html);
                    }

                    $('.filter-section .overlay').addClass('d-none');
                },

                error: function (data) {

                }
            });
        });

        var gmap = new GMaps({
            div: '#gmaps',
            zoom: 10,
            lat: 47.010284180836166,
            lng: 28.86056900024414,
            markerClusterer: function (map) {
                options = {
                    gridSize: 40,
                    styles: [
                        {
                            textColor: 'white',
                            url: '{{ url('img/cluster-marker.png') }}',
                            width: 59,
                            height: 59
                        },
                        {
                            textColor: 'white',
                            url: '{{ url('img/cluster-marker.png') }}',
                            width: 59,
                            height: 59
                        },
                        {
                            textColor: 'white',
                            url: '{{ url('img/cluster-marker.png') }}',
                            width: 59,
                            height: 59
                        }
                    ]
                };

                return new MarkerClusterer(map, [], options);
            }
        });
        var markers = {!! json_encode($markers) !!};

        for (key in markers) {
            markers[key]['icon'] = '{{ url('img/marker.png') }}';
        }

        gmap.addMarkers(markers);

    </script>
@stop
